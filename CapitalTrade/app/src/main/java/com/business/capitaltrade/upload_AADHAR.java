package com.business.capitaltrade;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;


/**
 * A simple {@link Fragment} subclass.
 */
public class upload_AADHAR extends BaseFragment {
ImageView i1,i2,i3,i4;
    final private int IMG=2;
    ScrollView scroladhar;
    final private int IMG_REQUEST=1;
ProgressDialog progressDialog;
    final private int IMG1=4;
    final private int IMG_REQ=3;
    Uri imageUri,imageUri1;
    SessionManager session;
    String id="",mob="";
    Bitmap bitmap,bitmap1;
    EditText adhar;
   // String adhaarno="";
    String secret="";
    String spinvalue="";
    RelativeLayout rl,rl1,ioi;
    Spinner spin;
    TextView textspin;
    LinearLayout clickadhar,click;
    String doc[]={"Please Select","Voter ID","Ration Card","Passport","Driving Licence","Govt. Employee ID"};
    public upload_AADHAR() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_upload__aadhar, container, false);
        session = new SessionManager(getContext().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        id = user.get(SessionManager.KEY_ID);
        mob = user.get(SessionManager.KEY_MOBILE);
        i1=(ImageView)v.findViewById(R.id.iv1);
        i2=(ImageView)v.findViewById(R.id.iv3);
        scroladhar=(ScrollView)v.findViewById(R.id.scroladhar);
        i3=(ImageView)v.findViewById(R.id.iv);
        i4=(ImageView)v.findViewById(R.id.iv2);
        adhar=(EditText) v.findViewById(R.id.adhar);
        textspin=(TextView) v.findViewById(R.id.spintext);
        rl=(RelativeLayout) v.findViewById(R.id.rl);
        rl1=(RelativeLayout) v.findViewById(R.id.rl1);
        ioi=(RelativeLayout) v.findViewById(R.id.ioi);
        clickadhar=(LinearLayout) v.findViewById(R.id.clickadhar);
        click=(LinearLayout) v.findViewById(R.id.click);
        spin=(Spinner) v.findViewById(R.id.spinner);
        //Creating the ArrayAdapter instance having the bank name list
        ArrayAdapter aa = new ArrayAdapter(getContext(),R.layout.spinnertext,doc);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);

        adhar.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        i1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable(getActivity())) {
                    selectImage2();
                }
                else
                {
                    Toast.makeText(getContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

       i2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable(getActivity())) {
                    selectImage3();
                }
                else
                {
                    Toast.makeText(getContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   scroladhar.fullScroll(ScrollView.FOCUS_DOWN);
                if(isNetworkAvailable(getActivity())) {
                    getadharinfo();
                }
                else
                {
                    Toast.makeText(getContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


 chechdatastatus2();
 myspin();


        return v;
    }

    private void selectImage2() {


        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {


                    selectimage();
                  /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

                    startActivityForResult(intent, 1);*/

                } else if (options[item].equals("Choose from Gallery"))

                {

                    Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, 2);


                } /*else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }*/

            }

        }).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TOdo
                dialog.cancel();
            }
        });

        builder.show();
    }
    private void selectimage(){
        //  Intent intent = new Intent();
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //  intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(i,IMG_REQUEST);
    }

    private void selectImage3() {


        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {


                    selectimage1();
                  /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

                    startActivityForResult(intent, 1);*/

                } else if (options[item].equals("Choose from Gallery"))

                {

                    Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent, 4);

                    //if everything is ok we will open image chooser
                   /* Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.putExtra("crop","true");
                    i.putExtra("aspectX",1);
                    i.putExtra("aspectY",1);
                    i.putExtra("outputX",200);
                    i.putExtra("outputY",200);
                    i.putExtra("return-data",true);
                 //   i.putExtra("crop","true");
                    startActivityForResult(i, 2);*/


                } /*else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }*/

            }

        }).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TOdo
                dialog.cancel();
            }
        });

        builder.show();
    }
    private void selectimage1(){
        //  Intent intent = new Intent();
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //  intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(i,IMG_REQ);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==IMG_REQUEST && resultCode ==RESULT_OK && data !=null){
            Uri path = data.getData();
            //  bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
            bitmap = (Bitmap) data.getExtras().get("data");
            i3.setImageBitmap(bitmap);
            secret="1";
            uploadadhar();
        }
        if(requestCode==IMG && resultCode ==RESULT_OK && data !=null)
        {
            /* Bundle extras  = data.getExtras();
            bitmap =extras.getParcelable("data");
            iv1.setImageBitmap(bitmap);*/
            //getting the image Uri
            imageUri = data.getData();



            // ImageCropFunction();
            try {
                //getting bitmap object from uri
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageUri);

                //displaying selected image to imageview
                //   image.setImageBitmap(bitmap);
                i3.setImageURI(imageUri);
                secret="1";
                uploadadhar();
                //calling the method uploadBitmap to upload image
                // uploadBitmap(bitmap);
            } catch (IOException e) {


                Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();
            }
        }

         if(requestCode==IMG_REQ && resultCode ==RESULT_OK && data !=null){
            Uri path = data.getData();
            //  bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
            bitmap1 = (Bitmap) data.getExtras().get("data");
            i4.setImageBitmap(bitmap1);
             secret="2";
             uploadadhar();

        }



      if(requestCode==IMG1 && resultCode ==RESULT_OK && data !=null)
        {
            /* Bundle extras  = data.getExtras();
            bitmap =extras.getParcelable("data");
            iv1.setImageBitmap(bitmap);*/
            //getting the image Uri
            imageUri = data.getData();

            // ImageCropFunction();
            try {
                //getting bitmap object from uri
                bitmap1 = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageUri);
                //displaying selected image to imageview
                 // i4.setImageBitmap(bitmap1);
                i4.setImageURI(imageUri);
                secret="2";
                uploadadhar();
                //calling the method uploadBitmap to upload image
                // uploadBitmap(bitmap);.
            } catch (IOException e) {

                Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();
            }
        }

        else
        {

        }
    }

    private String imagetostring(Bitmap bitmap)
    {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inSampleSize=1;
        final float scale=getResources().getDisplayMetrics().density;

        // Display dis=getWindowManager().getDefaultDisplay();
        // int width=dis.getWidth();
        bitmap=Bitmap.createScaledBitmap(bitmap,600,550, true);
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[] imbytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imbytes, Base64.DEFAULT);

    }

    private String imagetostring1(Bitmap bitmap1)
    {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inSampleSize=1;
        final float scale=getResources().getDisplayMetrics().density;
        // Display dis=getWindowManager().getDefaultDisplay();
        // int width=dis.getWidth();
        bitmap1=Bitmap.createScaledBitmap(bitmap1,600,550, true);
        bitmap1.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[] imbytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imbytes, Base64.DEFAULT);

    }

    public void getadharinfo(){
      //  adhaarno=adhar.getText().toString().trim();
        if (i1.getVisibility() == View.VISIBLE  || i2.getVisibility() == View.VISIBLE  ) {
            Toast.makeText(getContext().getApplicationContext(), "Please upload Identity Card image", Toast.LENGTH_SHORT).show();
        }
        else if(adhar.getText().toString().trim().equals(""))
        {
          //  scroladhar.fullScroll(View.FOCUS_DOWN);
            scroladhar.fullScroll(ScrollView.FOCUS_DOWN);
            Toast.makeText(getContext().getApplicationContext(), "Please fill Identity Card No.", Toast.LENGTH_SHORT).show();

        }
        else
        {
            secret="3";
            uploadadhar();
        }

    }

    private void uploadadhar() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";
        String urll="https://onistech.in/capitaltrade/android/adharinfo.php";
        ;
     //   urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.uploadAdhar(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                       // Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        try {
                            // ar.clear();
                            //for (int i = 0; i < response.length(); i++) {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            String success = jsonObject.getString("success");
                           // String status = jsonObject.getString("status");

                            if (success.equals(0)) {
                                session.logoutUser();
                                Toast.makeText(getActivity(), "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                                getActivity().finish();
                            }
                            else {
                                String code = jsonObject.getString("code");
                                if (code.equals("updatedfront")) {
                                    String imagfront1 = jsonObject.getString("front");

                                    i1.setVisibility(GONE);
                                    Glide.with(getContext().getApplicationContext())
                                            .load(imagfront1)
                                            //  .apply(new RequestOptions().override(1200, 200))
                                            .apply(new RequestOptions().disallowHardwareConfig()
                                                    .override(Target.SIZE_ORIGINAL)
                                                    .dontTransform()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true)
                                                    .dontAnimate()
                                                    .placeholder(R.drawable.loading)
                                                    .format(DecodeFormat.PREFER_ARGB_8888))
                                            .into(i3);

                                } else if (code.equals("updatedback")) {
                                    String imagback1 = jsonObject.getString("back");
                                    i2.setVisibility(GONE);
                                    //  Toast.makeText(getContext(), ""+imagback1, Toast.LENGTH_SHORT).show();
                                    Glide.with(getContext().getApplicationContext())
                                            .load(imagback1)
                                            //  .apply(new RequestOptions().override(1200, 200))
                                            .apply(new RequestOptions().disallowHardwareConfig()
                                                    .override(Target.SIZE_ORIGINAL)
                                                    .dontTransform()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true)
                                                    .dontAnimate()
                                                    .placeholder(R.drawable.loading)
                                                    .format(DecodeFormat.PREFER_ARGB_8888))
                                            .into(i4);

                                } else if (code.equals("updatedadhar")) {
                                    Toast.makeText(getContext().getApplicationContext(), "Identity Information saved successfully !!", Toast.LENGTH_SHORT).show();
                                    String adharno = jsonObject.getString("adhar");
                                    ioi.setVisibility(GONE);
                                    byte[] data = Base64.decode(adharno, Base64.DEFAULT);
                                    // e2.setText();
                                    adhar.setText(new String(data, StandardCharsets.UTF_8));
                                    adhar.setEnabled(false);
                                    adhar.setCursorVisible(false);
                                    click.setClickable(false);
                                    click.setBackgroundColor(Color.parseColor("#BDBDBD"));
                                    ((PersonalInfo) getActivity()).setCurrentItem(2, true);
                                } else {

                                    Toast.makeText(getContext(), "Something went wrong.Please Try Again !!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {

                        }
                    }
                    }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext().getApplicationContext(), "Server has encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
               // params.put("mob",mob);



                params.put("id",id);
                if(secret.equals("1"))
                {
                    params.put("front",imagetostring(bitmap));
                }
               else if(secret.equals("2"))
                   {
                    params.put("back",imagetostring1(bitmap1));
                }
                else if(secret.equals("3"))
                {
                    byte[] data = adhar.getText().toString().trim().getBytes(StandardCharsets.UTF_8);
                    params.put("adharno",Base64.encodeToString(data, Base64.DEFAULT));
                    params.put("idenityno",spinvalue);
                  //  params.put("status","1");
                }
               //
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                35000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }


    public void chechdatastatus2() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";

        String urll="https://onistech.in/capitaltrade/android/showadhar.php?id="+id+"";
        ;
        // urll = urll.replaceAll(" ", "%20");h
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.showAdhar(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        // Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        try {
                            // ar.clear();
                            //  for (int i = 0; i < response.length(); i++) {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            //get value of first index
                            String success = jsonObject.getString("success");

                            if (success.equals(0)) {
                                session.logoutUser();
                                Toast.makeText(getActivity(), "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                               getActivity().finish();
                            }
                            else{
                                String status1 = jsonObject.getString("adharno");
                                if (!success.equals("")) {
                                    String imagfront = jsonObject.getString("front");
                                    String imagback = jsonObject.getString("back");
                                    String adharnum = jsonObject.getString("adharno");
                                    ioi.setVisibility(GONE);
                                    rl.setVisibility(View.VISIBLE);
                                    rl1.setVisibility(View.VISIBLE);
                                    clickadhar.setVisibility(View.VISIBLE);
                                    i1.setVisibility(GONE);
                                    i2.setVisibility(GONE);
                                    byte[] data = Base64.decode(adharnum, Base64.DEFAULT);
                                    //  e2.setText(new String(data, StandardCharsets.UTF_8));
                                    adhar.setText(new String(data, StandardCharsets.UTF_8));
                                    adhar.setEnabled(false);
                                    adhar.setCursorVisible(false);
                                    click.setClickable(false);
                                    click.setBackgroundColor(Color.parseColor("#BDBDBD"));
                                    Glide.with(getContext().getApplicationContext())
                                            .load(imagfront)
                                            //  .apply(new RequestOptions().override(1200, 200))
                                            .apply(new RequestOptions().disallowHardwareConfig()
                                                    .override(Target.SIZE_ORIGINAL)
                                                    //.dontTransform()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true)
                                                    .dontAnimate()
                                                    .placeholder(R.drawable.loading)
                                                    //.fitCenter()
                                                    .format(DecodeFormat.PREFER_ARGB_8888))
                                            .into(i3);

                                    Glide.with(getContext().getApplicationContext())
                                            .load(imagback)
                                            //  .apply(new RequestOptions().override(1200, 200))
                                            .apply(new RequestOptions().disallowHardwareConfig()
                                                    .override(Target.SIZE_ORIGINAL)
                                                    // .dontTransform()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true)
                                                    .dontAnimate()
                                                    .placeholder(R.drawable.loading)
                                                    //.fitCenter()
                                                    .format(DecodeFormat.PREFER_ARGB_8888))
                                            .into(i4);


                                    PersonalInfo activity = (PersonalInfo) getActivity();
                                    String getData = activity.s2;
                                    if (!getData.equals("")) {
                                        i1.setVisibility(View.VISIBLE);
                                        i2.setVisibility(View.VISIBLE);
                                        click.setClickable(true);
                                        click.setBackgroundColor(Color.parseColor("#0099cc"));
                                        adhar.setEnabled(true);
                                        adhar.setCursorVisible(true);
                                        click.setClickable(true);
                                    } else {
//            i1.setVisibility(GONE);
//            i2.setVisibility(GONE);
                                    }
                                } else {
                                    ioi.setVisibility(View.VISIBLE);
                                    rl.setVisibility(View.VISIBLE);
                                    rl1.setVisibility(View.VISIBLE);
                                    clickadhar.setVisibility(View.VISIBLE);

                                }
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            // Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext().getApplicationContext(), "Server has encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }
//    @Override
//    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
//       // Toast.makeText(getContext(),doc[position] , Toast.LENGTH_LONG).show();
//        ucity = parent.getItemAtPosition(position).toString().trim();
//
//
//    }
//    @Override
//    public void onNothingSelected(AdapterView<?> arg0) {
//        // TODO Auto-generated method stub
//    }

    public void myspin(){
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinvalue = parent.getItemAtPosition(position).toString().trim();
                if(spinvalue.equals("Please Select"))
                {
                    rl.setVisibility(GONE);
                    rl1.setVisibility(GONE);
                    clickadhar.setVisibility(GONE);
                    click.setVisibility(GONE);
                }
                else
                {
                    scroladhar.fullScroll(View.FOCUS_DOWN);
                    adhar.requestFocus();
                    //scroladhar.fullScroll(ScrollView.FOCUS_DOWN);
                    rl.setVisibility(View.VISIBLE);
                    rl1.setVisibility(View.VISIBLE);
                    clickadhar.setVisibility(View.VISIBLE);
                    click.setVisibility(View.VISIBLE);
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}
