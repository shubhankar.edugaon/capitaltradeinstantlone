package com.business.capitaltrade;

public class ModelNoLoan {

    String code,sr,borrreqloanid,borrid,pname,lamnt,tenure,roi,loanno,ddate;
    public ModelNoLoan() {
    }

    public ModelNoLoan(String code,String sr, String borrreqloanid, String borrid, String pname, String lamnt,String tenure
    ,String roi,String loanno,String ddate){
        this.code = code;
        this.sr = sr;
        this.borrreqloanid = borrreqloanid;
        this.borrid=borrid;
        this.pname = pname;
        this.lamnt =lamnt;
        this.tenure =tenure;
        this.roi =roi;
        this.loanno =loanno;
        this.ddate=ddate;


    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLoanno() {
        return loanno;
    }

    public void setLoanno(String loanno) {
        this.loanno = loanno;
    }

    public String getDdate() {
        return ddate;
    }

    public void setDdate(String ddate) {
        this.ddate = ddate;
    }

    public String getSr() {
        return sr;
    }

    public void setSr(String sr) {
        this.sr = sr;
    }

    public String getBorrreqloanid() {
        return borrreqloanid;
    }

    public void setBorrreqloanid(String borrreqloanid) {
        this.borrreqloanid = borrreqloanid;
    }

    public String getBorrid() {
        return borrid;
    }

    public void setBorrid(String borrid) {
        this.borrid = borrid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getLamnt() {
        return lamnt;
    }

    public void setLamnt(String lamnt) {
        this.lamnt = lamnt;
    }

    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    public String getRoi() {
        return roi;
    }

    public void setRoi(String roi) {
        this.roi = roi;
    }
}
