package com.business.capitaltrade;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

;

import java.util.ArrayList;

public class RecyclerDocuments  extends RecyclerView.Adapter<RecyclerDocuments.MyViewHolder> {
   String  selectedFilePath="";
    ArrayList<ModelDocs> arrayList;
    private OnItemClick mCallback;
    Dialog dialog;
    //Pdf request code
    private int PICK_PDF_REQUEST = 1;
    String s1="",empstatus="";
    FloatingActionButton fab;
    long fileSizeInMB=0;
    Context context;
     public String[] web;
    public String[] serial;
    int j=1;
    LayoutInflater inflater;
    public static final String UPLOAD_URL = "https://onistech.in/capitaltrade/android/upload.php";
    public RecyclerDocuments()
    {

    }
    public RecyclerDocuments(ArrayList<ModelDocs> arrayList, Context context, OnItemClick listener, String[] web, String[] serial) {
        this.arrayList = arrayList;
        this.context = context;
        this.mCallback = listener;
        this.web=web;
        this.serial=serial;
        inflater = LayoutInflater.from(context);



    }
    @Override
    public RecyclerDocuments.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v=inflater.inflate(R.layout.uploadeddoc,parent,false);
        RecyclerDocuments.MyViewHolder myViewHolder = new RecyclerDocuments.MyViewHolder(v);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
      //  ModelDocs model = arrayList.get(position);


//        Toast.makeText(context, ""+arrayList.get(position).position, Toast.LENGTH_SHORT).show();

        holder.doc.setText(web[position]);
        holder.sr.setText(serial[position]+")");
//
//        for(int k=0;k<arrayList.size();k++) {
//
//
//
//        }


        for(int k=0;k<arrayList.size();k++) {
            String s =arrayList.get(k).position.toString();
           // Toast.makeText(context, ""+s, Toast.LENGTH_SHORT).show();
          //  Toast.makeText(context, ""+serial[position], Toast.LENGTH_SHORT).show();
            if(serial[position].equals(s))
            {
                holder.btn.setVisibility(View.GONE);
                holder.btn1.setVisibility(View.VISIBLE);
              //  holder.doc.setVisibility(View.GONE);
               // Toast.makeText(context, "gone", Toast.LENGTH_SHORT).show();
            }

        }



//      String s=  OthersDocument.docnaam;
//
//        Toast.makeText(context, ""+s, Toast.LENGTH_SHORT).show();
//        holder.sr.setText(arrayList.get(position).getSr()+")");
//        String str=web.get(position).getName();
//        holder.doc.setText(str);
//        if(arrayList.get(position).getStatus().equals("1")) {
//            holder.status.setText("Accepted");
//            holder.status.setTextColor(Color.parseColor("#33cc33"));
//        }
//
//        else if(arrayList.get(position).getStatus().equals("2")) {
//            holder.status.setText("Rejected");
//            holder.status.setTextColor(Color.parseColor("#ff0000"));
//        }
//        else {
//            holder.status.setText("Pending");
//            holder.status.setTextColor(Color.parseColor("#ffffbb33"));
//        }
//
        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // if(String.valueOf(arrayList.get(position).getSr()).equals("1"))
                mCallback.onClick(String.valueOf(serial[position]),"itr");

//        //showFileChooser();
            }
        });

//        if(arrayList.get(position).getDate().equals("")){
//            holder.date.setVisibility(View.GONE);
//            holder.status.setVisibility(View.GONE);
//            holder.tview.setVisibility(View.GONE);
//            holder.iv1.setVisibility(View.GONE);
//            holder.e.setVisibility(View.GONE);
//            holder.mobile.setVisibility(View.GONE);
//            holder.btn.setVisibility(View.VISIBLE);
//        }


    }


//    private void showFileChooser() {
//        Intent intent = new Intent();
//        intent.setType("application/pdf");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        ((Activity)context).startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST);
//    }



    @Override
    public int getItemCount() {
       // return arrayList.size();
        return web.length;
    }
    @Override
    public long getItemId(int position) {
        Log.e("position",String.valueOf(position));
       // Toast.makeText(context, ""+String.valueOf(position), Toast.LENGTH_SHORT).show();
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView doc,date,status,tview,sr,e,mobile;
        ImageView iv1;
        LinearLayout l1;

        Button btn,btn1;

        public MyViewHolder(final View itemView) {
            super(itemView);
            doc=itemView.findViewById(R.id.title);
            sr=itemView.findViewById(R.id.sr);
            date=itemView.findViewById(R.id.email);

            tview=itemView.findViewById(R.id.tview);
            e=itemView.findViewById(R.id.e);
            mobile=itemView.findViewById(R.id.mobile);
            status=itemView.findViewById(R.id.id);
            iv1=itemView.findViewById(R.id.imageView);
            l1=itemView.findViewById(R.id.ll);
            btn=(Button)itemView.findViewById(R.id.btn);
            btn1=(Button)itemView.findViewById(R.id.btn1);

            tview.setPaintFlags(tview.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            l1.setOnClickListener(this);
            //   c1.setOnClickListener(this);



       /* c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = getAdapterPosition();
                String str = arrayList.get(pos).getId();
                Toast.makeText(v.getContext(), ""+str, Toast.LENGTH_SHORT).show();
            }
        });*/

        }


        @Override
        public void onClick(View v) {
            int pos2 = getAdapterPosition();
            // String a2= arrayList.get(pos2).getSr();

            //   Toast.makeText(context, ""+a2, Toast.LENGTH_SHORT).show();


        }
    }



}
