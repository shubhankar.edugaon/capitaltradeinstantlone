package com.business.capitaltrade;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class PermissionRequest extends BaseActivity {
SessionManager session;
Button b1;
CheckBox check1;
    private PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        if (prefManager.isFirstTimeLaunchPermission() ==true) {
              //Toast.makeText(this, "first time", Toast.LENGTH_SHORT).show();
        }
        else
        {
            startActivity(new Intent(PermissionRequest.this,LoginActivity.class));
            // Toast.makeText(this, "second time", Toast.LENGTH_SHORT).show();
             finish();
        }
        setContentView(R.layout.activity_permission_request);

        session = new SessionManager(PermissionRequest.this);
        check1 =(CheckBox)findViewById(R.id.check1);

        changeLanguage();
      //  b1=(Button)findViewById(R.id.footer_text);

//        if (session.isLoggedIn()) {
//            // User is already logged in. Take him to main activity
//            requestStoragePermission();
//
//        }
//        else
//        {
//            Toast.makeText(this, "else part", Toast.LENGTH_SHORT).show();
//          ///  requestStoragePermission();
//            //requestStoragePermission1();
//        }




//        b1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }


    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                           // Toast.makeText(PermissionRequest.this, "called", Toast.LENGTH_SHORT).show();
                            prefManager.setFirstTimeLaunchPermission(false);
                            Intent intent = new Intent(PermissionRequest.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                            // Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            showSettingsDialog();
                          //  Toast.makeText(PermissionRequest.this, "wrong", Toast.LENGTH_SHORT).show();
                        }


                        // check for permanent denial of any permission
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            // show alert dialog navigating to Settings
//                           // Toast.makeText(PermissionRequest.this, "denied", Toast.LENGTH_SHORT).show();
//                            showSettingsDialog();
//                        }



                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PermissionRequest.this,R.style.AppCompatAlertDialogStyle1);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void disagree(View view) {

        showSettingsDialog();
//        if(check1.isChecked())
//        {
//            Toast.makeText(this, "checked", Toast.LENGTH_SHORT).show();
//        }
//        else {
//            Toast.makeText(this, "Please agree Privacy Policy", Toast.LENGTH_SHORT).show();
//        }

    }

    public void agree(View view) {
        if(check1.isChecked())
        {

            requestStoragePermission();
           // prefManager.setFirstTimeLaunchPermission(false);
//            startActivity(new Intent(PermissionRequest.this,LoginActivity.class));
//            finish();
           // Toast.makeText(this, "checked", Toast.LENGTH_SHORT).show();
        }
        else {

            Toast.makeText(this, "Please agree Privacy Policy", Toast.LENGTH_SHORT).show();
        }

    }
}
