package com.business.capitaltrade;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

;

import java.util.ArrayList;

public class Recyclertwo extends RecyclerView.Adapter<Recyclertwo.MyViewHolder> {

    ArrayList<Model2> arrayList;
    Context context;
    int j=1;
    LayoutInflater inflater;
    Dialog mydialog;
    int serial=1;
    public Recyclertwo()
    {

    }

    public Recyclertwo(ArrayList<Model2> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v=inflater.inflate(R.layout.showemi,parent,false);
        Recyclertwo.MyViewHolder myViewHolder = new Recyclertwo.MyViewHolder(v);
        mydialog=new Dialog(context);
        mydialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mydialog.setContentView(R.layout.showemidialog);

        return myViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Model2 model = arrayList.get(position);
       if(arrayList.get(position).getCode().equals("Available")) {

           holder.emino.setText(arrayList.get(position).getSr() + " EMI ");
         //  holder.emino.setText(String.valueOf(ordinal(serial)));
           holder.duedate.setText("Due Date - " + arrayList.get(position).getDuedate());
           holder.tamount.setText("\u20B9 " + arrayList.get(position).getEmiamnt());
           // ((Activity)context).finish();
        /*holder.title.setText(arrayList.get(position).getName());
        holder.email.setText(arrayList.get(position).getDate());
        holder.id.setText(arrayList.get(position).getCls());
        holder.service.setText(arrayList.get(position).getSection());
*/

           if (arrayList.get(position).getEmistatus().equals("Unpaid")) {
//            //holder.circle.setVisibility(View.GONE);
//            holder.paidcircle.setVisibility(View.VISIBLE);
//           String s= String.valueOf(holder.getAdapterPosition()+1);
//            Toast.makeText(context, ""+s, Toast.LENGTH_SHORT).show();

               if (j == 1) {
                   holder.btnpay.setVisibility(View.VISIBLE);
                   holder.btnpay.setText(arrayList.get(position).getEmistatus());
                   final int sdk = android.os.Build.VERSION.SDK_INT;
                   if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                       holder.btnpay.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.btnunpaid) );
                   } else {
                       holder.btnpay.setBackground(ContextCompat.getDrawable(context, R.drawable.btnunpaid));
                   }
                   j++;
               }


           }
           else {
               holder.paidcircle.setVisibility(View.VISIBLE);
               holder.btnpay.setVisibility(View.VISIBLE);
               holder.btnpay.setText(arrayList.get(position).getEmistatus());
               final int sdk = android.os.Build.VERSION.SDK_INT;
               if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                   holder.btnpay.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.btnpay) );
               } else {
                   holder.btnpay.setBackground(ContextCompat.getDrawable(context, R.drawable.btnpay));
               }

           }

           if (position == (getItemCount() - 1)) {
               // here goes some code
               //  callback.sendMessage(Message);
               holder.line1.setVisibility(View.GONE);
               setMargins(holder.btnpay, 115, 5, 0, 0);
           }

           holder.r1.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   if (arrayList.get(position).getEmistatus().equals("Paid") || holder.btnpay.getVisibility() == View.VISIBLE) {
                       WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                       Display display = wm.getDefaultDisplay();
                       Point size = new Point();
                       display.getSize(size);
                       int DialogWidth;
                       int DialogHeight;
                       //   DialogWidth = (int) (size.x * 0.95);
                       DialogWidth = WindowManager.LayoutParams.MATCH_PARENT;
                       ;
                       DialogHeight = WindowManager.LayoutParams.WRAP_CONTENT;
                       mydialog.getWindow().setLayout(DialogWidth, DialogHeight);
                       mydialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                       TextView offeramount = (TextView) mydialog.findViewById(R.id.offeramount);
                       TextView rateofint = (TextView) mydialog.findViewById(R.id.rateofint);
                       TextView ftenure = (TextView) mydialog.findViewById(R.id.ftenure);
                       TextView fmanager = (TextView) mydialog.findViewById(R.id.fmanager);
                       //   TextView fstatus = (TextView) mydialog.findViewById(R.id.fstatus);
                       TextView fstatus1 = (TextView) mydialog.findViewById(R.id.fstatus1);

                       offeramount.setText(arrayList.get(position).getPrincipal());
                       rateofint.setText(arrayList.get(position).getInterest());
                       ftenure.setText(arrayList.get(position).getBalance());
                       fmanager.setText(arrayList.get(position).getPanalty());
                       fstatus1.setText(arrayList.get(position).getFixpanalty());
                       mydialog.show();
                   }
               }
           });
           serial ++;
       }


       else
       {
           holder.r1.setVisibility(View.GONE);
           Toast.makeText(context, "No EMI is Available", Toast.LENGTH_SHORT).show();
       }
    }
    public static String ordinal(int i) {
        String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + sufixes[i % 10];

        }
    }
    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView emino,duedate,tamount;
        ImageView img;
        CardView c1;
        LinearLayout line1;
Button btnpay;
        View circle,paidcircle;
        RelativeLayout r1;

        public MyViewHolder(final View itemView) {
            super(itemView);
            circle= itemView.findViewById(R.id.circle);
            line1= itemView.findViewById(R.id.line1);
            emino=itemView.findViewById(R.id.emino);
            duedate=itemView.findViewById(R.id.duedate);
            btnpay=itemView.findViewById(R.id.btnpay);
            paidcircle=itemView.findViewById(R.id.paidcircle);
            tamount=itemView.findViewById(R.id.tamount);
            r1=itemView.findViewById(R.id.relative);
         //   c1.setOnClickListener(this);



       /* c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = getAdapterPosition();
                String str = arrayList.get(pos).getId();
                Toast.makeText(v.getContext(), ""+str, Toast.LENGTH_SHORT).show();
            }
        });*/

        }


        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
           // String a= arrayList.get(pos).getId();

        }
    }
}
