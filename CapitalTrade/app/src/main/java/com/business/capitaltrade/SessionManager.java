package com.business.capitaltrade;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;


public class SessionManager {
   // Shared Preferences
   SharedPreferences pref;

   // Editor for Shared preferences
   Editor editor;

   // Context
   Context _context;

   // Shared pref mode
   int PRIVATE_MODE = 0;


   // Sharedpref file name
   private static final String PREF_NAME = "CTL";

   // All Shared Preferences Keys
   private static final String IS_LOGIN = "IsLoggedIn";

   // User name (make variable public to access from outside)
   public static final String KEY_CODE = "code";

   // Email address (make variable public to access from outside)
   public static final String KEY_ID = "id";


   // Email address (make variable public to access from outside)
  // public static final String KEY_EMAIL = "email";

    // Mobile (make variable public to access from outside)
    public static final String KEY_MOBILE = "mobile";


    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_DOB = "dob";
    public static final String KEY_RELIGION = "religion";
    public static final String KEY_QUALIFICATION = "qualification";
    public static final String KEY_MARTIAL = "martial";
    public static final String KEY_FATHERNAME = "fathername";
    public static final String KEY_SPOUSENAME = "spousename";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_STATE = "state";
    public static final String KEY_CITY = "city";
    public static final String KEY_PINCODE = "pincode";
    public static final String KEY_LANDMARK = "landmark";



    public static final String KEY_EMPSTATUS = "empstatus";
    public static final String KEY_CNAME = "cname";
    public static final String KEY_CADDRESS = "caddress";
    public static final String KEY_CDESIGNATION = "cdesignation";
    public static final String KEY_CWORKINGYEAR = "cworkingyear";
    public static final String KEY_SALARYMODE = "salarymode";
    public static final String KEY_MONTHLYSALARY = "monthlysalary";
    public static final String KEY_EMPBADGE = "empbadge";
    public static final String KEY_PF = "pf";
    public static final String KEY_ANNUAL = "annual";
    public static final String KEY_GSTIN = "gstin";
    public static final String KEY_PAN = "pan";


    public static final String KEY_BANKNAME = "bankname";
    public static final String KEY_ACNUMBER = "acnumber";
    public static final String KEY_IFSC = "ifsc";
    public static final String KEY_ACNAME = "acname";

   // Constructor
   public SessionManager(Context context){
       this._context = context;
       pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
       editor = pref.edit();
   }


   /**
    * Create login session
    * */
   public void createpersonalinfo(String name,String email,String gender,String dob,String religion,String qualification,
                                  String martial,String fathername,String spousename,String address,String state,String city,
                                  String pincode, String landmark)
   {

       // Storing login value as TRUE
     //  editor.putBoolean(IS_LOGIN, true);

       // Storing name in pref
    //   editor.putString(KEY_NAME, name);

       // Storing id in pref
       editor.putString(KEY_NAME, name);

       // Storing email in pref
       editor.putString(KEY_EMAIL, email);

       // Storing mobile in pref
       editor.putString(KEY_GENDER, gender);

       editor.putString(KEY_DOB, dob);

       editor.putString(KEY_RELIGION, religion);

       editor.putString(KEY_QUALIFICATION, qualification);

       editor.putString(KEY_MARTIAL, martial);

       editor.putString(KEY_FATHERNAME, fathername);

       editor.putString(KEY_SPOUSENAME, spousename);

       editor.putString(KEY_ADDRESS, address);

       editor.putString(KEY_STATE, state);

       editor.putString(KEY_CITY, city);

       editor.putString(KEY_PINCODE, pincode);

       editor.putString(KEY_LANDMARK, landmark);

       // commit changes
       editor.commit();

   }



    public void createcompanyinfo(String status,String name,String address,String designation,String workingyear,String salarymode,String monthlysalary,
                                   String empbadge,String pf,String annualsalary,String gstin,String pan)
    {

        // Storing login value as TRUE
       // editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        //   editor.putString(KEY_NAME, name);

        // Storing id in pref
        editor.putString(KEY_EMPSTATUS, status);

        editor.putString(KEY_CNAME, name);

        editor.putString(KEY_CADDRESS, address);
        // Storing email in pref
        editor.putString(KEY_CDESIGNATION, designation);

        // Storing mobile in pref
        editor.putString(KEY_CWORKINGYEAR, workingyear);

        editor.putString(KEY_SALARYMODE, salarymode);

        editor.putString(KEY_MONTHLYSALARY, monthlysalary);

        editor.putString(KEY_EMPBADGE, empbadge);

        editor.putString(KEY_PF, pf);

        editor.putString(KEY_ANNUAL, annualsalary);

        editor.putString(KEY_GSTIN, gstin);

        editor.putString(KEY_PAN, pan);

        // commit changes
        editor.commit();

    }



    public void createbankinfo(String bname,String acno,String ifsc,String acname)
    {

        // Storing login value as TRUE
       // editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        //   editor.putString(KEY_NAME, name);

        // Storing id in pref
        editor.putString(KEY_BANKNAME, bname);

        // Storing mobile in pref
        editor.putString(KEY_ACNUMBER, acno);

        editor.putString(KEY_IFSC, ifsc);

        editor.putString(KEY_ACNAME, acname);

        // commit changes
        editor.commit();
    }


    public void createLoginSession(String id,String mobile,String code){

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        //   editor.putString(KEY_NAME, name);

        // Storing id in pref
        editor.putString(KEY_ID, id);

        // Storing email in pref
        //  editor.putString(KEY_EMAIL, email);

        // Storing mobile in pref
        editor.putString(KEY_MOBILE, mobile);

        editor.putString(KEY_CODE, code);

        // commit changes
        editor.commit();
    }



   /**
    * Check login method wil check user login status
    * If false it will redirect user to login page
    * Else won't do anything
    * */
   public void checkLogin(){
       // Check login status
       if(!this.isLoggedIn()){
           // user is not logged in redirect him to Login Activity
           Intent i = new Intent(_context, LoginActivity.class);
           // Closing all the Activities
           i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

           // Add new Flag to start new Activity
           i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

           // Staring Login Activity
           _context.startActivity(i);
       }

   }



   /**
    * Get stored session data
    * */



   public HashMap<String, String> getUserDetails(){

       HashMap<String, String> user = new HashMap<String, String>();
       // user name
     //  user.put(KEY_NAME, pref.getString(KEY_NAME, null));

       // user id
       user.put(KEY_ID, pref.getString(KEY_ID, null));

       // user email id
     //  user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

       // user  mobile
       user.put(KEY_MOBILE, pref.getString(KEY_MOBILE, null));

       user.put(KEY_CODE, pref.getString(KEY_CODE, null));

       // return user
       return user;
   }


    public HashMap<String, String> getPersonalDetails(){

        HashMap<String, String> user1 = new HashMap<String, String>();
        // user name
        //  user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        // user id
        user1.put(KEY_NAME, pref.getString(KEY_NAME, null));

        // user email id
          user1.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));


        // user  mobile
        user1.put(KEY_GENDER, pref.getString(KEY_GENDER, null));

        user1.put(KEY_DOB, pref.getString(KEY_DOB, null));

        user1.put(KEY_RELIGION, pref.getString(KEY_RELIGION, null));

        user1.put(KEY_QUALIFICATION, pref.getString(KEY_QUALIFICATION, null));

        user1.put(KEY_MARTIAL, pref.getString(KEY_MARTIAL, null));

        user1.put(KEY_FATHERNAME, pref.getString(KEY_FATHERNAME, null));

        user1.put(KEY_SPOUSENAME, pref.getString(KEY_SPOUSENAME, null));

        user1.put(KEY_ADDRESS, pref.getString(KEY_ADDRESS, null));

        user1.put(KEY_STATE, pref.getString(KEY_STATE, null));

        user1.put(KEY_CITY, pref.getString(KEY_CITY, null));

        user1.put(KEY_PINCODE, pref.getString(KEY_PINCODE, null));


        user1.put(KEY_ADDRESS, pref.getString(KEY_ADDRESS, null));

        user1.put(KEY_LANDMARK, pref.getString(KEY_LANDMARK, null));


        // return user
        return user1;
    }


    public HashMap<String, String> getCompanyinformation(){

        HashMap<String, String> user2 = new HashMap<String, String>();
        // user name
        //  user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        // user id
        user2.put(KEY_EMPSTATUS, pref.getString(KEY_EMPSTATUS, null));

        // user email id
        user2.put(KEY_CNAME, pref.getString(KEY_CNAME, null));


        // user  mobile
        user2.put(KEY_CADDRESS, pref.getString(KEY_CADDRESS, null));

        user2.put(KEY_CDESIGNATION, pref.getString(KEY_CDESIGNATION, null));

        user2.put(KEY_CWORKINGYEAR, pref.getString(KEY_CWORKINGYEAR, null));

        user2.put(KEY_SALARYMODE, pref.getString(KEY_SALARYMODE, null));

        user2.put(KEY_MONTHLYSALARY, pref.getString(KEY_MONTHLYSALARY, null));

        user2.put(KEY_EMPBADGE, pref.getString(KEY_EMPBADGE, null));

        user2.put(KEY_PF, pref.getString(KEY_PF, null));

        user2.put(KEY_ANNUAL, pref.getString(KEY_ANNUAL, null));

        user2.put(KEY_GSTIN, pref.getString(KEY_GSTIN, null));

        user2.put(KEY_PAN, pref.getString(KEY_PAN, null));

        // return user
        return user2;
    }


    public HashMap<String, String> getbankinformation(){

        HashMap<String, String> user3 = new HashMap<String, String>();
        // user name
        //  user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        // user id
        user3.put(KEY_BANKNAME, pref.getString(KEY_BANKNAME, null));

        // user email id
        user3.put(KEY_ACNUMBER, pref.getString(KEY_ACNUMBER, null));


        // user  mobile
        user3.put(KEY_IFSC, pref.getString(KEY_IFSC, null));

        user3.put(KEY_ACNAME, pref.getString(KEY_ACNAME, null));


        // return user
        return user3;
    }

    /**
    * Clear session details
    * */
   public void logoutUser(){
       // Clearing all data from Shared Preferences
       editor.clear();
       editor.commit();

       // After logout redirect user to Loing Activity
       Intent i = new Intent(_context, LoginActivity.class);
       // Closing all the Activities
       i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

       i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
       // Add new Flag to start new Activity
       i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

       // Staring Login Activity
       _context.startActivity(i);
   }

   /**
    * Quick check for login
    * **/
   // Get Login State
   public boolean isLoggedIn()
   {
       return pref.getBoolean(IS_LOGIN, false);
   }
}