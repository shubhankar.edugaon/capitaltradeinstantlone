package com.business.capitaltrade;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoOFLoans extends BaseFragment {
    ArrayList<ModelNoLoan> ar = new ArrayList<ModelNoLoan>();
    RequestQueue mRequest;
    // SessionManager session;
    RecyclerView r1;
    RecyclerNoofLoan rec;
    ProgressDialog progressDialog;
    SessionManager session;
    String id="",code="",sr="",borrreqloanid="",borrid="",pname="",lamnt="",tenure="",roi="",loanno="",ddate="";

    public NoOFLoans() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Disbursed Loan's");
       View v= inflater.inflate(R.layout.fragment_no_ofloans, container, false);
        mRequest = Volley.newRequestQueue(getContext().getApplicationContext());

        r1 = (RecyclerView)v.findViewById(R.id.recyclerloan);
        session = new SessionManager(getContext());
        HashMap<String, String> user = session.getUserDetails();
        id = user.get(SessionManager.KEY_ID);
        if(isNetworkAvailable(getActivity())) {
            getvolley();
        }
        else
        {
            Toast.makeText(getContext(), "No Internet Available", Toast.LENGTH_SHORT).show();

        }

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //  Log.i(tag, "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {

                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("fragmentNumber",1);
                    startActivity(i);
                    ((Activity) getActivity()).overridePendingTransition(0, 0);
//                    FragemtDasboard e1 = new FragemtDasboard();
//                    FragmentTransaction manager = getFragmentManager().beginTransaction();
//                    manager.replace(R.id.fragment_container, e1);
//                    // manager.addToBackStack(null);
//                    manager.commit();

                    return true;
                }
                return false;
            }
        });
       return v;
    }
    private void getvolley() {
        String url = "https://onistech.in/capitaltrade/android/noofloan.php?id="+id+"";
        // String url = "http://bollywoodcity.in/event/index.php?start_date="+date[0]+"&end_date="+date1[0]+"&start_time="+time[0]+"&end_time="+time1[0]+"";
        //String url = "http://bollywoodcity.in/event/index.php?date="+date[0]+"&date2="+date1[0]+"&start_time="+time[0]+"&end_time="+time1[0]+"";
        StringRequest stringRequest =  new StringRequest(Request.Method.POST, UrlUtils.repayment(id), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    ar.clear();
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String success = jsonObject.getString("success");
                        //String status = jsonObject.getString("status");

                        if (success.equals(0)) {
                            session.logoutUser();
                            Toast.makeText(getActivity(), "Sorry, sessionn time out!!!", Toast.LENGTH_LONG).show();

                        } else {
                            code = jsonObject.getString("code");
                            borrreqloanid = jsonObject.getString("borrreqloanid");
                            borrid = jsonObject.getString("borrid");
                            pname = jsonObject.getString("pname");
                            lamnt = jsonObject.getString("lamnt");
                            tenure = jsonObject.getString("tenure");
                            roi = jsonObject.getString("roi");
                            loanno = jsonObject.getString("loanno");
                            ddate = jsonObject.getString("ddate");
//
                            //   Toast.makeText(getContext(), ""+code, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+borrreqloanid, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+borrid, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+pname, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+lamnt, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+tenure, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+roi, Toast.LENGTH_SHORT).show();

                            rec = new RecyclerNoofLoan(Ali(), getContext(), communication);
                            r1.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            r1.setAdapter(rec);
                        }

                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();

                     //Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                Toast.makeText(getContext(),"Sorry ,Something went Wrong Or No Connection !! ", Toast.LENGTH_SHORT).show();
                // progressDialog.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                // params.put("Authorizations", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FuZHJvaWRcLyIsImF1ZCI6Imh0dHA6XC9cL2xvY2FsaG9zdFwvYW5kcm9pZFwvIiwiaWF0IjoxNjQwMTQyMDg4LCJleHAiOjE2NDAxNDU2ODgsImRhdGEiOnsiYm9ycm93ZXJfaWQiOiIxMzA0MSJ9fQ.cYCdfE28XxwWkmBJki0BZfb7jHP2GLgaz7ZcxPm87JQ");
                return params;

            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getContext(),R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Fetching Loan Order....");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public ArrayList<ModelNoLoan> Ali() {
        ModelNoLoan m1 = new ModelNoLoan(code, sr, borrreqloanid, borrid,pname,lamnt,tenure,roi,loanno,ddate);
        ar.add(m1);
        return ar;

    }

    FragmentCommunication communication=new FragmentCommunication() {

        @Override
        public void respond(int position, String id) {
            LoanList fragmentB = new LoanList();
            Bundle bundle = new Bundle();
            bundle.putString("ID",id);
            fragmentB.setArguments(bundle);
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.fragment_container, fragmentB).commit();
        }

    };

}
