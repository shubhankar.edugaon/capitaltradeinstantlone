package com.business.capitaltrade;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity {
SessionManager session;
String sesid="",mob="",name="",email="";
    String value="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

     //   getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FragemtDasboard()).commit();
        //   BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);
        // bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        setupNavigationView();

        //headerView.findViewById(R.id.name);
        session = new SessionManager(getApplicationContext());
        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */
        session.checkLogin();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

      // name = user.get(SessionManager.KEY_NAME);

        // email
        sesid = user.get(SessionManager.KEY_ID);
        mob = user.get(SessionManager.KEY_MOBILE);

       // email = user.get(SessionManager.KEY_EMAIL);
//        Toast.makeText(this, ""+name, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, ""+sesid, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, ""+mob, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, ""+email, Toast.LENGTH_SHORT).show();

//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            startActivityForResult(intent, 100);
      //  Toast.makeText(this, "1", Toast.LENGTH_SHORT).show();
//        FragemtDasboard.note.getView().setVisibility(View.GONE);
//        FragemtDasboard.note.getView.setVisibility(View.GONE);



        if(getIntent().getIntExtra("fragmentNumber",0)==1){
            //finish();
          //  Toast.makeText(this, "aman", Toast.LENGTH_SHORT).show();
        }
        else if(getIntent().getIntExtra("fragmentNumber",0)==2)
        {
            final Dialog dialog = new Dialog(MainActivity.this);
            dialog.setCancelable(true);

            //  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            View view1 = getLayoutInflater().inflate(R.layout.afterloginpopup, null);
            dialog.setContentView(view1);
            Button start = (Button) dialog.findViewById(R.id.start);
          //  dialog.getWindow().setLayout(650, 750);
            dialog.setCancelable(false);
            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
        else {
           // Toast.makeText(this, "gupta", Toast.LENGTH_SHORT).show();
            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragemtDasboard()).commit();
        }

//        Bundle extras = getIntent().getExtras();
//
//        if (extras != null) {
//try {
//    value = extras.getString("key");
//    if (value.equals("open")) {
//        final Dialog dialog = new Dialog(MainActivity.this);
//        dialog.setCancelable(true);
//
//        //  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        View view1 = getLayoutInflater().inflate(R.layout.afterloginpopup, null);
//        dialog.setContentView(view1);
//        Button start = (Button) dialog.findViewById(R.id.start);
//        dialog.getWindow().setLayout(650, 750);
//        dialog.setCancelable(false);
//        start.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//    } else {
//
//    }
//}
//catch (Exception e)
//{
//    Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
//}
//          //  Toast.makeText(MainActivity.this, ""+value, Toast.LENGTH_SHORT).show();
//        }
//        else
//        {
//
//            //Toast.makeText(MainActivity.this, "nothing will happen", Toast.LENGTH_SHORT).show();
//        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id)
        {
            case R.id.logout:
                // Clear the session data
                // This will clear all session data and
                // redirect user to LoginActivity
                session.logoutUser();
                finish();
                break;

            case R.id.changepass:
                // Clear the session data
                // This will clear all session data and
                // redirect user to LoginActivity
               startActivity(new Intent(MainActivity.this,ChangePassword.class));
                finish();
                break;
            case R.id.permission:
                // Clear the session data
                // This will clear all session data and
                // redirect user to LoginActivity
                 startActivity(new Intent(MainActivity.this, Permission.class));
                // finish();
                break;
        }
        return true;

    }


    private void setupNavigationView() {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        if (bottomNavigationView != null) {

            // Select first menu item by default and show Fragment accordingly.
            Menu menu = bottomNavigationView.getMenu();
            selectFragment(menu.getItem(0));

            // Set action to perform when any menu-item is selected.
            bottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            selectFragment(item);
                            return false;
                        }
                    });
        }
    }

        protected void selectFragment(MenuItem item) {

        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.home:
                // Action to perform when Home Menu item is selected.
                pushFragment(new FragemtDasboard());
                break;
            case R.id.action_bag:
              //  Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new NoOFLoans(),"loanno").commit();
              //  pushFragment(new FragemtDasboard());
                // Action to perform when Bag Menu item is selected.
                //  pushFragment(new BagFragment());
                break;
            case R.id.userpro:
                //Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new Profile()).commit();
                // Action to perform when Account Menu item is selected.
                //   pushFragment(new AccountFragment());
                break;

            case R.id.contact:
                //Toast.makeText(this, "work is under progress", Toast.LENGTH_SHORT).show();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new ContactUs()).commit();
                // Action to perform when Account Menu item is selected.
                //   pushFragment(new AccountFragment());
                break;
        }

    }


    /**
     * Method to push any fragment into given id.
     *
     * @param fragment An instance of Fragment to show into the given id.
     */
    protected void pushFragment(Fragment fragment) {
        if (fragment == null)
            return;

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (transaction != null) {
                transaction.replace(R.id.fragment_container, fragment,"aman");
                transaction.commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
      //  MyApplication.getInstance().setConnectivityListener(MainActivity.this);
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */

}
