package com.business.capitaltrade;

public class Model2 {

    String emiid,borrreqloanid,borrid,emiamnt,principal,interest,balance,duedate,panalty,fixpanalty,emistatus,depositedate,sr,code;

    public Model2() {
    }

    public String getEmiid() {
        return emiid;
    }

    public void setEmiid(String emiid) {
        this.emiid = emiid;
    }

    public String getBorrreqloanid() {
        return borrreqloanid;
    }

    public void setBorrreqloanid(String borrreqloanid) {
        this.borrreqloanid = borrreqloanid;
    }

    public String getBorrid() {
        return borrid;
    }

    public void setBorrid(String borrid) {
        this.borrid = borrid;
    }

    public String getEmiamnt() {
        return emiamnt;
    }

    public void setEmiamnt(String emiamnt) {
        this.emiamnt = emiamnt;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getPanalty() {
        return panalty;
    }

    public void setPanalty(String panalty) {
        this.panalty = panalty;
    }

    public String getFixpanalty() {
        return fixpanalty;
    }

    public void setFixpanalty(String fixpanalty) {
        this.fixpanalty = fixpanalty;
    }

    public String getEmistatus() {
        return emistatus;
    }

    public void setEmistatus(String emistatus) {
        this.emistatus = emistatus;
    }

    public String getDepositedate() {
        return depositedate;
    }

    public void setDepositedate(String depositedate) {
        this.depositedate = depositedate;
    }

    public String getSr() {
        return sr;
    }

    public void setSr(String sr) {
        this.sr = sr;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Model2(String emiid, String borrreqloanid, String borrid, String emiamnt, String principal, String interest, String balance
    , String duedate, String panalty, String fixpanalty, String emistatus, String depositedate, String sr, String code) {
        this.emiid = emiid;
        this.borrreqloanid = borrreqloanid;
        this.borrid=borrid;
        this.emiamnt = emiamnt;
        this.principal =principal;
        this.interest =interest;
        this.balance =balance;
        this.duedate =duedate;
        this.panalty =panalty;
        this.fixpanalty =fixpanalty;
        this.emistatus =emistatus;

        this.depositedate =depositedate;
        this.sr =sr;
        this.code=code;

    }
}
