package com.business.capitaltrade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Permission extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.permission);
    }

    public void gotit(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
