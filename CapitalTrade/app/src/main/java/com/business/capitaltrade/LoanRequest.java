package com.business.capitaltrade;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class LoanRequest extends BaseActivity {
ProgressDialog progressDialog;
RelativeLayout relativeLayout;
    EditText e1,e3;
    TextView t1;
    LinearLayout invvalid;
    String amount="",purpose="",tenure="",minint="",maxint="";
    LinearLayout dealerLinear;
    Spinner s1,subspinner,dname;
    ScrollView sc;
    TextView estimate,loan,loanerror;
    SessionManager session;
    AlertDialog alertDialogAndroid;
    String serverurl="https://onistech.in/capitaltrade/android/loanrequest.php";
    String loanfees="https://onistech.in/capitaltrade/android/calcfees.php";
    String otpurl="https://onistech.in/capitaltrade/android/otpApi.php";
    String id="",mob="",loanname="",category="",subloanname="",subloanid="",dealername="",sendDealerid="";
    int loanno=0;
    String actualloanno="";
    String state,subtype,dealerName="";
    final Context c = this;
    HashMap<String, String> states = new HashMap<>();
    HashMap<String, String> subTypes = new HashMap<>();
    HashMap<String, String> dealer = new HashMap<>();
    int stateidint,subtypeint,dealerint;
    String profee="";
    String url ="https://onistech.in/capitaltrade/android/loanpurpose.php" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_request);
        e1=(EditText)findViewById(R.id.amount);
      //  t1=(TextView) findViewById(R.id.t1);
        s1=(Spinner)findViewById(R.id.purpose);
        subspinner=(Spinner)findViewById(R.id.subpurpose);
        dname=(Spinner)findViewById(R.id.dname);
        dealerLinear=(LinearLayout)findViewById(R.id.dealerlinear);
//        t1.s Toast.makeText(LoanRequest.this, "if part", Toast.LENGTH_SHORT).show();etVisibility(View.GONE);
       // s2.setVisibility(View.GONE);
        e3=(EditText)findViewById(R.id.tenure);
        sc=(ScrollView)findViewById(R.id.sc);
        loanerror=(TextView)findViewById(R.id.loanerror);

        loan=(TextView)findViewById(R.id.loan);
    //    invvalid=(LinearLayout)findViewById(R.id.invalid);
        session = new SessionManager(LoanRequest.this);
        HashMap<String, String> user = session.getUserDetails();
        id = user.get(SessionManager.KEY_ID);
        mob = user.get(SessionManager.KEY_MOBILE);

        e1.addTextChangedListener(new NumberTextWatcherForThousand(e1));

        mc();
        //getDealer();
        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @SuppressLint("WrongConstant")
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                loanname = parent.getItemAtPosition(position).toString(); //this is your selected item

                if(loanname.equals("Select Loan"))
                {


                    subspinner.setVisibility(View.GONE);

                }
                else
                {
                            subspinner.setVisibility(View.VISIBLE);
                            category = states.get(loanname);
                            mc1();

                            if(category.equals("2") || category.equals("11") || category.equals("12"))

                            {
                                dealerLinear.setVisibility(View.VISIBLE);
                                dname.setVisibility(View.VISIBLE);
                                getDealer();
                            }

                            else
                            {
                                dealerLinear.setVisibility(View.GONE);
                                dname.setVisibility(View.GONE);
                            }
                 //   Toast.makeText(LoanRequest.this,"the id of the selected category is "+category,Toast.LENGTH_SHORT).show();

                }

            }
           public void onNothingSelected(AdapterView<?> parent)
            {

           }
       });


        subspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                subloanname = parent.getItemAtPosition(position).toString(); //this is your selected item
                //  String selectedItem = parent.getItemAtPosition(position).toString();

                    subloanid = subTypes.get(subloanname);

          //      Toast.makeText(LoanRequest.this, ""+subloanid, Toast.LENGTH_SHORT).show();

            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });


        dname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @SuppressLint("WrongConstant")
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                dealername = parent.getItemAtPosition(position).toString(); //this is your selected item



                    sendDealerid = dealer.get(dealername);
                    //mc1();
//                if(sendDealerid.equals("0"))
//                {
//                    sendDealerid="";
//                }
//                else
//                {
//
//                }

                   //   Toast.makeText(LoanRequest.this,"the id of the selected category is "+sendDealerid,Toast.LENGTH_SHORT).show();



            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
    }

    private void getdata()
    {
        amount= NumberTextWatcherForThousand.trimCommaOfString(e1.getText().toString().trim());
       //purpose=e2.getText().toString();

       tenure=e3.getText().toString().trim();
      //roi=e4.getText().toString().trim();
                     //  int actualamnt= Integer.parseInt(amount);
       // if(amount<50000)
        if(amount.equals("")) {
            new AlertDialog.Builder(LoanRequest.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill  Amount")
//and some more method calls
                    .show();
        }

        else if(Integer.parseInt(amount)<1000 || Integer.parseInt(amount) > 5000000 )
        {
            loanerror.setText("* Loan amount should be 1000 - 5000000 ");
            loanerror.setVisibility(View.VISIBLE);
            animateView(loanerror);
        }

        else if(loanname.equals("Select Loan"))
        {

            new AlertDialog.Builder(LoanRequest.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please Select Purpose")
//and some more method calls
                    .show();
        }

        else if(subloanname.equals("Select Mobile"))
        {

            new AlertDialog.Builder(LoanRequest.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please Select Mobile name")
//and some more method calls
                    .show();
        }
        else if(tenure.equals(""))
        {
            new AlertDialog.Builder(LoanRequest.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Tenure")
//and some more method calls
                    .show();
        }
        else if(Integer.parseInt(tenure) > 60)
        {
            loanerror.setText("* Tenure can not be greater than 60 Months");
            loanerror.setVisibility(View.VISIBLE);
            animateView(loanerror);

        }

        else
        {
            CalculateFees();
           // showpop();
//            Random rnd = new Random();
//             loanno = 100000 + rnd.nextInt(900000);
//            String loaninstring=String.valueOf(loanno);
//            actualloanno="CTLN"+loaninstring;
//            getvolley();
        }
    }

    private void SendLoanRequest() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.sendLoanRequest(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject= jsonArray.getJSONObject(0);
                            String success = jsonObject.getString("success");

                            if (success.equals("0")) {
                                session.logoutUser();
                                Toast.makeText(LoanRequest.this, "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                String code = jsonObject.getString("code");

                                //Toast.makeText(LoanRequest.this, ""+code, Toast.LENGTH_LONG).show();
                                // String message=jsonObject.getString("message");
                                if (code.equals("request_submitted")) {
                                    String loannum = jsonObject.getString("loanno");
                                    String mob = jsonObject.getString("contact");
                                    String name = jsonObject.getString("bname");
                                    e1.setEnabled(false);
                                    //e2.setEnabled(false);
                                    e3.setEnabled(false);
                                    //   e4.setEnabled(false);

                                    LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                                    View mView = layoutInflaterAndroid.inflate(R.layout.prompts, null);
                                    final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                                    alertDialogBuilderUserInput.setView(mView);

                                    final TextView userInputDialogEditText = (TextView) mView.findViewById(R.id.userInputDialog);
                                    userInputDialogEditText.setText("Your Loan request number is " + loannum);
                                    TextView ok = (TextView) mView.findViewById(R.id.ok);

                                    ok.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            alertDialogAndroid.cancel();
                                            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragemtDasboard()).commit();
                                            Intent intent = new Intent(LoanRequest.this, MainActivity.class);
                                            //intent.putExtra("EXTRA", "openFragment");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.putExtra("fragmentNumber", 1);
                                            //intent.putExtra("childname",s);
                                            startActivity(intent);
                                            finish();
                                        }
                                    });

                                    alertDialogAndroid = alertDialogBuilderUserInput.create();
                                    alertDialogAndroid.setCanceledOnTouchOutside(false);
                                    alertDialogBuilderUserInput.setCancelable(false);
                                    alertDialogBuilderUserInput.setCancelable(false);
                                    alertDialogAndroid.show();

                                    SmsToLender(mob, name, amount);

                                    //  Toast.makeText(LoanRequest.this, ""+message, Toast.LENGTH_LONG).show();
                                } else if (code.equals("wrong")) {
                                    String msg = jsonObject.getString("lduration").trim();
                                    String arr[] = msg.split("-");

                                    LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                                    View mView = layoutInflaterAndroid.inflate(R.layout.prompts, null);
                                    final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                                    alertDialogBuilderUserInput.setView(mView);

                                    final TextView userInputDialogEditText = (TextView) mView.findViewById(R.id.userInputDialog);
                                    final TextView text = (TextView) mView.findViewById(R.id.text);
                                    userInputDialogEditText.setText("Unable to process your Loan Request");
                                    text.setText("Reason - Loan Request can not be accepted before 30 Days from the date of your Last Requested Loan.");
                                    TextView ok = (TextView) mView.findViewById(R.id.ok);

                                    ok.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            alertDialogAndroid.cancel();
                                            //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragemtDasboard()).commit();
                                            Intent intent = new Intent(LoanRequest.this, MainActivity.class);
                                            //intent.putExtra("EXTRA", "openFragment");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.putExtra("fragmentNumber", 1);
                                            //intent.putExtra("childname",s);
                                            startActivity(intent);
                                            finish();
                                        }
                                    });

                                    alertDialogAndroid = alertDialogBuilderUserInput.create();
                                    alertDialogAndroid.setCanceledOnTouchOutside(false);
                                    alertDialogBuilderUserInput.setCancelable(false);
                                    alertDialogBuilderUserInput.setCancelable(false);
                                    alertDialogAndroid.show();

                                } else if (code.equals("already")) {
                                    Random rnd = new Random();
                                    loanno = 100000 + rnd.nextInt(900000);
                                    String loaninstring = String.valueOf(loanno);
                                    actualloanno = "CTLN" + loaninstring;
                                    SendLoanRequest();
                                } else if (code.equals("not_submitted")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(LoanRequest.this, "Sorry, Server encountered problem !!!", Toast.LENGTH_LONG).show();
//                                startActivity(new Intent(LoanRequest.this,LoginActivity.class));
//                                finish();
                                }
                            }
                        } catch (JSONException e) {

                            Toast.makeText(LoanRequest.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                            Toast.makeText(LoanRequest.this, "Server encountered some problem !!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoanRequest.this,"Something went wrong !!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("loanno",actualloanno);
                params.put("id",id);
                params.put("purpose",category);
                params.put("subpurpose",subloanid);
                params.put("loanamount",amount);
                params.put("tenure",tenure);
                params.put("dealerid",sendDealerid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(LoanRequest.this);
        requestQueue.add(stringRequest);
        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(LoanRequest.this,R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Taking Request.Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void loanrequest(View view) {
        if(isNetworkAvailable(LoanRequest.this)) {
            getdata();
        }
        else
        {
            Toast.makeText(LoanRequest.this, "No Internet Availble !!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void mc(){

        JsonArrayRequest req=new JsonArrayRequest(UrlUtils.loanPurpose(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try
                {
                   // relativeLayout.setVisibility(View.VISIBLE);
                    progressDialog.dismiss();
                    ArrayList<String> al1 = new ArrayList<String>();
                    ArrayList<String> al2 = new ArrayList<String>();
                            al2.add("0");
                            al1.add("Select Loan");
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject person = (JSONObject) response.get(i);
                        String success = person.getString("success");
                        String status = person.getString("status");

                        if (success.equals("0")) {
                            session.logoutUser();
                            Toast.makeText(LoanRequest.this, "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else {
                            stateidint = person.getInt("loanid");
                            al2.add(String.valueOf(stateidint));

                            state = person.getString("loan");
                            al1.add(state);
                            minint = person.getString("min");
                            maxint = person.getString("max");
                        }

                        chal(al1);
                        bkl(al1,al2);
                       // e4.setText(interest+" % ");
                       // Toast.makeText(c, ""+interest, Toast.LENGTH_SHORT).show();
                       /* if(state.equals("Others"))
                            al1.remove(state);*/
                    }

                }
                catch (JSONException e) {
                    progressDialog.dismiss();


                    Toast.makeText(LoanRequest.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoanRequest.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();

                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(LoanRequest.this);
        requestQueue.add(req);
        progressDialog = new ProgressDialog(LoanRequest.this,R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Loading Services...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    public void chal(ArrayList<String> al)
    {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s1.setAdapter(dataAdapter);
    }

    public void bkl(ArrayList<String> al1,ArrayList<String> al2)
    {
        for(int i = 0;i<al1.size();i++)
            states.put(al1.get(i),al2.get(i));
    }

    private void mc1() {

        String url1 = "https://onistech.in/capitaltrade/android/prosubtype.php?subtypeid=" + category + "";
        JsonArrayRequest req = new JsonArrayRequest(UrlUtils.subLoanPurpose(category), new Response.Listener<JSONArray>() {
            @SuppressLint("ResourceType")
            @Override
            public void onResponse(JSONArray response) {

                try {
                    progressDialog.dismiss();
                    ArrayList<String> al1 = new ArrayList<String>();
                    ArrayList<String> al2 = new ArrayList<String>();
                    al2.add("0");
                    al1.add("Select Mobile");
                    subspinner.setVisibility(View.VISIBLE);
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject person = (JSONObject) response.get(i);

                        String success = person.getString("success");

                        if (success.equals("0")) {
                            session.logoutUser();
                            Toast.makeText(LoanRequest.this, "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else {
                            subtypeint = person.getInt("product_id");
                            //Toast.makeText(c, ""+String.valueOf(subtypeint), Toast.LENGTH_SHORT).show();
                            al2.add(String.valueOf(subtypeint));
                            subtype = person.getString("sub_product_name");
                            al1.add(subtype);
                        }
                        chal1(al1);
                        bkl1(al1, al2);

                        /*if(subtype.equals("All"))
                            al1.remove(subtype);
                       /* if(state.equals("Others"))
                            al1.remove(state);*/
                    }


                } catch (JSONException e) {
                    progressDialog.dismiss();
                    subspinner.setVisibility(View.GONE);

                    //   Toast.makeText(LoanRequest.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoanRequest.this, "Failed to load !!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();

            params.put("Authorizations", "Bearer " + AppSession.logedInToken);
            return params;
        }
    };

        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(LoanRequest.this);
        requestQueue.add(req);
        progressDialog = new ProgressDialog(LoanRequest.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Fetching ...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    public void chal1(ArrayList<String> al)
    {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,R.layout.spinnertext, al);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subspinner.setAdapter(dataAdapter);
//        if (cityvalue !="") {
//            int spinnerPosition = dataAdapter.getPosition(cityvalue);
//            s2.setSelection(spinnerPosition);
//        }
//        else if(cityvalue.equals(""))
//        {
//            // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
//            // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            s2.setAdapter(dataAdapter);
//        }
    }

    public void bkl1(ArrayList<String> al1,ArrayList<String> al2)
    {
        for(int i = 0;i<al1.size();i++)
            subTypes.put(al1.get(i),al2.get(i));
    }





    private void getDealer(){

        JsonArrayRequest req=new JsonArrayRequest(UrlUtils.dealerName(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try
                {
                    // relativeLayout.setVisibility(View.VISIBLE);
                   // progressDialog.dismiss();
                    ArrayList<String> al1 = new ArrayList<String>();
                    ArrayList<String> al2 = new ArrayList<String>();
                    al2.add("0");
                    al1.add("Select Dealer Name");
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject person = (JSONObject) response.get(i);
                        String success = person.getString("success");

                        if (success.equals("0")) {
                            session.logoutUser();
                            Toast.makeText(LoanRequest.this, "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else {
                            dealerint = person.getInt("dealerid");
                            al2.add(String.valueOf(dealerint));
                            dealerName = person.getString("dealername");
                            al1.add(dealerName);
                        }
                        dealerArray(al1);
                        dealerAdd(al1,al2);
                     //   minint  = person.getString("min");
                     //   maxint  = person.getString("max");
                        // e4.setText(interest+" % ");
                        // Toast.makeText(c, ""+interest, Toast.LENGTH_SHORT).show();
                       /* if(state.equals("Others"))
                            al1.remove(state);*/
                    }
                }
                catch (JSONException e) {
                    // dname.setVisibility(View.GONE);
                  //  progressDialog.dismiss();
                   //

                   // Toast.makeText(LoanRequest.this, "Sorry,Server encountered problem !!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(LoanRequest.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(LoanRequest.this);
        requestQueue.add(req);
//        progressDialog = new ProgressDialog(LoanRequest.this,R.style.AppCompatAlertDialogStyle);
//        progressDialog.setMessage("Loading Services...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
    }


    public void dealerArray(ArrayList<String> al)
    {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dname.setAdapter(dataAdapter);
    }

    public void dealerAdd(ArrayList<String> al1,ArrayList<String> al2)
    {
        for(int i = 0;i<al1.size();i++)
            dealer.put(al1.get(i),al2.get(i));
    }





//    public void estimate(View view) {
//        amount= NumberTextWatcherForThousand.trimCommaOfString(e1.getText().toString().trim());
//        tenure=e3.getText().toString().trim();
//       // intrst=e4.getText().toString().trim();
//
//        if(amount.equals("")) {
//            new AlertDialog.Builder(LoanRequest.this)
//                    //.setTitle("Record New Track")
//                    .setMessage("Please fill  Amount")
////and some more method calls
//                    .show();
//        }
//        else if(loanname.equals("Select Loan"))
//        {
//            new AlertDialog.Builder(LoanRequest.this)
//                    //.setTitle("Record New Track")
//                    .setMessage("Please Select Purpose")
////and some more method calls
//                    .show();
//        }
//        else if(tenure.equals(""))
//        {
//            new AlertDialog.Builder(LoanRequest.this)
//                    //.setTitle("Record New Track")
//                    .setMessage("Please fill Tenure")
////and some more method calls
//                    .show();
//        }
//        else
//        {
//            Dialog mydialog;
//            mydialog = new Dialog(LoanRequest.this);
//            mydialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//            // mydialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
//            mydialog.setContentView(R.layout.estimated);
//            WindowManager wm = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);
//            Display display = wm.getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int DialogWidth;
//            int DialogHeight;
//
//            //   DialogWidth = (int) (size.x * 0.95);
//            DialogWidth =  WindowManager.LayoutParams.MATCH_PARENT;;
//            DialogHeight = WindowManager.LayoutParams.WRAP_CONTENT;
//
//            mydialog.getWindow().setLayout(DialogWidth, DialogHeight);
//            mydialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//
//            TextView t1 = (TextView) mydialog.findViewById(R.id.tamount);
//            TextView t2 = (TextView) mydialog.findViewById(R.id.tinterest);
//            TextView t3 = (TextView) mydialog.findViewById(R.id.tpayable);
//            int amt =Integer.parseInt(amount);
//            int month = Integer.parseInt(tenure);
//
//            double roimin = Double.parseDouble(minint) / (12*100) ;
//            double powermin= Math.pow(1+roimin,month);
//            double emimin = (amt*roimin*powermin) / (powermin-1);
//            double valueRoundedmin = Math.round(emimin * 100D) / 100D;
//            DecimalFormat formatter = new DecimalFormat("#,###,###.##");
//            String yourFormattedStringmin = formatter.format(valueRoundedmin);
//
//
//            double roimax = Double.parseDouble(maxint) / (12*100) ;
//            double powermax= Math.pow(1+roimax,month);
//            double emimax = (amt*roimax*powermax) / (powermax-1);
//            double valueRoundedmax = Math.round(emimax * 100D) / 100D;
//            DecimalFormat formatterr = new DecimalFormat("#,###,###.##");
//            String yourFormattedStringmax = formatterr.format(valueRoundedmax);
//
//            t1.setText(yourFormattedStringmin+" - "+yourFormattedStringmax);
//
//
//
//            double tintmin=Math.round(valueRoundedmin*month-amt);
//            String yourinterestmin = formatter.format(tintmin);
//
//            double tintmax=Math.round(valueRoundedmax*month-amt);
//            String yourinterestmax = formatter.format(tintmax);
//
//            t2.setText(yourinterestmin+" - "+yourinterestmax);
//
//
//
//            double tamntmin=amt+tintmin;
//            String yourtamountmin = formatter.format(tamntmin);
//
//            double tamntmax=amt+tintmax;
//            String yourtamountmax = formatter.format(tamntmax);
//            t3.setText(yourtamountmin+" - "+yourtamountmax);
//            mydialog.show();
//        }
//
//    }

    public void animateView(View view){
        Animation shake = AnimationUtils.loadAnimation(LoanRequest.this,R.anim.shake);
        view.startAnimation(shake);
    }
    
    public void showpop(){
        final Dialog mydialog;
        mydialog = new Dialog(LoanRequest.this);
        mydialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        // mydialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        mydialog.setContentView(R.layout.estimated);
        WindowManager wm = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int DialogWidth;
        int DialogHeight;

        //   DialogWidth = (int) (size.x * 0.95);
        DialogWidth =  WindowManager.LayoutParams.MATCH_PARENT;;
        DialogHeight = WindowManager.LayoutParams.WRAP_CONTENT;

        mydialog.getWindow().setLayout(DialogWidth, DialogHeight);
        mydialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        TextView t1 = (TextView) mydialog.findViewById(R.id.tamount);
        TextView t2 = (TextView) mydialog.findViewById(R.id.tinterest);
        TextView t3 = (TextView) mydialog.findViewById(R.id.tpayable);
        TextView t4 = (TextView) mydialog.findViewById(R.id.text1);
        TextView t5 = (TextView) mydialog.findViewById(R.id.pronote);
        Button reject = (Button) mydialog.findViewById(R.id.reject);
        Button accept = (Button) mydialog.findViewById(R.id.accept);
        int amt =Integer.parseInt(amount);
        int month = Integer.parseInt(tenure);

        double roimin = Double.parseDouble(minint) / (12*100) ;
        double powermin= Math.pow(1+roimin,month);
        double emimin = (amt*roimin*powermin) / (powermin-1);
        double valueRoundedmin = Math.round(emimin * 100D) / 100D;
        DecimalFormat formatter = new DecimalFormat("#,###,###.##");
        String yourFormattedStringmin = formatter.format(valueRoundedmin);


        double roimax = Double.parseDouble(maxint) / (12*100) ;
        double powermax= Math.pow(1+roimax,month);
        double emimax = (amt*roimax*powermax) / (powermax-1);
        double valueRoundedmax = Math.round(emimax * 100D) / 100D;
        DecimalFormat formatterr = new DecimalFormat("#,###,###.##");
        String yourFormattedStringmax = formatterr.format(valueRoundedmax);

        t1.setText(yourFormattedStringmin+" - "+yourFormattedStringmax);



        double tintmin=Math.round(valueRoundedmin*month-amt);
        String yourinterestmin = formatter.format(tintmin);

        double tintmax=Math.round(valueRoundedmax*month-amt);
        String yourinterestmax = formatter.format(tintmax);

        t2.setText(yourinterestmin+" - "+yourinterestmax);



        double tamntmin=amt+tintmin;
        String yourtamountmin = formatter.format(tamntmin);

        double tamntmax=amt+tintmax;
        String yourtamountmax = formatter.format(tamntmax);
        t3.setText(yourtamountmin+" - "+yourtamountmax);
        t4.setText("Rs. "+profee+" /-");
        t5.setSelected(true);
        mydialog.setCancelable(false);
        mydialog.show();
 // ===============================Generating Random Number =============================================//
            Random rnd = new Random();
             loanno = 100000 + rnd.nextInt(900000);
            String loaninstring=String.valueOf(loanno);
            actualloanno="CTLN"+loaninstring;
  //=====================================================================================================//
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mydialog.dismiss();
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable(LoanRequest.this)) {
                    SendLoanRequest();
                    mydialog.dismiss();
                }
                else
                {
                    Toast.makeText(LoanRequest.this, "No Internet is Available", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void CalculateFees() {
        StringRequest stringRequest =  new StringRequest(Request.Method.POST, UrlUtils.calcLoanFees(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                    String code = jsonObject.getString("code");
                    //String message = jsonObject.getString("code");
                    // String check = jsonObject.getString("check");//we should write key name

                    //Toast.makeText(SignIn.this, ""+check, Toast.LENGTH_SHORT).show();
                    if(code.equals("available"))
                    {
                         profee = jsonObject.getString("profee");
                        progressDialog.dismiss();
                         showpop();
                    }

                    else if(code.equals("unavailable"))
                    //  else if(code.equals("login_success"))
                    {
                        progressDialog.dismiss();

                            Toast.makeText(LoanRequest.this, "Unable to process Request !!!", Toast.LENGTH_SHORT).show();



                    }

                    else
                    {
                        Toast.makeText(LoanRequest.this, "The server encountered an error processing the request.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {

                    progressDialog.dismiss();
                    Toast.makeText(LoanRequest.this, "Something went wrong !", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoanRequest.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();
                Toast.makeText(LoanRequest.this,"Sorry ,Something went Wrong Or No Connection !! ", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();

                params.put("Authorizations", "Bearer " + AppSession.logedInToken);

                params.put("l_amount",amount);
                //params.put("u_pass",pass);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoanRequest.this);
        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(LoanRequest.this,R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Calculating Fees...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void SmsToLender(String mob, final String name, final String amount){
       // Toast.makeText(this, ""+mob, Toast.LENGTH_SHORT).show();
      //  Toast.makeText(this, ""+name, Toast.LENGTH_SHORT).show();
       final String s = "+91" + mob;
       // otpurl = otpurl.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.otpApi(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {


                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                            String code = jsonObject.getString("code");
                            if (code.equals("sent")) {
                                    Toast.makeText(LoanRequest.this, "Company is notified for your Loan request.", Toast.LENGTH_LONG).show();
                                    //  t1.setText("We've sent an OTP to the mobile number "+s+". Please enter it below to complete verification.");
                                    // startTimer();
                                } else {
                                    //Toast.makeText(LoanRequest.this, "Invalid mobile number !!!", Toast.LENGTH_SHORT).show();
                                }
                                //  Toast.makeText(signup.this, ""+status, Toast.LENGTH_SHORT).show();


                    }
                        catch (Exception e)
                        {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  progressDialog.dismiss();
//
                Toast.makeText(getApplicationContext(), "Something went wrong !!!", Toast.LENGTH_SHORT).show();

            }

        }) {@Override
        protected Map<String, String> getParams() throws AuthFailureError {

            Map<String,String> params = new HashMap<String,String>();
            params.put("u_mobile",s);
            params.put("name",name);
            params.put("amount",amount);
            return params;
        }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
//            //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
//        progressDialog = new ProgressDialog(LoanRequest.this, R.style.AppCompatAlertDialogStyle);
//        progressDialog.setMessage("Sending OTP..");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
    }


}
