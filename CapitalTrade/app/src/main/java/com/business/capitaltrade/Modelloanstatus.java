package com.business.capitaltrade;

public class Modelloanstatus {
    String borrowid,loannum,pname,lamount,tenure,astatus,vstatus,pstatus,roi,status,disburse_status,
    ploanrid,borreqloanid,f_amt,f_roi,f_tenure,f_approve_by,f_status,f_aggreement_status,processing_fee_status,borrower_reply_status,
    empstatus,gpno,bfile,mandate;
    public Modelloanstatus() {
    }

    public String getMandate() {
        return mandate;
    }

    public void setMandate(String mandate) {
        this.mandate = mandate;
    }

    public Modelloanstatus(String borrowid, String loannum, String pname, String lamount, String tenure, String astatus, String vstatus, String pstatus, String roi, String status,
                           String ploanrid, String borreqloanid, String f_amt, String f_roi, String f_tenure, String f_approve_by, String f_status, String f_aggreement_status,
                           String processing_fee_status, String borrower_reply_status, String disburse_status, String empstatus,
                           String gpno, String bfile, String manadate) {
        this.borrowid = borrowid;
        this.loannum = loannum;
        this.pname = pname;
        this.lamount = lamount;
        this.tenure=tenure;
        this.astatus=astatus;
        this.vstatus=vstatus;
        this.pstatus=pstatus;
        this.roi=roi;
        this.status=status;
        this.ploanrid=ploanrid;
        this.borreqloanid=borreqloanid;
        this.f_amt=f_amt;
        this.f_roi=f_roi;
        this.f_tenure=f_tenure;
        this.f_approve_by=f_approve_by;
        this.f_status=f_status;
        this.f_aggreement_status=f_aggreement_status;
        this.processing_fee_status=processing_fee_status;
        this.borrower_reply_status=borrower_reply_status;
        this.disburse_status=disburse_status;
        this.empstatus=empstatus;
        this.gpno=gpno;
        this.bfile=bfile;
        this.mandate=manadate;
    }

    public String getEmpstatus() {
        return empstatus;
    }

    public void setEmpstatus(String empstatus) {
        this.empstatus = empstatus;
    }

    public String getDisburse_status() {
        return disburse_status;
    }

    public void setDisburse_status(String disburse_status) {
        this.disburse_status = disburse_status;
    }

    public String getBorrowid() {
        return borrowid;
    }

    public void setBorrowid(String borrowid) {
        this.borrowid = borrowid;
    }

//    public String getProfee() {
//        return profee;
//    }
//
//    public void setProfee(String profee) {
//        this.profee = profee;
//    }

    public String getLoannum() {
        return loannum;
    }

    public void setLoannum(String loannum) {
        this.loannum = loannum;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getLamount() {
        return lamount;
    }

    public String getPloanrid() {
        return ploanrid;
    }

    public void setPloanrid(String ploanrid) {
        this.ploanrid = ploanrid;
    }

    public String getGpno() {
        return gpno;
    }

    public void setGpno(String gpno) {
        this.gpno = gpno;
    }

    public String getBfile() {
        return bfile;
    }

    public void setBfile(String bfile) {
        this.bfile = bfile;
    }

    public String getBorreqloanid() {
        return borreqloanid;
    }

    public void setBorreqloanid(String borreqloanid) {
        this.borreqloanid = borreqloanid;
    }

    public String getF_amt() {
        return f_amt;
    }

    public void setF_amt(String f_amt) {
        this.f_amt = f_amt;
    }

    public String getF_roi() {
        return f_roi;
    }

    public void setF_roi(String f_roi) {
        this.f_roi = f_roi;
    }

    public String getF_tenure() {
        return f_tenure;
    }

    public void setF_tenure(String f_tenure) {
        this.f_tenure = f_tenure;
    }

    public String getF_approve_by() {
        return f_approve_by;
    }

    public void setF_approve_by(String f_approve_by) {
        this.f_approve_by = f_approve_by;
    }

    public String getF_status() {
        return f_status;
    }

    public void setF_status(String f_status) {
        this.f_status = f_status;
    }

    public String getF_aggreement_status() {
        return f_aggreement_status;
    }

    public void setF_aggreement_status(String f_aggreement_status) {
        this.f_aggreement_status = f_aggreement_status;
    }

    public String getProcessing_fee_status() {
        return processing_fee_status;
    }

    public void setProcessing_fee_status(String processing_fee_status) {
        this.processing_fee_status = processing_fee_status;
    }

    public String getBorrower_reply_status() {
        return borrower_reply_status;
    }

    public void setBorrower_reply_status(String borrower_reply_status) {
        this.borrower_reply_status = borrower_reply_status;
    }

    public void setLamount(String lamount) {
        this.lamount = lamount;
    }

    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    public String getAstatus() {
        return astatus;
    }

    public void setAstatus(String astatus) {
        this.astatus = astatus;
    }

    public String getVstatus() {
        return vstatus;
    }

    public void setVstatus(String vstatus) {
        this.vstatus = vstatus;
    }

    public String getPstatus() {
        return pstatus;
    }

    public void setPstatus(String pstatus) {
        this.pstatus = pstatus;
    }

    public String getRoi() {
        return roi;
    }

    public void setRoi(String roi) {
        this.roi = roi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
