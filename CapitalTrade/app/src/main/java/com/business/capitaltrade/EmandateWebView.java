package com.business.capitaltrade;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class EmandateWebView extends AppCompatActivity {

    String emandate="";
    android.webkit.WebView webView;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emandate_web_view);
        webView =(android.webkit.WebView )findViewById(R.id.webview1);
        //    webView.setInitialScale(1);
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setLoadWithOverviewMode(true);
//        webView.getSettings().setUseWideViewPort(true);
//
//        webView.setScrollBarStyle(android.webkit.WebView.SCROLLBARS_OUTSIDE_OVERLAY);
//        webView.setScrollbarFadingEnabled(false);
//        // webView.setBuiltInZoomControls(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        emandate= getIntent().getStringExtra("link");
        //  Toast.makeText(this, ""+loanId, Toast.LENGTH_SHORT).show();
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        String url=emandate.trim();
        webView.loadUrl(url);



    }
}

