package com.business.capitaltrade;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class BankDetails extends BaseActivity {

    EditText bname,acno,ifscode,acname;
    String bankname="",acnumber="",ifsc="",accountname="",sesid="",banknamevalue="";
    String serverurl="https://onistech.in/capitaltrade/android/bankinfo.php";
    ProgressDialog progressDialog;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        session = new SessionManager(BankDetails.this);
        bname=(EditText)findViewById(R.id.bname);
        acno=(EditText)findViewById(R.id.acno);
        ifscode=(EditText)findViewById(R.id.ifscode);
        acname=(EditText)findViewById(R.id.acname);
        HashMap<String, String> user = session.getUserDetails();
        sesid = user.get(SessionManager.KEY_ID);
       // Toast.makeText(this, ""+sesid, Toast.LENGTH_SHORT).show();

        HashMap<String, String> user3 = session.getbankinformation();

        // name = user.get(SessionManager.KEY_NAME);

        // email
        banknamevalue = user3.get(SessionManager.KEY_BANKNAME);

   //     Toast.makeText(this, ""+banknamevalue, Toast.LENGTH_SHORT).show();

        if(banknamevalue==null || banknamevalue.equals(""))
        {
            // Toast.makeText(this, ""+namevalue, Toast.LENGTH_SHORT).show();
            checkbankinfo();

        }
        else
        {
            acnumber = user3.get(SessionManager.KEY_ACNUMBER);
            ifsc = user3.get(SessionManager.KEY_IFSC);
            accountname = user3.get(SessionManager.KEY_ACNAME);

            bname.setText(banknamevalue);
            acno.setText(acnumber);
            ifscode.setText(ifsc);
            acname.setText(accountname);
        }

    }

    public void submitabnkinfo(View view) {
        if(isNetworkAvailable(getApplicationContext()))
        {
            bankname=bname.getText().toString().trim();
          //  acnumber=acno.getText().toString().trim();
            ifsc=ifscode.getText().toString().trim();
            accountname=acname.getText().toString().trim();

            if(bankname.equals(""))
            {
                new AlertDialog.Builder(BankDetails.this)
                        //.setTitle("Record New Track")
                        .setMessage("Please enter Bank Name")
//and some more method calls
                        .show();
            }
            else if(acno.getText().toString().trim().equals(""))
            {
                new AlertDialog.Builder(BankDetails.this)
                        //.setTitle("Record New Track")
                        .setMessage("Please enter Account Number")
//and some more method calls
                        .show();
            }

            else if(ifsc.equals(""))
            {
                new AlertDialog.Builder(BankDetails.this)
                        //.setTitle("Record New Track")
                        .setMessage("Please enter IFSC code")
//and some more method calls
                        .show();
            }
            else if(accountname.equals(""))
            {
                new AlertDialog.Builder(BankDetails.this)
                        //.setTitle("Record New Track")
                        .setMessage("Please enter Account holder name")
//and some more method calls
                        .show();
            }

            else
            {
                SubmitBankInfo();
            }

        }
        else
        {
            Toast.makeText(this, "No Internet Connection !!!", Toast.LENGTH_SHORT).show();
        }
    }

    public void SubmitBankInfo(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.BankInfo(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject= jsonArray.getJSONObject(0);
                            String success = jsonObject.getString("success");

                            if (success.equals("0")) {
                                session.logoutUser();
                                Toast.makeText(BankDetails.this, "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                String code = jsonObject.getString("code");
                                if (code.equals("request_submitted")) {

                                    session.createbankinfo(bankname, acno.getText().toString().trim(), ifsc, accountname);
                                    //    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragemtDasboard()).commit();
                                    Intent intent = new Intent(BankDetails.this, MainActivity.class);
                                    //intent.putExtra("EXTRA", "openFragment");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("fragmentNumber", 1);
                                    //intent.putExtra("childname",s);
                                    startActivity(intent);
                                    finish();


                                    //  Toast.makeText(LoanRequest.this, ""+message, Toast.LENGTH_LONG).show();
                                } else if (code.equals("not_submitted")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(BankDetails.this, "Sorry,Try Again !!!", Toast.LENGTH_LONG).show();
//                                startActivity(new Intent(LoanRequest.this,LoginActivity.class));
//                                finish();
                                }
                            }
                        } catch (JSONException e) {
                            Toast.makeText(BankDetails.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(BankDetails.this,"Server encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("bname",bankname);
                byte[] data = acno.getText().toString().trim().getBytes(StandardCharsets.UTF_8);
                params.put("acno", Base64.encodeToString(data, Base64.DEFAULT));
                params.put("ifsc",ifsc);
                params.put("acname",accountname);
                params.put("id",sesid);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(BankDetails.this);
        requestQueue.add(stringRequest);
        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(BankDetails.this,R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Submitting Bank Details.Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void checkbankinfo() {
        String urll = "https://onistech.in/capitaltrade/android/showbankinfo.php?id=" + sesid + "";
        ;
        // urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.showBankInfo(sesid),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        // Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            //get value of first index
                            String success = jsonObject.getString("success");


                            if (success.equals("0") ) {
                                session.logoutUser();
                                Toast.makeText(BankDetails.this, "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                if (!success.equals("")) {
                                    String bankname = jsonObject.getString("bname");
                                    String accno = jsonObject.getString("acno");
                                    String accname = jsonObject.getString("acname");
                                    String iffsc = jsonObject.getString("ifsc");
                                    session.createbankinfo(bankname, accno, iffsc, accname);
                                    bname.setText(bankname);
                                    acno.setText(accno);
                                    ifscode.setText(iffsc);
                                    acname.setText(accname);

                                }
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            Toast.makeText(BankDetails.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(BankDetails.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new

            DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            RequestQueue requestQueue = Volley.newRequestQueue(BankDetails.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

            //initialize the progress dialog and show it
            progressDialog =new

            ProgressDialog(BankDetails .this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
