package com.business.capitaltrade;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class ChangePassword extends BaseActivity {

    SessionManager session;
    RelativeLayout relative;
    TextView timer;
    EditText changepassword, changepassword1, e5, oldpass;
    String mobile = "", change = "", s = "", pin = "", id = "", otpValue = "";
    ProgressDialog progressDialog;
    Button sendOTP, submitOTP, fsubmit, sendotp;
    LinearLayout OTPVarifie;
    String changepassurl = "https://onistech.in/capitaltrade/android/changepass.php";
    String otpurl = "https://onistech.in/capitaltrade/android/otpApi.php";
    CountDownTimer cTimer = null;

    public static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{2,}" +               //at least 2 characters
                    "$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        changepassword = (EditText) findViewById(R.id.password);
        changepassword1 = (EditText) findViewById(R.id.password1);
        relative = (RelativeLayout) findViewById(R.id.relative);
        e5 = (EditText) findViewById(R.id.et28);
        oldpass = (EditText) findViewById(R.id.oldPass);
//        e2=(EditText)findViewById(R.id.e2);
//        e3=(EditText)findViewById(R.id.e3);
//        e4=(EditText)findViewById(R.id.e4);
        timer = (TextView) findViewById(R.id.timer);
        fsubmit = (Button) findViewById(R.id.fsubmit);
        sendotp = (Button) findViewById(R.id.sendotp);
        // OTPVarifie=(LinearLayout)findViewById(R.id.OTPVarifie);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        mobile = user.get(SessionManager.KEY_MOBILE);
        //    Toast.makeText(this, ""+mobile, Toast.LENGTH_SHORT).show();
        id = user.get(SessionManager.KEY_ID);
    }

    public void getotp(View view) {
        if (oldpass.getText().toString().trim().equals("")) {
            new AlertDialog.Builder(ChangePassword.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill current password field")
//and some more method calls
                    .show();
        } else if (changepassword.getText().toString().trim().equals("") || changepassword1.getText().toString().trim().equals("")) {
            new AlertDialog.Builder(ChangePassword.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Password Fields")
//and some more method calls
                    .show();
        } else if (!changepassword.getText().toString().trim().equals(changepassword1.getText().toString().trim())) {
            new AlertDialog.Builder(ChangePassword.this)
                    //.setTitle("Record New Track")
                    .setMessage("Password doesn't match")
//and some more method calls
                    .show();
        } else if (!PASSWORD_PATTERN.matcher(changepassword.getText().toString().trim()).matches()) {
            new AlertDialog.Builder(ChangePassword.this)
                    //.setTitle("Record New Track")
                    .setMessage("Password must be of 1 digit,1 lower case letter,no white spaces,at least 2 characters,at least 1 special character")
//and some more method calls
                    .show();
            //  changepassword.setError("1 digit,1 lower case letter,no white spaces,at least 2 characters,at least 1 special character");
        } else if (!PASSWORD_PATTERN.matcher(changepassword1.getText().toString().trim()).matches()) {

            new AlertDialog.Builder(ChangePassword.this)
                    //.setTitle("Record New Track")
                    .setMessage("Password must be of 1 digit,1 lower case letter,no white spaces,at least 2 characters,at least 1 special character")
//and some more method calls
                    .show();
            // changepassword1.setError("1 digit,1 lower case letter,no white spaces,at least 2 characters,at least 1 special character");
        } else {
            if (isNetworkAvailable(getApplicationContext())) {
                oldpass.setEnabled(false);
                changepassword.setEnabled(false);
                changepassword1.setEnabled(false);
                changepass();
            } else {
                Toast.makeText(ChangePassword.this, "Please Check Internet Connection !!!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void sendotp() {
//        Random otp = new Random();
//
//        StringBuilder builder = new StringBuilder();
//        for (int count = 0; count < 4; count++) {
//            builder.append(otp.nextInt(10));
//        }
//        change = builder.toString().trim();
        //   Toast.makeText(this, ""+change, Toast.LENGTH_SHORT).show();
        s = "+91" + mobile;

        // otpurl = otpurl.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.otpApi(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                            String code = jsonObject.getString("code");
                            if (code.equals("sent")) {
                                // e5.setEnabled(true);
                                otpValue = jsonObject.getString("value");
                                relative.setVisibility(View.VISIBLE);
                                fsubmit.setVisibility(View.VISIBLE);
                                sendotp.setBackgroundResource(R.drawable.shapeloginbtn);
                                sendotp.setClickable(false);
                                sendotp.setText("OTP Send. Please Wait...");
                                startTimer();
                                Toast.makeText(ChangePassword.this, "OTP send Successfully", Toast.LENGTH_SHORT).show();
                                // getvolley1(mobile,change);

                            } else {
                                Toast.makeText(ChangePassword.this, "sorry,Server encountered problem !!!", Toast.LENGTH_LONG).show();
                            }
                            //  Toast.makeText(signup.this, ""+status, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(ChangePassword.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(getApplicationContext(), "Server encountered some problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("u_mobile", s);
                params.put("changepass", "changepass");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
//            //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(ChangePassword.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Sending OTP..");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    void startTimer() {
        timer.setVisibility(View.VISIBLE);
        cTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                // t2.setText("seconds remaining: " + millisUntilFinished / 1000);
                long millis = millisUntilFinished;
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                //  timer.setText(hms);//set text
                timer.setText(hms.substring(3, 8));//set text
                //    String abc = hms.replaceAll();
                //   t2.setClickable(false);
            }

            public void onFinish() {
                sendotp.setBackgroundResource(R.drawable.shapelogin);
                sendotp.setClickable(true);
                sendotp.setText("Resend OTP");
                timer.setVisibility(View.GONE);

            }
        };
        cTimer.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ChangePassword.this, MainActivity.class));
        finish();
    }

    public void submit(View view) {
        matchotp();
    }

    public void matchotp() {
        pin = e5.getText().toString().trim();

        if (pin.equals("")) {

            Toast.makeText(this, "Please fill OTP", Toast.LENGTH_SHORT).show();
            // getvolley();
        } else if (pin.equals(otpValue)) {

            //   Toast.makeText(this, "OTP match", Toast.LENGTH_SHORT).show();
            if (isNetworkAvailable(getApplicationContext())) {
                changepass();
            } else {
                Toast.makeText(ChangePassword.this, "Please Check Internet Connection !!! ", Toast.LENGTH_SHORT).show();
            }
            // getvolley();
        } else {
            Toast.makeText(this, "OTP does not match", Toast.LENGTH_SHORT).show();
        }
    }


    private void changepass() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.changePass(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String code = jsonObject.getString("code");
                            String success = jsonObject.getString("success");
                            if (code.equals("request_submitted")){
                                Toast.makeText(getApplicationContext(),"password changed",Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ChangePassword.this, LoginActivity.class));
                                finish();
                            }else if (code.equals("request_mismatch")){
                                Toast.makeText(getApplicationContext(),"password doen't match",Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(ChangePassword.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(ChangePassword.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);

                return params;
            }
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                try {
                    String passHash = GFG.toHexString(GFG.getSHA(oldpass.getText().toString().trim()));
                    String newPassHash = GFG.toHexString(GFG.getSHA(changepassword.getText().toString().trim()));
                params.put("mobile",mobile);
                params.put("pass",newPassHash);
                params.put("oldpass",passHash);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
//                try {

//                    String preLoginHash = GFG.toHexString(GFG.getSHA(AppSession.preLoginToken.toString().trim()));
//                    String saltPass = preLoginHash+passHash;
//                    params.put("oldpass",saltPass);
//                } catch (NoSuchAlgorithmException e) {
//                    Toast.makeText(ChangePassword.this, "Something went Wrong !!!", Toast.LENGTH_SHORT).show();
//                }
//                try {
//                    String passHash = GFG.toHexString(GFG.getSHA(changepassword.getText().toString().trim()));
//                    String preLoginHash = GFG.toHexString(GFG.getSHA(AppSession.preLoginToken.toString().trim()));
//                    String saltPass = preLoginHash+passHash;
//                    params.put("pass",saltPass);
//                } catch (NoSuchAlgorithmException e) {
//
//                    Toast.makeText(ChangePassword.this, "Something went Wrong !!!", Toast.LENGTH_SHORT).show();
//                }
                // params.put("pass",changepassword.getText().toString().trim());
               // params.put("mobile", mobile);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(ChangePassword.this);
        requestQueue.add(stringRequest);
        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(ChangePassword.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Updating your request.Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /*public void sendotp(View view) {
        if (changepassword.getText().toString().trim().equals("")) {
            Toast.makeText(this, "Password Field can't be blank", Toast.LENGTH_SHORT).show();
        }
        else
        {
            getotp();
         }
    }

    public void getotp(){
        Random otp = new Random();

        StringBuilder builder = new StringBuilder();
        for (int count = 0; count < 4; count++) {
            builder.append(otp.nextInt(10));
        }
        change = builder.toString().trim();
        //   Toast.makeText(this, ""+change, Toast.LENGTH_SHORT).show();
        s = "+91" + mobile;

        //String url = "http://psms.onistech.com/submitsms.jsp?user=swarsang&key=cfca888991XX&mobile=" + s + "&message=Your Login  OTP for Swar Sangeet is " + change + "&senderid=INFOSM&accusage=1";
        // String msg="Your Registration OTP for Capital Trade Link is";
        String url="http://psms.onistech.com/submitsms.jsp?user=lendtree&key=e79717ad24XX&mobile=" + s + "&message=Your Password Change OTP for Capital Link is " + change + "&senderid=LENDTR&accusage=1";
        url = url.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
//                            progressDialog.dismiss();
                        // Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        //  String names = "prappo,prince";
                        String[] namesList = response.split(",");
                        String status = namesList [0].trim();
                        if(status.equals("sent"))
                        {
                            // e5.setEnabled(true);
                            OTPVarifie.setVisibility(View.VISIBLE);
                            submitOTP.setVisibility(View.VISIBLE);
                            //fsubmit.setVisibility(View.VISIBLE);
                            //e3.setEnabled(false);
                           // e4.setEnabled(false);
                            sendOTP.setBackgroundResource(R.drawable.shapeloginbtn);
                            sendOTP.setClickable(false);
                            startTimer();
                            Toast.makeText(ChangePassword.this, "OTP send Successfully", Toast.LENGTH_SHORT).show();
                            // getvolley1(mobile,change);

                        }
                        else
                        {
                            Toast.makeText(ChangePassword.this, "Invalid mobile number !!!", Toast.LENGTH_SHORT).show();
                        }
                        //  Toast.makeText(signup.this, ""+status, Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
               progressDialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                else {
                    Toast.makeText(getApplicationContext(),"Something went wrong!!!", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(getApplicationContext(),""+message, Toast.LENGTH_SHORT).show();
//
//                Toast.makeText(getApplicationContext(),"Something went wrong!!!", Toast.LENGTH_SHORT).show();
//
            }

        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
//            //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(ChangePassword.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Sending OTP..");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    void startTimer() {
        timer.setVisibility(View.VISIBLE);
        cTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                // t2.setText("seconds remaining: " + millisUntilFinished / 1000);
                long millis = millisUntilFinished;
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                timer.setText( hms.substring(3,8));//set text
                //    String abc = hms.replaceAll();
                //   t2.setClickable(false);
            }
            public void onFinish() {
                sendOTP.setBackgroundResource(R.drawable.btnupload);
                sendOTP.setClickable(true);
                sendOTP.setText("Resend OTP");
                timer.setVisibility(View.GONE);

            }
        };
        cTimer.start();
    }*/

}


