package com.business.capitaltrade;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;

;

import java.text.DecimalFormat;

public class EmiCalc extends AppCompatActivity {

    EditText e1,e2,e3;
    TextView t1,t2,t3;
    String amount="",interest="",tenure="";
    TableLayout table;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emi_calc);
        e1=(EditText)findViewById(R.id.amount);
        e2=(EditText)findViewById(R.id.interest);
        e3=(EditText)findViewById(R.id.tenure);
        t1=(TextView)findViewById(R.id.tamount);
        t2=(TextView)findViewById(R.id.tinterest);
        t3=(TextView) findViewById(R.id.tpayable);
        table=(TableLayout) findViewById(R.id.table2);
        e1.addTextChangedListener(new NumberTextWatcherForThousand(e1));
    }


    public void calculate(View view) {
        getdata();
        //calculation();
       // amount=e1.getText().toString().trim();

       // Toast.makeText(this, ""+amt, Toast.LENGTH_SHORT).show();
    }

    public void getdata(){
        amount= NumberTextWatcherForThousand.trimCommaOfString(e1.getText().toString().trim());
       interest= e2.getText().toString().trim();
       tenure=e3.getText().toString().trim();
        if(amount.equals(""))
        {
            new AlertDialog.Builder(EmiCalc.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Loan Amount")
//and some more method calls
                    .show();
        }

        else if(interest.equals(""))
        {
            new AlertDialog.Builder(EmiCalc.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Rate of Interest")
//and some more method calls
                    .show();
        }

        else if(tenure.equals(""))
        {
            new AlertDialog.Builder(EmiCalc.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Loan Tenure")
//and some more method calls
                    .show();
        }
        else
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            calculation();
        }
    }

    public void calculation(){

        table.setVisibility(View.VISIBLE);
        int amt =Integer.parseInt(amount);
        double roi = Double.parseDouble(interest) / (12*100) ;
        int month = Integer.parseInt(tenure);
        double power= Math.pow(1+roi,month);


        double emi = (amt*roi*power) / (power-1);
        double valueRounded = Math.round(emi * 100D) / 100D;
        DecimalFormat formatter = new DecimalFormat("#,###,###.##");
        String yourFormattedString = formatter.format(valueRounded);
       // Toast.makeText(this, ""+valueRounded, Toast.LENGTH_SHORT).show();
       // Toast.makeText(this, ""+yourFormattedString, Toast.LENGTH_SHORT).show();
        t1.setText(yourFormattedString);


        double tint=Math.round(valueRounded*month-amt);
       // double realint=Math.round(tint) - amt;
      //  DecimalFormat formatinterest = new DecimalFormat("#,###,###.##");
        String yourinterest = formatter.format(tint);
        t2.setText(yourinterest);

        double tamnt=amt+tint;
       // DecimalFormat formatinterest = new DecimalFormat("#,###,###.##");
        String yourtamount = formatter.format(tamnt);
        t3.setText(yourtamount+" / -");
      //  Toast.makeText(this, ""+tint, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
