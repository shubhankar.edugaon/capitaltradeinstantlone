package com.business.capitaltrade;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Proposal extends AppCompatActivity {
    ProgressDialog progressDialog;
    String borrowid="",loannum="",pname="",lamount="",tenure="",roi="",status="",astatus="",vstatus="",pstatus="",sesid="",
            ploanrid="",borreqloanid="",f_amt="",f_roi="",f_tenure="",f_approve_by="",f_status="",f_aggreement_status="",processing_fee_status="",borrower_reply_status="",profee="",disburse_status,
    empstatus,gpno="",bfile="",mandate="";
    RecyclerView r1;
    ArrayList<Modelloanstatus> ar = new ArrayList<Modelloanstatus>();

    RequestQueue mRequest;
    SessionManager session;
    Recycler rec;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proposal);
        mRequest = Volley.newRequestQueue(Proposal.this);
        session = new SessionManager(getApplicationContext());
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // name
       // String name = user.get(SessionManager.KEY_NAME);

        // email
        sesid = user.get(SessionManager.KEY_ID);

      //  Toast.makeText(this, ""+sesid, Toast.LENGTH_SHORT).show();
        r1 = (RecyclerView)findViewById(R.id.recycler);

        getvolley();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getvolley() {
        String url = "https://onistech.in/capitaltrade/android/showloanorder.php?id="+sesid+"";
        // String url = "http://bollywoodcity.in/event/index.php?start_date="+date[0]+"&end_date="+date1[0]+"&start_time="+time[0]+"&end_time="+time1[0]+"";
        //String url = "http://bollywoodcity.in/event/index.php?date="+date[0]+"&date2="+date1[0]+"&start_time="+time[0]+"&end_time="+time1[0]+"";
        StringRequest stringRequest =  new StringRequest(Request.Method.POST, UrlUtils.loanProposal(sesid), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    ar.clear();
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        //get value of first index
                        // String code = jsonObject.getString("code");
                        //String message = jsonObject.getString("code"); //we should write key name;
                        String success = jsonObject.getString("success");
                        String status = jsonObject.getString("status");

                        if (success.equals(0) && status.equals(401)) {
                            session.logoutUser();
                            Toast.makeText(Proposal.this, "Sorry, sessionn time out!!!", Toast.LENGTH_LONG).show();

                        }else {
                            borrowid = jsonObject.getString("borrowid");
                            loannum = jsonObject.getString("loannum");
                            pname = jsonObject.getString("pname");
                            lamount = jsonObject.getString("lamount");
                            tenure = jsonObject.getString("tenure");
                            astatus = jsonObject.getString("astatus");
                            vstatus = jsonObject.getString("vstatus");
                            pstatus = jsonObject.getString("pstatus");
                            roi = jsonObject.getString("roi");
                            ploanrid = jsonObject.getString("ploanrid");
                            borreqloanid = jsonObject.getString("borreqloanid");
                            f_amt = jsonObject.getString("f_amt");
                            f_roi = jsonObject.getString("f_roi");
                            f_tenure = jsonObject.getString("f_tenure");
                            f_approve_by = jsonObject.getString("f_approve_by");
                            f_status = jsonObject.getString("f_status");
                            f_aggreement_status = jsonObject.getString("f_aggreement_status");
                            processing_fee_status = jsonObject.getString("processing_fee_status");
                            borrower_reply_status = jsonObject.getString("borrower_reply_status");
                            disburse_status = jsonObject.getString("disburse_status");
                            status = jsonObject.getString("status");
                            empstatus = jsonObject.getString("empstatus");
                            gpno = jsonObject.getString("gpno");
                            bfile = jsonObject.getString("bfile");
                            mandate = jsonObject.getString("emandate");

                            //  Toast.makeText(Proposal.this, " "+ mandate, Toast.LENGTH_SHORT).show();
                            //  Toast.makeText(Proposal.this, ""+bfile, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+loannum, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+pname, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+lamount, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+tenure, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+astatus, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+vstatus, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+pstatus, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+roi, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+ploanrid, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+borreqloanid, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+f_amt, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+f_roi, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+f_tenure, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+f_approve_by, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+f_status, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+f_aggreement_status, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+processing_fee_status, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(Proposal.this, ""+borrower_reply_status, Toast.LENGTH_SHORT).show();

                            //  status = jsonObject.getString("status");

                            rec = new Recycler(Proposal.this, Ali());
                            r1.setLayoutManager(new LinearLayoutManager(Proposal.this, LinearLayoutManager.VERTICAL, false));
                            r1.setAdapter(rec);
                        }
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();

                     Toast.makeText(Proposal.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressDialog.dismiss();
                Toast.makeText(Proposal.this,"Sorry ,Something went Wrong Or No Connection !! ", Toast.LENGTH_SHORT).show();
                // progressDialog.dismiss();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(Proposal.this);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(Proposal.this,R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Retrieving Loan status....");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public ArrayList<Modelloanstatus> Ali() {
        Modelloanstatus m1 = new Modelloanstatus( borrowid, loannum,pname,lamount,tenure,astatus,vstatus,pstatus,roi,status,
                ploanrid,borreqloanid,f_amt,f_roi,f_tenure,f_approve_by,f_status,f_aggreement_status,processing_fee_status,borrower_reply_status,disburse_status,empstatus,
                gpno,bfile,mandate);
        ar.add(m1);
        return ar;

    }
}
