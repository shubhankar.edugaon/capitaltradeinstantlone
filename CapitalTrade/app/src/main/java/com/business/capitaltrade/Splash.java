package com.business.capitaltrade;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class Splash extends BaseActivity {
    ImageView iv;
    long Delay = 5000;
    Context context;
    VersionChecker versionChecker;
    String latestVersion="";
    PackageInfo pInfo;
    String version="";
    RelativeLayout relative;
     Snackbar snackbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        relative=(RelativeLayout)findViewById(R.id.relative);
        checkConnection();
    }

    public void checkversion(){
        try {
            latestVersion = versionChecker.execute().get();
            //Toast.makeText(this, ""+latestVersion, Toast.LENGTH_SHORT).show();
        } catch (ExecutionException e) {

        } catch (InterruptedException e) {

        }

        try {
            pInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            // Toast.makeText(context, ""+version, Toast.LENGTH_SHORT).show();

            if(latestVersion.equals(version))
            {
                iv=(ImageView)findViewById(R.id.iv);
                Glide.with(Splash.this)
                        .load(R.drawable.pgif)
                        .into(iv);

                Timer RunSplash = new Timer();

                // Task to do when the timer ends
                TimerTask ShowSplash = new TimerTask() {
                    @Override
                    public void run() {
                        // Close SplashScreenActivity.class

                        // Start MainActivity.class
//                Intent myIntent = new Intent(Splash.this,
//                        LoginActivity.class);
                        Intent myIntent = new Intent(Splash.this,
                                Language.class);
                        startActivity(myIntent);
                        finish();
                    }
                };

                // Start the timer
                RunSplash.schedule(ShowSplash, Delay);
                // Toast.makeText(context, "Not Move", Toast.LENGTH_SHORT).show();
                //Toast.makeText(context, "Move", Toast.LENGTH_SHORT).show();
            }
            else
            {

                openstore();
                //Toast.makeText(context, "No move", Toast.LENGTH_SHORT).show();
            }
        } catch (PackageManager.NameNotFoundException e) {

        }

    }


    public void openApp()
    {
        iv=(ImageView)findViewById(R.id.iv);
        Glide.with(Splash.this)
                .load(R.drawable.pgif)
                .into(iv);

        Timer RunSplash = new Timer();

        // Task to do when the timer ends
        TimerTask ShowSplash = new TimerTask() {
            @Override
            public void run() {
                // Close SplashScreenActivity.class

                // Start MainActivity.class
//                Intent myIntent = new Intent(Splash.this,
//                        LoginActivity.class);
                Intent myIntent = new Intent(Splash.this,
                        Language.class);
                startActivity(myIntent);
                finish();
            }
        };

        // Start the timer
        RunSplash.schedule(ShowSplash, Delay);
    }


    public void openstore()
    {

        // Create the object of
        // AlertDialog Builder class
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(Splash.this);

        // Set the message show for the Alert time
        builder.setMessage("New Update Available. Press OK to Update from Play Store.");

        // Set Alert Title
        builder.setTitle("Alert !");

        // Set Cancelable false
        // for when the user clicks on the outside
        // the Dialog Box then it will remain show
        builder.setCancelable(false);

        // Set the positive button with yes name
        // OnClickListener method is use of
        // DialogInterface interface.

        builder.setPositiveButton(
                        "Ok",
                        new DialogInterface
                                .OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                final String appPackageName = getPackageName();
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    finish();
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    finish();
                                }
                                // When the user click yes button
                                // then app will close
                                //finish();
                            }
                        });


        // Create the Alert dialog
        final AlertDialog alertDialog = builder.create();
//2. now setup to change color of the button
        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#000000"));
            }
        });
        // Show the Alert Dialog box
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public void checkConnection(){

        if(isNetworkAvailable(getApplicationContext())) {
            changeLanguage();
            context = this;
            versionChecker = new VersionChecker();
            checkversion();
          //  openApp();
        }
        else {

           snackbar = Snackbar
                    .make(relative, "No Internet Connection",
                            Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                          //  snackbar.dismiss();

                            checkConnection();

                        }
                    });
            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();

            ViewGroup.LayoutParams params = sbView.getLayoutParams();
            params.height = 100;
            sbView.setLayoutParams(params);

            TextView textView = (TextView) sbView.findViewById
                    (android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            textView.setTypeface(textView.getTypeface(), Typeface.BOLD);


//            FrameLayout.LayoutParams parentParams = (FrameLayout.LayoutParams)
            sbView.getLayoutParams();

            snackbar.show();
        }
    }

}
