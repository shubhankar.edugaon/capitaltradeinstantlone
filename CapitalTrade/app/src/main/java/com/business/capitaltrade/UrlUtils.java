package com.business.capitaltrade;


public class UrlUtils {
    //  public static String oldurl="http://onistech.in/capitaltrade/android/";
    public static String oldurl = "https://capitaltrade.in/capitaltrade/android/";

    public static String preLogin(){
        String url = oldurl + "/prelogin.php";
        return url;
    }

    public static String login() {
        String url = oldurl + "/testlogin.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String checkMobile() {
        String url = oldurl + "/checkmobile.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String registration() {
        String url = oldurl + "/registration.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String otpApi() {
        String url = oldurl + "/otpApi.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String loginOtp() {
        String url = oldurl + "/loginwithotp.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String forgotPass() {
        String url = oldurl + "/forgotpass.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String checkDoc() {
        String url = oldurl + "/checkdoc.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String repayment(String id) {
        String url = oldurl + "/noofloan.php?id=" + id + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String showEmiChart(String id, String page, String serial) {
        String url = oldurl + "/showemi.php?id=" + id + "&page=" + page + "&serial=" + serial + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String userProfie() {
        String url = oldurl + "/userprofile.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String changePass() {
        String url = oldurl + "/changepass.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String showPersonalInfo(String id) {
        String url = oldurl + "showpersonalinfo.php?id=" + id + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String personalInfo() {
        String url = oldurl + "/personalinfo.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String fetchState() {
        String url = oldurl + "/state.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String fetchCity(String stateId) {
        String url = oldurl + "/city.php?stateid=" + stateId + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String checkPincode() {
        String url = oldurl + "/checkpincode.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String companyInfo() {
        String url = oldurl + "/userroll.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String showCompanyInfo(String id) {
        String url = oldurl + "/showroll.php?id=" + id + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String BankInfo() {
        String url = oldurl + "/bankinfo.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String showBankInfo(String id) {
        String url = oldurl + "/showbankinfo.php?id=" + id + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String loanPurpose() {
        String url = oldurl + "/loanpurpose.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String subLoanPurpose(String subtypeid) {
        String url = oldurl + "/prosubtype.php?subtypeid=" + subtypeid + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String dealerName() {
        String url = oldurl + "/dealerName.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String calcLoanFees() {
        String url = oldurl + "/calcfees.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String sendLoanRequest() {
        String url = oldurl + "/loanrequest.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String loanProposal(String id) {
        String url = oldurl + "/showloanorder.php?id=" + id + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String borrowerReply() {
        String url = oldurl + "/borrowerreply.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String showUploadedDoc(String loanId) {
        String url = oldurl + "/uploadeddoc.php?id=" + loanId + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String uploadDoc() {
        String url = oldurl + "/upload.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String uploadPan() {
        String url = oldurl + "/paninfo.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String showPan(String id) {
        String url = oldurl + "/showpan.php?id=" + id + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String uploadAdhar() {
        String url = oldurl + "/adharinfo.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }


    public static String showAdhar(String id) {
        String url = oldurl + "/showadhar.php?id=" + id + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String uploadPhoto() {
        String url = oldurl + "/photoinfo.php";
        url = url.replaceAll(" ", "%20");
        return url;
    }

    public static String showPhoto(String id) {
        String url = oldurl + "/showphoto.php?id=" + id + "";
        url = url.replaceAll(" ", "%20");
        return url;
    }

}
