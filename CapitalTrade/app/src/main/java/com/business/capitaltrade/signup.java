package com.business.capitaltrade;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static com.business.capitaltrade.ChangePassword.PASSWORD_PATTERN;

public class signup extends BaseActivity {
    ProgressDialog progressDialog;
    TextView otp;
    EditText e1,e2,e3,e4;
    static EditText e5;
    CheckBox c1;
    TextView login;
    TextView timer;
    SessionManager session;
    Button sendotp,fsubmit;
    String name="",email="",mobile="",pin="",change="",s="",prefix="",id="",otpValue="";
    String serverurl="https://onistech.in/capitaltrade/android/registration.php";
    String otpurl="https://onistech.in/capitaltrade/android/otpApi.php";
    CountDownTimer cTimer = null;
    RelativeLayout relative;
  //  String serverurl1="https://lendtree.onistech.in/android/otp.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);
        session = new SessionManager(getApplicationContext());
      //  e1=(EditText)findViewById(R.id.et21);   //name
      //  e2=(EditText)findViewById(R.id.et22);  //email
        e3=(EditText)findViewById(R.id.et23); // mobile
        e4=(EditText)findViewById(R.id.et26); //pass
        e5=(EditText)findViewById(R.id.et28); //otp
        c1=(CheckBox)findViewById(R.id.check);
        sendotp=(Button) findViewById(R.id.sendotp);
        fsubmit=(Button) findViewById(R.id.fsubmit);
        timer=(TextView) findViewById(R.id.timer);
        login=(TextView)findViewById(R.id.login);
        relative=(RelativeLayout) findViewById(R.id.relative);
        login.setPaintFlags(login.getPaintFlags() |  Paint.UNDERLINE_TEXT_FLAG);
//        otp=(TextView)findViewById(R.id.otp);
//        e1.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(c1.isChecked()){
                    e4.setTransformationMethod(null);
                }

                else {
                    e4.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });

    }

    private void getdata()
    {
       // name=e1.getText().toString().trim();
     //   email=e2.getText().toString().trim();
        mobile=e3.getText().toString().trim();
        //pass=e4.getText().toString().trim();
        pin=e5.getText().toString().trim();

//        if(name.equals(""))
//        {
//            new AlertDialog.Builder(signup.this)
//                    //.setTitle("Record New Track")
//                    .setMessage("Please fill  Name")
////and some more method calls
//                    .show();
//        }
//        else if(email.equals(""))
//        {
//            new AlertDialog.Builder(signup.this)
//                    //.setTitle("Record New Track")
//                    .setMessage("Please fill Email")
////and some more method calls
//                    .show();
//        }
         if(mobile.equals(""))
        {
            new AlertDialog.Builder(signup.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Mobile Number")
//and some more method calls
                    .show();
        }
         else if(mobile.length()!=10)
         {
             new AlertDialog.Builder(signup.this)
                     //.setTitle("Record New Track")
                     .setMessage("Mobile Number must be of 10 digits")
//and some more method calls
                     .show();
         }
        else if(e4.getText().toString().trim().equals(""))
        {
            new AlertDialog.Builder(signup.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please Set Password")
//and some more method calls
                    .show();
        }
         else if (!PASSWORD_PATTERN.matcher(e4.getText().toString().trim()).matches()) {

             new AlertDialog.Builder(signup.this)
                     //.setTitle("Record New Track")
                     .setMessage("Password must be of 1 digit,1 lower case letter,no white spaces,at least 2 characters,at least 1 special character")
//and some more method calls
                     .show();
         }
        else if(pin.equals(""))
        {
            new AlertDialog.Builder(signup.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill One Time Password")
//and some more method calls
                    .show();
        }
        else
        {
           //matchotp();
            getvolley();
        }
    }

    private void getvolley() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.registration(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject= jsonArray.getJSONObject(0);
                            String code=jsonObject.getString("code");
                            String message=jsonObject.getString("message");
                            if(code.equals("reg_failed"))
                            {

                              //  e2.setText("");
                               // e3.setText("");
                                Toast.makeText(signup.this, ""+message, Toast.LENGTH_LONG).show();
                            }
                            else if(code.equals("reg_success"))
                            {
                                String borrid=jsonObject.getString("borrid");
                                String borrcode=jsonObject.getString("borrcode");
                                String borrcontact=jsonObject.getString("borrcontact");
                                // progressDialog.dismiss();
                                if(borrcode.equals(prefix) && borrcontact.equals(mobile))
                                {
                                    session.createLoginSession(borrid,borrcontact,borrcode);
                                    //
                                //startActivity(new Intent(signup.this,MainActivity.class));
                              //  finish();

                                    Intent i = new Intent(signup.this, MainActivity.class);
                                    i.putExtra("fragmentNumber",2);
                                    startActivity(i);
                                    finish();
//                             Toast.makeText(signup.this, ""+message, Toast.LENGTH_LONG).show();
//                                Toast.makeText(signup.this, ""+borrid, Toast.LENGTH_SHORT).show();
//                                Toast.makeText(signup.this, ""+borrcode, Toast.LENGTH_SHORT).show();
//                                Toast.makeText(signup.this, ""+borrcontact, Toast.LENGTH_SHORT).show();
                                }
                                 else
                                {
                                    Toast.makeText(signup.this, "Something Mismatch !!!", Toast.LENGTH_SHORT).show();
                                }

                            }

                            else if(code.equals("already"))
                            {
                                // progressDialog.dismiss();
                                int random = (int)((Math.random() * 9000000)+1000000);
                                prefix = "BR_"+random;
                                getvolley();
                                Toast.makeText(signup.this, "Please Wait", Toast.LENGTH_LONG).show();

                            }
                            else if(code.equals("reg_denied"))
                            {
                                // progressDialog.dismiss();
                               // int random = (int)((Math.random() * 9000000)+1000000);
                               // prefix = "BR_"+random;
                                //getvolley();
                                Toast.makeText(signup.this, "sorry,Server encountered problem !!!", Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {

                            Toast.makeText(signup.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();


                Toast.makeText(signup.this,"Server encountered some problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
              //  params.put("u_name",name);
               // params.put("u_mail",email);
                params.put("u_mobile",mobile);

                    String pass = e4.getText().toString().trim();
//                    String passHash = GFG.toHexString(GFG.getSHA(e4.getText().toString().trim()));
//                    String preTokenHash = GFG.toHexString(GFG.getSHA(AppSession.preLoginToken.toString().trim()));
//                    String saltPass = preTokenHash+passHash;
                    params.put("u_pass",pass);
                // params.put("u_code",prefix);
                params.put("u_code",prefix);
                params.put("u_otp",pin);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(signup.this);
        requestQueue.add(stringRequest);
        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(signup.this,R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Signing Up....");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void signin(View view) {
        startActivity(new Intent(signup.this, LoginActivity.class));
        finish();
    }

    public void matchotp(){
        if(pin.equals(otpValue))
        {

           int random = (int)((Math.random() * 9000000)+1000000);
           prefix = "BR_"+random;
           //cTimer.cancel();
          //  Toast.makeText(this, ""+prefix, Toast.LENGTH_SHORT).show();
            if(isNetworkAvailable(getApplicationContext()))
            {
               // getvolley();
            }
            else
                {

                Toast.makeText(signup.this, "Please Check Internet Connection !!!", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this, "OTP does not match", Toast.LENGTH_SHORT).show();
        }
    }

    public void submit(View view) {
        getdata();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(signup.this, LoginActivity.class));
        this.finish();
    }

    private void getdata1()
    {
//        name=e1.getText().toString().trim();
   //     email=e2.getText().toString().trim();
        mobile=e3.getText().toString().trim();
        //pass=e4.getText().toString().trim();
      //  e3.setClickable(false);
       // e3.setEnabled(false);

//        if(name.equals(""))
//        {
//            new AlertDialog.Builder(signup.this)
//                    //.setTitle("Record New Track")
//                    .setMessage("Please fill  Name")
////and some more method calls
//                    .show();
//        }
//        else if(email.equals(""))
//        {
//            new AlertDialog.Builder(signup.this)
//                    //.setTitle("Record New Track")
//                    .setMessage("Please fill Email")
////and some more method calls
//                    .show();
//        }
         if(mobile.equals(""))
        {
            new AlertDialog.Builder(signup.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Mobile Number")
//and some more method calls
                    .show();
        }
         else if(mobile.length()!=10)
         {
             new AlertDialog.Builder(signup.this)
                     //.setTitle("Record New Track")
                     .setMessage("Mobile Number must be of 10 digits")
//and some more method calls
                     .show();
         }

        else if(e4.getText().toString().trim().equals(""))
        {
            new AlertDialog.Builder(signup.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Password")
//and some more method calls
                    .show();
        }
         else if (!PASSWORD_PATTERN.matcher(e4.getText().toString().trim()).matches()) {

             new AlertDialog.Builder(signup.this)
                     //.setTitle("Record New Track")
                     .setMessage("Password must be of 1 digit,1 lower case letter,no white spaces,at least 2 characters,at least 1 special character")
//and some more method calls
                     .show();
         }
        else
        {

            s = "+91" + mobile;

            //otpurl = otpurl.replaceAll(" ", "%20");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.otpApi(),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {


                                progressDialog.dismiss();
                                JSONArray jsonArray = new JSONArray(response);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                                String code = jsonObject.getString("code");
                                if (code.equals("sent")) {
                                    // e5.setEnabled(true);
                                    otpValue = jsonObject.getString("value");
                                    relative.setVisibility(View.VISIBLE);
                                    fsubmit.setVisibility(View.VISIBLE);
                                    e3.setEnabled(false);
                                    e4.setEnabled(false);
                                    sendotp.setBackgroundResource(R.drawable.shapeloginbtn);
                                    sendotp.setClickable(false);
                                    sendotp.setText("OTP Send. Please Wait...");
                                    startTimer();
                                    Toast.makeText(signup.this, "OTP send Successfully", Toast.LENGTH_SHORT).show();
                                    // getvolley1(mobile,change);
                                } else {
                                    Toast.makeText(signup.this, "Mobile number Already registered,Try another", Toast.LENGTH_SHORT).show();
                                }
                                //  Toast.makeText(signup.this, ""+status, Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e)
                            {
                                Toast.makeText(signup.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    //
                    Toast.makeText(getApplicationContext(),"Server encountered some problem !!!", Toast.LENGTH_SHORT).show();

                }

            })
            {@Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params = new HashMap<String,String>();
                params.put("u_mobile",s);
                params.put("signup","1");
                return params;
            }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
//            //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
            progressDialog = new ProgressDialog(signup.this, R.style.AppCompatAlertDialogStyle);
            progressDialog.setMessage("Sending OTP..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }



    }


    public void getotp(View view) {
        if(isNetworkAvailable(getApplicationContext())) {
            getdata1();
        }
        else
        {
            Toast.makeText(signup.this, "Please Check Internet Connection !!!", Toast.LENGTH_SHORT).show();
        }
    }

    public void recivedSms(String message)
    {
        try
        {
            //  String str="sdfvsdf68fsdfsf8999fsdf09";
            String numberOnly= message.replaceAll("[^0-9]", "");
            e5.setText(numberOnly);
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
        }
    }



//    public void getvolley1(final String mob, final String otp1) {
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, serverurl1,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        try {
//                            progressDialog.dismiss();
//                            JSONArray jsonArray = new JSONArray(response);
//                            JSONObject jsonObject= jsonArray.getJSONObject(0);
//                            String code=jsonObject.getString("code");
//                          //  String message=jsonObject.getString("message");
//                            if(code.equals("otpsend"))
//                            {
//                                Toast.makeText(signup.this, ""+code, Toast.LENGTH_LONG).show();
//                            }
//                            else
//                            {
//
//                            }
//                        } catch (JSONException e) {
//
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//
//
//                Toast.makeText(signup.this,"Server has encountered problem !!!", Toast.LENGTH_SHORT).show();
//
//            }
//
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("u_mobile",mob);
//                params.put("u_otp",otp1);
//                return params;
//            }
//        };
//
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
//                20000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        RequestQueue requestQueue = Volley.newRequestQueue(signup.this);
//        requestQueue.add(stringRequest);
//        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
//        progressDialog = new ProgressDialog(signup.this,R.style.AppCompatAlertDialogStyle);
//        progressDialog.setMessage("Sending otp....");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//    }

//    public static String sent()
//    {
//        String s1 ="jhand";
//        return s1;
//    }

    void startTimer() {
        timer.setVisibility(View.VISIBLE);
        cTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                // t2.setText("seconds remaining: " + millisUntilFinished / 1000);
                long millis = millisUntilFinished;
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
              //  timer.setText(hms);//set text
                timer.setText( hms.substring(3,8));//set text
                //    String abc = hms.replaceAll();
             //   t2.setClickable(false);
            }
            public void onFinish() {
                sendotp.setBackgroundResource(R.drawable.shapelogin);
                sendotp.setClickable(true);
                sendotp.setText("Resend OTP");
                timer.setVisibility(View.GONE);

            }
        };
        cTimer.start();
    }
}
