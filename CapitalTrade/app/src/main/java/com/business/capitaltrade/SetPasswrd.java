package com.business.capitaltrade;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static com.business.capitaltrade.ChangePassword.PASSWORD_PATTERN;

public class SetPasswrd extends AppCompatActivity {
    String mob = "";
    String otp = "";
    EditText pass;
    ProgressDialog progressDialog;
    CheckBox c1;
    String forgetpassurl = "https://onistech.in/capitaltrade/android/forgotpass.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_passwrd);

        // get user data from session
        pass = (EditText) findViewById(R.id.pass);
        c1 = (CheckBox) findViewById(R.id.check);
        mob = getIntent().getExtras().getString("mobile");
        otp = getIntent().getExtras().getString("otp");
        //    name = user.get(SessionManager.KEY_NAME);

        // email
        c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (c1.isChecked()) {
                    pass.setTransformationMethod(null);
                } else {
                    pass.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });
    }

    public void done(View view) {
        if (pass.getText().toString().trim().equals("")) {
            new AlertDialog.Builder(SetPasswrd.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Password")
//and some more method calls
                    .show();
        } else if (!PASSWORD_PATTERN.matcher(pass.getText().toString().trim()).matches()) {

            new AlertDialog.Builder(SetPasswrd.this)
                    //.setTitle("Record New Track")
                    .setMessage("Password must be of 1 digit,1 lower case letter,no white spaces,at least 2 characters,at least 1 special character")
//and some more method calls
                    .show();
        } else {
            forgetpass();
        }
    }

    private void forgetpass() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.forgotPass(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            //    Toast.makeText(SetPasswrd.this, ""+response, Toast.LENGTH_SHORT).show();
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String code = jsonObject.getString("code");
                            //Toast.makeText(LoanRequest.this, ""+code, Toast.LENGTH_LONG).show();
                            // String message=jsonObject.getString("message");
                            if (code.equals("password_changed")) {

                                Toast.makeText(SetPasswrd.this, "Password Changed Successfully !", Toast.LENGTH_SHORT).show();
                                //    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragemtDasboard()).commit();
                                Intent intent = new Intent(SetPasswrd.this, LoginActivity.class);
                                startActivity(intent);
                                finish();

                                //  Toast.makeText(LoanRequest.this, ""+message, Toast.LENGTH_LONG).show();
                            } else if (code.equals("not_submitted")) {
                                // progressDialog.dismiss();
                                Toast.makeText(SetPasswrd.this, "Sorry, Server encountered problem!!!", Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(LoanRequest.this,LoginActivity.class));
//                                finish();
                            } else {
                                Toast.makeText(SetPasswrd.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {

                            Toast.makeText(SetPasswrd.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(SetPasswrd.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Authorizations", "Bearer " + AppSession.logedInToken);

                params.put("mobile", mob);
                params.put("u_otp", otp);
                try {
                    String preTokenHash = GFG.toHexString(GFG.getSHA(AppSession.preLoginToken.toString().trim()));
                    String passHash = GFG.toHexString(GFG.getSHA(pass.getText().toString().trim()));
                    String saltHash = passHash;
                    params.put("pass",passHash);
                } catch (NoSuchAlgorithmException e) {
                    Toast.makeText(SetPasswrd.this, "Something went Wrong !!!", Toast.LENGTH_SHORT).show();
                }
                // params.put("pass",pass.getText().toString().trim());
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(SetPasswrd.this);
        requestQueue.add(stringRequest);
        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(SetPasswrd.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Updating your request.Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SetPasswrd.this, LoginActivity.class));
        finish();
    }
}
