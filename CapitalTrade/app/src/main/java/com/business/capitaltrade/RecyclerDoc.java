package com.business.capitaltrade;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecyclerDoc extends RecyclerView.Adapter<RecyclerDoc.MyViewHolder> {

    ArrayList<ModelUploadeddoc> arrayList;
    Context context;
    int j=1;
    LayoutInflater inflater;
    private FragmentCommunication mCommunicator;
    public RecyclerDoc()
    {

    }

    public RecyclerDoc(ArrayList<ModelUploadeddoc> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;

        inflater = LayoutInflater.from(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v=inflater.inflate(R.layout.uploadeddoc,parent,false);
        RecyclerDoc.MyViewHolder myViewHolder = new RecyclerDoc.MyViewHolder(v);

        return myViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelUploadeddoc model = arrayList.get(position);
        if(arrayList.get(position).getCode().equals("Available")) {
          String str=arrayList.get(position).getDoc();
          //  holder.doc.setText(arrayList.get(position).getDoc());
            List<String> doclist = Arrays.asList(str.split("-"));   // method to split string
            String arr[] =doclist.get(2).split("\\.");              // also a method to split string
            //Toast.makeText(context, ""+doclist.get(1), Toast.LENGTH_SHORT).show();

            holder.doc.setText(doclist.get(1)+"."+arr[1]);

            holder.date.setText(arrayList.get(position).getDate());
            holder.status.setText(arrayList.get(position).getStatus());
            if(arrayList.get(position).getStatus().equals("1")) {
                holder.status.setText("Accepted");
                holder.status.setTextColor(Color.parseColor("#33cc33"));
            }

            else if(arrayList.get(position).getStatus().equals("2")) {
                holder.status.setText("Rejected");
                holder.status.setTextColor(Color.parseColor("#ff0000"));
            }
            else {
                holder.status.setText("Pending");
                holder.status.setTextColor(Color.parseColor("#ffffbb33"));
            }

            holder.sr.setText(arrayList.get(position).getSr());


            holder.tview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //int pos = getAdapterPosition();
                    String str = arrayList.get(position).getDoc();
                    context.startActivity(new Intent(context, WebView.class).putExtra("name",str));
                    ((Activity)context).finish();

                }
            });
            //   String arr[]=doclist.get(2).split(".");
           // Toast.makeText(context, ""+arr[1], Toast.LENGTH_SHORT).show();

//            holder.name.setText(arrayList.get(position).getPname());
//            holder.damount.setText(arrayList.get(position).getLamnt());
//            holder.ddate.setText(arrayList.get(position).getDdate());




//
//            LoanList fragmentB = new LoanList();
//            Bundle bundle = new Bundle();
//            bundle.putString("ID", arrayList.get(position).getBorrreqloanid());
//            fragmentB.setArguments(bundle);

//            holder.l1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });

        }


        else
        {
            holder.l1.setVisibility(View.GONE);
            Toast.makeText(context, "No Document is uploaded yet.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView doc,date,status,tview,sr;

        LinearLayout l1;



        public MyViewHolder(final View itemView) {
            super(itemView);
            doc=itemView.findViewById(R.id.title);
            sr=itemView.findViewById(R.id.sr);
            date=itemView.findViewById(R.id.email);

            tview=itemView.findViewById(R.id.tview);
            status=itemView.findViewById(R.id.id);

            l1=itemView.findViewById(R.id.ll);

            tview.setPaintFlags(tview.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            l1.setOnClickListener(this);
            //   c1.setOnClickListener(this);



       /* c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = getAdapterPosition();
                String str = arrayList.get(pos).getId();
                Toast.makeText(v.getContext(), ""+str, Toast.LENGTH_SHORT).show();
            }
        });*/

        }


        @Override
        public void onClick(View v) {
            int pos2 = getAdapterPosition();
           // String a2= arrayList.get(pos2).getSr();

            //   Toast.makeText(context, ""+a2, Toast.LENGTH_SHORT).show();


        }
    }
}
