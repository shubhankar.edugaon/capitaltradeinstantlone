package com.business.capitaltrade;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.view.View.GONE;

public class Recycler extends RecyclerView.Adapter<Recycler.MyViewHolder> {
    String s = "", rateOfint = "";
    ArrayList<Modelloanstatus> arrayList;
    Context mcontext;
    LayoutInflater minflater;
    Dialog mydialog;
    String sesid = "", set = "";
    ProgressDialog progressDialog;
    AlertDialog alertDialogAndroid;
    SessionManager session;
    DownloadManager downloadManager;

    public Recycler() {

    }

    public Recycler(Context context, ArrayList<Modelloanstatus> arrayList) {
        this.arrayList = arrayList;
        this.mcontext = context;
        minflater = LayoutInflater.from(mcontext);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = minflater.inflate(R.layout.getdata, parent, false);
        Recycler.MyViewHolder myViewHolder = new Recycler.MyViewHolder(v);
        session = new SessionManager(mcontext);
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // email
        sesid = user.get(SessionManager.KEY_ID);
        //  Toast.makeText(mcontext, ""+sesid, Toast.LENGTH_SHORT).show();

//        mydialog = new Dialog(mcontext);
//        mydialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//       // mydialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
//        mydialog.setContentView(R.layout.table_item1);
//
//        myViewHolder.b1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               // mydialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//              //  TextView i1 = (TextView) mydialog.findViewById(R.id.offeramount);
//                WindowManager wm = (WindowManager)mcontext.getSystemService(Context.WINDOW_SERVICE);
//                Display display = wm.getDefaultDisplay();
//                Point size = new Point();
//                display.getSize(size);
//                int DialogWidth;
//                int DialogHeight;
//
//             //   DialogWidth = (int) (size.x * 0.95);
//                DialogWidth =  WindowManager.LayoutParams.MATCH_PARENT;;
//                DialogHeight = WindowManager.LayoutParams.WRAP_CONTENT;
//
//                mydialog.getWindow().setLayout(DialogWidth, DialogHeight);
//                mydialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//
//                TextView i1 = (TextView) mydialog.findViewById(R.id.offeramount);
//               // arrayList.get(posi)
//                mydialog.show();
//            }
//        });
        return myViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Modelloanstatus model = arrayList.get(position);
        if (arrayList.get(position).getStatus().equals("Not Available")) {
            holder.c1.setVisibility(GONE);
            Toast.makeText(mcontext, "No Loan Request is submitted yet", Toast.LENGTH_SHORT).show();
        }


        holder.loannum.setText(arrayList.get(position).getLoannum());
        holder.name.setText(arrayList.get(position).getPname());
        holder.amount.setText(arrayList.get(position).getLamount() + " Rs");
        holder.days.setText(arrayList.get(position).getTenure() + " months");
        s = (arrayList.get(position).getVstatus());

        /*rateOfint=(arrayList.get(position).getRoi());
        holder.rate.setText(arrayList.get(position).getRoi()+" %");
            if(!rateOfint.equals("Not Available"))
            {
                holder.rate.setText(arrayList.get(position).getRoi()+ " %");
            }
            else
            {
                holder.rate.setText("NA");
            }*/
        // Toast.makeText(context, ""+s, Toast.LENGTH_SHORT).show();


//=========================================Verification Status=========================================================//

        if (arrayList.get(position).getVstatus().equals("0")) {
            Glide.with(mcontext)
                    .load(R.drawable.emosad)
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(holder.img);
            holder.stat.setText("Pending");
            holder.stat.setTextColor(Color.parseColor("#ffad33"));
        } else {
            Glide.with(mcontext)
                    .load(R.drawable.emohappy)
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(holder.img);
            holder.stat.setText("Verified");
            holder.stat.setTextColor(Color.parseColor("#33cc33"));
        }

//----------------------------------------------------------------------------------------------------------------------//

        if (arrayList.get(position).getPstatus().equals("1")) {
            holder.b1.setVisibility(View.VISIBLE);
        }
        if (arrayList.get(position).getAstatus().equals("1")) {
            Glide.with(mcontext)
                    .load(R.drawable.emoveryhappy)
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(holder.img);
            holder.stat.setText("Approved");
            holder.stat.setTextColor(Color.parseColor("#005ce6"));

        }


        if (arrayList.get(position).getDisburse_status().equals("1")) {
            Glide.with(mcontext)
                    .load(R.drawable.emoveryhappy)
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(holder.img);
            holder.stat.setText("Disbursed");
            holder.stat.setTextColor(Color.parseColor("#2eb82e"));

        }
//===================================Visibility of List of documents=====================================================//
        if (arrayList.get(position).getBorrower_reply_status().equals("1")) {
            holder.list.setVisibility(View.VISIBLE);
        } else {
            holder.list.setVisibility(GONE);
        }
//----------------------------------------------------------------------------------------------------------------------//

        //==================================Visibilty of Agreement=============================================================//

        if (arrayList.get(position).getBfile().equals("https://capitaltrade.in/ctlAdmin/pdfagreement/upload/")
                || arrayList.get(position).getBfile().equals("")) {
            if (arrayList.get(position).getGpno().equals("https://capitaltrade.in/ctlAdmin/pdfagreement/aggrement/eSign")
                    || arrayList.get(position).getGpno().equals("")) {
                holder.agreement.setVisibility(GONE);
                holder.vagree.setVisibility(GONE);
                holder.dagree.setVisibility(GONE);
                holder.esignbtn.setVisibility(GONE);
            } else {
                holder.agreement.setVisibility(View.VISIBLE);
                holder.vagree.setVisibility(View.VISIBLE);
                holder.dagree.setVisibility(View.VISIBLE);
                holder.esignbtn.setVisibility(View.VISIBLE);
            }

        } else {
            holder.agreement.setVisibility(View.VISIBLE);
            holder.vagree.setVisibility(View.VISIBLE);
            holder.dagree.setVisibility(View.VISIBLE);
            holder.esignbtn.setVisibility(GONE);
        }
//----------------------------------------------------------------------------------------------------------------------//


//==================================== On click E_SIGN button For Web VIew ==============================================//

        holder.esignbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcontext.startActivity(new Intent(mcontext, EsignWebView.class).putExtra("loanid", arrayList.get(position).getBorreqloanid()));
                ((Activity) mcontext).finish();
            }
        });


//------------------------------------------------------------------------------------------------------------------------//


//===================================Visibility of E-Mandate button==============================================//


        if (arrayList.get(position).getMandate().equals("")) {
            holder.emandate.setVisibility(GONE);

        } else {
            holder.emandate.setVisibility(View.VISIBLE);
        }

        //===================================Click listener E-Mandate button==============================================//


        holder.emandate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = arrayList.get(position).getMandate();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                mcontext.startActivity(i);
                // mcontext.startActivity(new Intent(mcontext,EmandateWebView.class).putExtra("link",arrayList.get(position).getMandate()));
                //((Activity)mcontext).finish();
            }
        });


//================================== View Document Click Listener =======================================================//

        holder.vagree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (arrayList.get(position).getBfile().equals("https://capitaltrade.in/ctlAdmin/pdfagreement/upload/")
                        || arrayList.get(position).getBfile().equals("")) {
                    mcontext.startActivity(new Intent(mcontext, ShowAgreement.class).putExtra("name", arrayList.get(position).getGpno()));
                    ((Activity) mcontext).finish();
                } else {
                    mcontext.startActivity(new Intent(mcontext, ShowAgreement.class).putExtra("name", arrayList.get(position).getBfile()));
                    ((Activity) mcontext).finish();
                }
            }
        });

//----------------------------------------------------------------------------------------------------------------------//

        //========================================Download Document Click Listener ==============================================//

        holder.dagree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arrayList.get(position).getBfile().equals("https://capitaltrade.in/ctlAdmin/pdfagreement/upload/")
                        || arrayList.get(position).getBfile().equals("")) {
                    Toast.makeText(mcontext, "Downloading is in Progress.Please check Download Folder.", Toast.LENGTH_LONG).show();
                    download(arrayList.get(position).getGpno());
                } else {
                    Toast.makeText(mcontext, "Downloading is in Progress.Please check Download Folder.", Toast.LENGTH_LONG).show();
                    download(arrayList.get(position).getBfile());
                }

            }
        });

        //---------------------------------------------------------------------------------------------------------------------------//


//=================================To Show List of Documents button ==================================================//

        holder.list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydialog = new Dialog(mcontext);
                mydialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                // mydialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
                mydialog.setContentView(R.layout.requireddoc);
                WindowManager wm = (WindowManager) mcontext.getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int DialogWidth;
                int DialogHeight;

                //   DialogWidth = (int) (size.x * 0.95);
                DialogWidth = WindowManager.LayoutParams.MATCH_PARENT;
                ;
                DialogHeight = WindowManager.LayoutParams.WRAP_CONTENT;
                mydialog.getWindow().setLayout(DialogWidth, DialogHeight);
                mydialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                TextView doc = (TextView) mydialog.findViewById(R.id.doc);
                RelativeLayout business = (RelativeLayout) mydialog.findViewById(R.id.business);
                RelativeLayout salaried = (RelativeLayout) mydialog.findViewById(R.id.salaried);
                Button bdoc = (Button) mydialog.findViewById(R.id.bdoc);
                Button sdoc = (Button) mydialog.findViewById(R.id.sdoc);
                doc.setPaintFlags(doc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                if (arrayList.get(position).getEmpstatus().equals("Self-employed")) {
                    salaried.setVisibility(GONE);
                    business.setVisibility(View.VISIBLE);
                } else {
                    business.setVisibility(GONE);
                    salaried.setVisibility(View.VISIBLE);
                }
                bdoc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mydialog.dismiss();
                        String a = arrayList.get(position).getBorreqloanid();
                        String b = arrayList.get(position).getEmpstatus().trim();
                        //  mcontext.startActivity(new Intent(mcontext,OtherDoc.class).putExtra("loanid",a));

                        mcontext.startActivity(new Intent(mcontext, OthersDocument.class).putExtra("loanid", a)
                                .putExtra("empstatus", b));

                        //Toast.makeText(mcontext, ""+b, Toast.LENGTH_SHORT).show();
                        //     mcontext.startActivity(new Intent(mcontext,OthersDocument.class));
//                        Intent i = new Intent(mcontext,OtherDoc.class);
//                        view.getContext().startActivity(i);
                        ((Activity) mcontext).finish();
                    }
                });
                sdoc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mydialog.dismiss();
                        String a = arrayList.get(position).getBorreqloanid();
                        String b = arrayList.get(position).getEmpstatus();
                        //  mcontext.startActivity(new Intent(mcontext,OtherDoc.class).putExtra("loanid",a));

                        mcontext.startActivity(new Intent(mcontext, OthersDocument.class).putExtra("loanid", a)
                                .putExtra("empstatus", b));


                        // Toast.makeText(mcontext, ""+b, Toast.LENGTH_SHORT).show();

                        //  mcontext.startActivity(new Intent(mcontext,OthersDocument.class));
//                        Intent i = new Intent(mcontext,OtherDoc.class);
//                        view.getContext().startActivity(i);
                        ((Activity) mcontext).finish();
                    }
                });
                mydialog.show();
            }
        });


//=================================To show View Offer Button and its onclick details============================================================//
        holder.b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydialog = new Dialog(mcontext);
                mydialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                // mydialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
                mydialog.setContentView(R.layout.table_item1);
                // mydialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                //  TextView i1 = (TextView) mydialog.findViewById(R.id.offeramount);
                WindowManager wm = (WindowManager) mcontext.getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int DialogWidth;
                int DialogHeight;

                //   DialogWidth = (int) (size.x * 0.95);
                DialogWidth = WindowManager.LayoutParams.MATCH_PARENT;
                ;
                DialogHeight = WindowManager.LayoutParams.WRAP_CONTENT;

                mydialog.getWindow().setLayout(DialogWidth, DialogHeight);
                mydialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                TextView offeramount = (TextView) mydialog.findViewById(R.id.offeramount);
                TextView rateofint = (TextView) mydialog.findViewById(R.id.rateofint);
                TextView ftenure = (TextView) mydialog.findViewById(R.id.ftenure);
                TextView fmanager = (TextView) mydialog.findViewById(R.id.fmanager);
                //   TextView fstatus = (TextView) mydialog.findViewById(R.id.fstatus);
                TextView fstatus1 = (TextView) mydialog.findViewById(R.id.fstatus1);
                Button btncancel = (Button) mydialog.findViewById(R.id.btncancel);
                Button btnaccept = (Button) mydialog.findViewById(R.id.btnaccept);
                TextView text2 = (TextView) mydialog.findViewById(R.id.text2);
                // String s =arrayList.get(position).getF_amt();

                offeramount.setText(arrayList.get(position).getF_amt());
                rateofint.setText(arrayList.get(position).getF_roi());
                ftenure.setText(arrayList.get(position).getF_tenure());
                fmanager.setText(arrayList.get(position).getF_approve_by());
                // fstatus.setText(arrayList.get(position).getF_status());

                if (arrayList.get(position).getBorrower_reply_status().equals("0")) {
                    fstatus1.setText("Pending");
                    btnaccept.setVisibility(View.VISIBLE);
                    btncancel.setVisibility(View.VISIBLE);
                    fstatus1.setTextColor(Color.parseColor("#ffffbb33"));
                    text2.setVisibility(View.VISIBLE);
                } else if (arrayList.get(position).getBorrower_reply_status().equals("1")) {
                    fstatus1.setText("Accepted");
                    btnaccept.setVisibility(GONE);
                    btncancel.setVisibility(GONE);
                    fstatus1.setTextColor(Color.parseColor("#33cc33"));
                    text2.setVisibility(GONE);

                } else if (arrayList.get(position).getBorrower_reply_status().equals("2")) {
                    fstatus1.setText("Rejected");
                    fstatus1.setTextColor(Color.parseColor("#ff0000"));
                    btnaccept.setVisibility(GONE);
                    btncancel.setVisibility(GONE);
                    text2.setVisibility(GONE);
                }
                btnaccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isNetworkAvailable(mcontext)) {
                            set = "1";
                            String borreqlid = arrayList.get(position).getBorreqloanid();
                            String famt = arrayList.get(position).getF_amt();
                            setdata(set, borreqlid, famt);
                        } else {
                            Toast.makeText(mcontext, "No Internet Available", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                btncancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isNetworkAvailable(mcontext)) {
                            set = "2";
                            String borreqlid = arrayList.get(position).getBorreqloanid();
                            String famt = arrayList.get(position).getF_amt();
                            setdata(set, borreqlid, famt);
                        } else {
                            Toast.makeText(mcontext, "No Internet Available", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                mydialog.show();
            }
        });
//=============================================End of button Click=========================================================//
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void setdata(final String set, final String borrreqloanid, final String famnt) {
        String serverurl = "https://onistech.in/capitaltrade/android/borrowerreply.php";
//arrayList.get()
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.borrowerReply(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                    String success = jsonObject.getString("success");
                    String status = jsonObject.getString("status");

                    if (success.equals(0) && status.equals(401) ) {
                        session.logoutUser();
                        Toast.makeText(mcontext, "Sorry, sessionn time out!!!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        String code = jsonObject.getString("code");
                        //String message = jsonObject.getString("code");
                        // String check = jsonObject.getString("check");//we should write key name

                        if (code.equals("accepted")) {
                            progressDialog.dismiss();
                            mydialog.dismiss();
                            String fee = jsonObject.getString("profee");
                            //   double processing = ((Double.parseDouble(famnt)* Double.parseDouble(fee))/100);
                            double processing = Double.parseDouble(fee);
                            // String actualpfee= String.valueOf(processing);
                            DecimalFormat formatter = new DecimalFormat("#,###,###.##");
                            String actualpfee = formatter.format(processing);

                            final AlertDialog.Builder imageDialog = new AlertDialog.Builder(mcontext);
                            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(LAYOUT_INFLATER_SERVICE);

                            View customView = inflater.inflate(R.layout.custom_fullimage_dialog, null);
                            imageDialog.setView(customView);
                            TextView text1 = (TextView) customView.findViewById(R.id.text1);
                            TextView ok = (TextView) customView.findViewById(R.id.ok);
                            // image.setImageDrawable(tempImageView.getDrawable());
                            text1.setText(actualpfee + " /- ");
//
//                        imageDialog.create();
//                        imageDialog.show();
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialogAndroid.cancel();
                                    Intent intent = new Intent(mcontext, MainActivity.class);
                                    //intent.putExtra("EXTRA", "openFragment");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("fragmentNumber", 1);
                                    //intent.putExtra("childname",s);
                                    mcontext.startActivity(intent);
                                    ((Activity) mcontext).finish();
                                }
                            });
                            alertDialogAndroid = imageDialog.create();
                            alertDialogAndroid.setCanceledOnTouchOutside(false);
                            alertDialogAndroid.setCancelable(false);
                            imageDialog.setCancelable(false);
                            alertDialogAndroid.show();
                        } else if (code.equals("rejected")) {
                            progressDialog.dismiss();
                            mydialog.dismiss();
                            Toast.makeText(mcontext, "Your Rejection of Proposal is taken.", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(mcontext, MainActivity.class);
                            //intent.putExtra("EXTRA", "openFragment");
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("fragmentNumber", 1);
                            //intent.putExtra("childname",s);
                            mcontext.startActivity(intent);
                            ((Activity) mcontext).finish();
                        }
                    }

                } catch (JSONException e) {

                    Toast.makeText(mcontext, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(,"Sorry ,Something went Wrong Or No Connection !! ", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();

                params.put("id", sesid);
                params.put("borrowerreqloanid", borrreqloanid);
                if (set.equals("1")) {
                    params.put("accept", set);
                } else {
                    params.put("reject", set);
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Updating Request....");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }


    public void download(String str) {
        downloadManager = (DownloadManager) mcontext.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri uri = Uri.parse(str);

        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        Long reference = downloadManager.enqueue(request);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView loannum, name, amount, days, rate, stat, list, agreement, vagree, dagree;
        ImageView img;
        CardView c1;
        LinearLayout l1;
        Button b1, esignbtn, emandate;
        // String loan="",pname="",amount="",tenure="",roi="";


        public MyViewHolder(final View itemView) {
            super(itemView);
            loannum = itemView.findViewById(R.id.loannum);
            name = itemView.findViewById(R.id.name);
            list = itemView.findViewById(R.id.list);
            amount = itemView.findViewById(R.id.amount);
            days = itemView.findViewById(R.id.days);
            // rate= itemView.findViewById(R.id.rate);
            img = itemView.findViewById(R.id.imageView);
            b1 = itemView.findViewById(R.id.offer);
            l1 = itemView.findViewById(R.id.l1);
            stat = itemView.findViewById(R.id.stat);
            agreement = itemView.findViewById(R.id.agreement);
            vagree = itemView.findViewById(R.id.vagree);
            dagree = itemView.findViewById(R.id.dagree);
            esignbtn = itemView.findViewById(R.id.esignbtn);
            emandate = itemView.findViewById(R.id.emandate);
            c1 = itemView.findViewById(R.id.card);
            c1.setOnClickListener(this);
            list.setPaintFlags(list.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            vagree.setPaintFlags(vagree.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            dagree.setPaintFlags(dagree.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            //holder.stat.setText("Approved");


       /* c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = getAdapterPosition();
                String str = arrayList.get(pos).getId();
                Toast.makeText(v.getContext(), ""+str, Toast.LENGTH_SHORT).show();
            }
        });*/

        }


        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            //  String a= arrayList.get(pos).getId();

        }
    }
}
