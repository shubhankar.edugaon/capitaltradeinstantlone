package com.business.capitaltrade;

public class ModelUploadeddoc {

    String code,date,doc,status,sr;
    public ModelUploadeddoc() {
    }
    public ModelUploadeddoc(String code,String date, String doc,String status,String sr)
    {
        this.code = code;
        this.date = date;
        this.doc = doc;
        this.status = status;
        this.sr=sr;
    }

    public String getStatus() {
        return status;
    }

    public String getSr() {
        return sr;
    }

    public void setSr(String sr) {
        this.sr = sr;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }
}
