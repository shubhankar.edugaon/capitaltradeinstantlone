package com.business.capitaltrade;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ForgotPassword extends BaseActivity {
EditText mob,etotp;
LinearLayout linear;
ProgressDialog progressDialog;
    String checkmob="https://onistech.in/capitaltrade/android/checkmobile.php";
    String otpurl="https://onistech.in/capitaltrade/android/otpApi.php";
    String change="",s="",otpValue="";
    CountDownTimer cTimer = null;
    TextView resend;
    Button continuebtn,sendotp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mob=(EditText)findViewById(R.id.mob);
     etotp=(EditText)findViewById(R.id.etotp);
        sendotp=(Button) findViewById(R.id.sendotp);
     resend=(TextView)findViewById(R.id.resend);
        continuebtn=(Button)findViewById(R.id.continuebtn);
        linear=(LinearLayout) findViewById(R.id.linear);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ForgotPassword.this,LoginActivity.class));
        finish();
    }

    public void reset(View view) {
     if(mob.getText().toString().trim().equals(""))
     {
         new AlertDialog.Builder(ForgotPassword.this)
                 //.setTitle("Record New Track")
                 .setMessage("Please fill Mobile Number")
//and some more method calls
                 .show();
     }
     else if(mob.length()!=10)
     {
         new AlertDialog.Builder(ForgotPassword.this)
                 //.setTitle("Record New Track")
                 .setMessage("Mobile Number must be of 10 digits")
//and some more method calls
                 .show();
     }
     else
     {
         if(isNetworkAvailable(getApplicationContext())) {
             checkmob();
         }
         else {
             Toast.makeText(ForgotPassword.this, "Please Check Internet Connection !!!", Toast.LENGTH_SHORT).show();
         }
     }
    }

    public void checkmob(){
        StringRequest stringRequest =  new StringRequest(Request.Method.POST, UrlUtils.checkMobile(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                    String code = jsonObject.getString("code");
                    //String message = jsonObject.getString("code");
                    // String check = jsonObject.getString("check");//we should write key name

                    //Toast.makeText(SignIn.this, ""+check, Toast.LENGTH_SHORT).show();
                    if(code.equals("login_failed"))
                    {
                        progressDialog.dismiss();

                        Toast.makeText(ForgotPassword.this, "Mobile Number not exists. Please Register youself", Toast.LENGTH_SHORT).show();
                    }

                    else if(code.equals("login_success"))
                    //  else if(code.equals("login_success"))
                    {
                        progressDialog.dismiss();
                       // Toast.makeText(ForgotPassword.this, "success", Toast.LENGTH_SHORT).show();
                        again();


                    }
                    else
                    {
                        Toast.makeText(ForgotPassword.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {

                    progressDialog.dismiss();
                    Toast.makeText(ForgotPassword.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ForgotPassword.this,"Server encountered problem !!! ", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> params = new HashMap<String,String>();
                params.put("u_mobile",mob.getText().toString().trim());
                // params.put("u_pass",pass);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ForgotPassword.this);
        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(ForgotPassword.this,R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Verifying Number....");
        progressDialog.setCancelable(false);
        progressDialog.show();


    }


    public void again()
    {
        String mobile=mob.getText().toString().trim();
//        Random otp = new Random();
//
//        StringBuilder builder = new StringBuilder();
//        for (int count = 0; count < 4; count++) {
//            builder.append(otp.nextInt(10));
//        }
//        change = builder.toString().trim();
        //   Toast.makeText(this, ""+change, Toast.LENGTH_SHORT).show();
        s = "+91" + mobile;
        //otpurl = otpurl.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.otpApi(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                            String code = jsonObject.getString("code");
                            if (code.equals("sent")) {
                                otpValue = jsonObject.getString("value");
                                Toast.makeText(ForgotPassword.this, "OTP send Successfully", Toast.LENGTH_SHORT).show();
                                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linear.getLayoutParams();
                                params.setMargins(50, 220, 50, 0);
                                linear.setLayoutParams(params);
                                // t1.setText("We've sent an OTP to the mobile number "+s+". Please enter it below to complete verification.");
                                etotp.setVisibility(View.VISIBLE);
                                continuebtn.setVisibility(View.VISIBLE);
                                mob.setEnabled(false);
                                resend.setVisibility(View.VISIBLE);
                                sendotp.setBackgroundResource(R.drawable.shapeloginbtn);
                                sendotp.setClickable(false);
                                sendotp.setText("OTP Send. Please Wait...");
                                startTimer();
                            } else {
                                Toast.makeText(ForgotPassword.this, "Sorry, Server encountered problem!!!", Toast.LENGTH_SHORT).show();
                            }
                            //  Toast.makeText(signup.this, ""+status, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(ForgotPassword.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
//
                Toast.makeText(getApplicationContext(), "Something went wrong !!!", Toast.LENGTH_SHORT).show();

            }

        }) {@Override
        protected Map<String, String> getParams() throws AuthFailureError {

            Map<String,String> params = new HashMap<String,String>();
            params.put("u_mobile",s);
            params.put("forgot","1");
            return params;
        }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
//            //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(ForgotPassword.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Sending OTP..");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    void startTimer() {
        cTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                // t2.setText("seconds remaining: " + millisUntilFinished / 1000);
                long millis = millisUntilFinished;
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                resend.setText(hms.substring(3,8));//set text
                //    String abc = hms.replaceAll();
                resend.setClickable(false);
            }
            public void onFinish() {
                resend.setClickable(true);
                sendotp.setBackgroundResource(R.drawable.shapelogin);
                sendotp.setClickable(true);
                sendotp.setText("Resend OTP");
                resend.setVisibility(View.GONE);
            }
        };
        cTimer.start();
    }

//    public void resend(View view) {
//        again();
//    }

    public void continueotp(View view) {
        if(etotp.getText().toString().trim().equals(""))
        {
            new AlertDialog.Builder(ForgotPassword.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill OTP")
//and some more method calls
                    .show();
        }else {
            startActivity(new Intent(ForgotPassword.this,SetPasswrd.class)
                    .putExtra("mobile",mob.getText().toString().trim())
                    .putExtra("otp",etotp.getText().toString().trim()));
            finish();
        }
    }

}
