package com.business.capitaltrade;
import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

;

import java.util.ArrayList;

public class RecyclerNoofLoan extends RecyclerView.Adapter<RecyclerNoofLoan.MyViewHolder> {

    ArrayList<ModelNoLoan> arrayList;
    Context context;
    int j=1;
    LayoutInflater inflater;
    private FragmentCommunication mCommunicator;
    public RecyclerNoofLoan()
    {

    }

    public RecyclerNoofLoan(ArrayList<ModelNoLoan> arrayList, Context context, FragmentCommunication communication) {
        this.arrayList = arrayList;
        this.context = context;
        mCommunicator = communication;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v=inflater.inflate(R.layout.noloan,parent,false);
        RecyclerNoofLoan.MyViewHolder myViewHolder = new RecyclerNoofLoan.MyViewHolder(v,mCommunicator);

        return myViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ModelNoLoan model = arrayList.get(position);
        if(arrayList.get(position).getCode().equals("Available")) {
            holder.loanno.setText(arrayList.get(position).getLoanno());
            holder.name.setText(arrayList.get(position).getPname());
            holder.damount.setText(arrayList.get(position).getLamnt());
            holder.ddate.setText(arrayList.get(position).getDdate());





            LoanList fragmentB = new LoanList();
            Bundle bundle = new Bundle();
            bundle.putString("ID", arrayList.get(position).getBorrreqloanid());
            fragmentB.setArguments(bundle);

//            holder.l1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });

        }


        else
        {
            holder.l1.setVisibility(View.GONE);
            Toast.makeText(context, "No Loan  is Disbursed yet.", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        FragmentCommunication mComminication;
        TextView loanno,name,damount,ddate;
        Button btndisbursed;
        LinearLayout l1;



        public MyViewHolder(final View itemView, FragmentCommunication Communicator) {
            super(itemView);
            loanno=itemView.findViewById(R.id.loannum);
            name=itemView.findViewById(R.id.name);
            name=itemView.findViewById(R.id.name);
            damount=itemView.findViewById(R.id.amount);
            l1=itemView.findViewById(R.id.loanview);
            ddate=itemView.findViewById(R.id.ddate);
            btndisbursed=itemView.findViewById(R.id.btndisbursed);
            mComminication=Communicator;
            btndisbursed.setOnClickListener(this);
            //   c1.setOnClickListener(this);



       /* c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = getAdapterPosition();
                String str = arrayList.get(pos).getId();
                Toast.makeText(v.getContext(), ""+str, Toast.LENGTH_SHORT).show();
            }
        });*/

        }

        public boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        @Override
        public void onClick(View v) {
            if(isNetworkAvailable(context )) {
                int pos2 = getAdapterPosition();
                String a2 = arrayList.get(pos2).getBorrreqloanid();
                mComminication.respond(getAdapterPosition(), a2);
            }
            else
            {
                Toast.makeText(context, "No Inernet Available", Toast.LENGTH_SHORT).show();
            }
         //   Toast.makeText(context, ""+a2, Toast.LENGTH_SHORT).show();


        }
    }
}
