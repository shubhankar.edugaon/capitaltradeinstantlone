package com.business.capitaltrade;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;

/**
 * A simple {@link Fragment} subclass.
 */
public class upload_PAN extends BaseFragment {
    @BindView(R.id.iv1)
    ImageView img;
    @BindView(R.id.panimg)
    ImageView image;
    Unbinder unbinder;
    Bitmap bitmap;
    final private int IMG=2;
    final String date[] = new String[1];
    final private int IMG_REQUEST=1;
    Uri imageUri;
    EditText e1,e2,e3;
    SessionManager session;
    String mob="",dob="",id="";
    ProgressDialog progressDialog;
    LinearLayout linearLayout;
    RelativeLayout rl;
    boolean isImageFitToScreen;
    View v;
    public upload_PAN() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

         v= inflater.inflate(R.layout.fragment_upload__pan, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        unbinder = ButterKnife.bind(this,v);
        e2=(EditText)v.findViewById(R.id.e2);
        linearLayout=(LinearLayout)v.findViewById(R.id.click);
        rl=(RelativeLayout) v.findViewById(R.id.panrl);
        session = new SessionManager(getContext().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        id = user.get(SessionManager.KEY_ID);
         mob = user.get( SessionManager.KEY_MOBILE);
         //image.setTag("bc");
        e2.setFilters(new InputFilter[]{new InputFilter.AllCaps(),new InputFilter.LengthFilter(10)});
       // initialize();
        // Toast.makeText(getContext().getApplicationContext(), ""+date[0], Toast.LENGTH_SHORT).show();
//        dialogpicker();

        linearLayout.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 if(isNetworkAvailable(getActivity()))
                 {
                     getinfopan();
                 }
                 else
                 {
                     Toast.makeText(getContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                   //  alertdialog(getActivity());
                 }
             }
         });

        if(isNetworkAvailable(getActivity()))
        {
            chechdatastatus();

        }
        else
        {
            Toast.makeText(getContext(), "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
            //  alertdialog(getActivity());
        }
//        if(isNetworkAvailable(getActivity()))
//        {
//
//        }
//        else
//        {
//            alertdialog(getActivity());
//        }
//
//        image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(isImageFitToScreen) {
//                    isImageFitToScreen=false;
//                    image.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
//                    image.setAdjustViewBounds(true);
//                }else{
//                    isImageFitToScreen=true;
//                    image.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
//                    image.setScaleType(ImageView.ScaleType.FIT_XY);
//                }
//            }
//        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @OnClick(R.id.iv1)
    public void panclick(View view) {
        requestStoragePermission();
     //   Toast.makeText(getContext().getApplicationContext(), "dd", Toast.LENGTH_SHORT).show();

    }

    private void selectImage2() {


        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {


                    selectimage();
                  /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

                    startActivityForResult(intent, 1);*/

                } else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);

                    //if everything is ok we will open image chooser
                   /* Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.putExtra("crop","true");
                    i.putExtra("aspectX",1);
                    i.putExtra("aspectY",1);
                    i.putExtra("outputX",200);
                    i.putExtra("outputY",200);
                    i.putExtra("return-data",true);
                 //   i.putExtra("crop","true");
                    startActivityForResult(i, 2);*/


                } /*else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }*/

            }

        }).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TOdo
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void selectimage(){
        //  Intent intent = new Intent();
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //  intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(i,IMG_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==IMG_REQUEST && resultCode ==RESULT_OK && data !=null){
            Uri path = data.getData();
            //  bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
            bitmap = (Bitmap) data.getExtras().get("data");
            image.setImageBitmap(bitmap);
            image.setTag("mc");
        }

        else if(requestCode==IMG && resultCode ==RESULT_OK && data !=null)
        {
            /* Bundle extras  = data.getExtras();
            bitmap =extras.getParcelable("data");
            iv1.setImageBitmap(bitmap);*/
            //getting the image Uri
            imageUri = data.getData();

            // ImageCropFunction();
            try {
                //getting bitmap object from uri
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageUri);

                //displaying selected image to imageview
             //   image.setImageBitmap(bitmap);
                image.setImageURI(imageUri);
                image.setTag("mc");
                //calling the method uploadBitmap to upload image
                // uploadBitmap(bitmap);
            } catch (IOException e) {
            }
        }
        else {

        }
    }

    private String imagetostring(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inSampleSize=1;
        final float scale=getResources().getDisplayMetrics().density;

       // Display dis=getWindowManager().getDefaultDisplay();
        // int width=dis.getWidth();
        bitmap=Bitmap.createScaledBitmap(bitmap,600,550, true);
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[] imbytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imbytes, Base64.DEFAULT);

    }

public void getinfopan()
{
   // ((PersonalInfo)getActivity()).setCurrentItem (1, true);
      //  pannum=e2.getText().toString().trim();
    String backgroundImageName = String.valueOf(image.getTag());

     if(e2.getText().toString().trim().equals(""))
    {
        new AlertDialog.Builder(getActivity())
                //.setTitle("Record New Track")
                .setMessage("Please fill  Pan Number")
                .show();
    }
     else if(e2.getText().toString().trim().length() !=10)
     {
         new AlertDialog.Builder(getActivity())
                 //.setTitle("Record New Track")
                 .setMessage("Pan Card must be of 10 digits")
                 .show();
     }
    else if(backgroundImageName.equals("bc"))
     {
         Toast.makeText(getContext(), "please Select/Capture Pan card image", Toast.LENGTH_SHORT).show();
     }

    else {


        uploadpan();
    }

}

    private void uploadpan() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";
        String urll="https://onistech.in/capitaltrade/android/paninfo.php";
        ;
       // urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.uploadPan(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                       // Toast.makeText(getContext().getApplicationContext(), ""+response, Toast.LENGTH_SHORT).show();
                        try {
                            // ar.clear();
                            //for (int i = 0; i <=response.length(); i++) {
                                JSONArray jsonArray = new JSONArray(response);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String success = jsonObject.getString("success");
                            //String status = jsonObject.getString("status");

                            if (success.equals(0)) {
                                session.logoutUser();
                                Toast.makeText(getActivity(), "Sorry, sessionn time out!!!", Toast.LENGTH_LONG).show();
                                getActivity().finish();
                            }
                            else {
                                //get value of first index
                                // String code = jsonObject.getString("code");
                                //String message = jsonObject.getString("code"); //we should write key name;

                                String code = jsonObject.getString("code");
                                image.setTag(null);
                                if (code.equals("updated")) {
                                    Toast.makeText(getContext().getApplicationContext(), "PAN card saved successfully !!", Toast.LENGTH_SHORT).show();
                                    String pan = jsonObject.getString("pan");
                                    String imag1 = jsonObject.getString("image");
                                    byte[] data = Base64.decode(pan, Base64.DEFAULT);
                                    e2.setText(new String(data, StandardCharsets.UTF_8));
                                    //  String encodePan = new String(data, StandardCharsets.UTF_8);
                                    //   Toast.makeText(getContext(), ""+imag1, Toast.LENGTH_SHORT).show();
                                    //  e1.setText(name);

                                    // e3.setText(dob);
                                    // e1.setEnabled(false);
                                    // e1.setCursorVisible(false);
                                    e2.setEnabled(false);
                                    e2.setCursorVisible(false);
//                                    e3.setEnabled(false);
//                                    e3.setCursorVisible(false);
                                    img.setVisibility(GONE);
                                    linearLayout.setClickable(false);
                                    linearLayout.setBackgroundColor(Color.parseColor("#BDBDBD"));
                                    Glide.with(getContext().getApplicationContext())
                                            .load(imag1)
                                            //  .apply(new RequestOptions().override(1200, 200))
                                            .apply(new RequestOptions().disallowHardwareConfig()
                                                    .override(Target.SIZE_ORIGINAL)
                                                    .dontTransform()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true)
                                                    .dontAnimate()
                                                    .placeholder(R.drawable.loading)
                                                    //.fitCenter()

                                                    .format(DecodeFormat.PREFER_ARGB_8888))
                                            .into(image);
                                    ((PersonalInfo) getActivity()).setCurrentItem(1, true);
                                } else if (code.equals("not_updated")) {
                                    Toast.makeText(getContext().getApplicationContext(), "Failed to update,Try Again!!", Toast.LENGTH_SHORT).show();
                                } else if (code.equals("failed")) {
                                    Toast.makeText(getContext().getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (JSONException e) {
                            progressDialog.dismiss();

                             Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

               Toast.makeText(getContext().getApplicationContext(), "Server has encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                byte[] data = e2.getText().toString().trim().getBytes(StandardCharsets.UTF_8);
               // String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                params.put("panno",Base64.encodeToString(data, Base64.DEFAULT));
              //  params.put("dob",dob);
                params.put("id",id);
                params.put("u_image",imagetostring(bitmap));
                params.put("status","1");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void chechdatastatus() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";

        String urll="https://onistech.in/capitaltrade/android/showpan.php?id="+id+"";
        ;
       // urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.showPan(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        // Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        try {
                            rl.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = new JSONArray(response);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                //get value of first index

                            String success = jsonObject.getString("success");
                           // String status1 = jsonObject.getString("status");

                            if (success.equals("0")) {
                                session.logoutUser();
                                Toast.makeText(getActivity(), "Sorry, sessionn time out!!!", Toast.LENGTH_LONG).show();
                                getActivity().finish();
                            }
                            else {
                                //String status = jsonObject.getString("pan");
                                if (!success.equals("")) {
                                    image.setTag(null);
                                    String pan = jsonObject.getString("pan");
                                    String imag = jsonObject.getString("image");

                                    byte[] data = Base64.decode(pan, Base64.DEFAULT);
                                    e2.setText(new String(data, StandardCharsets.UTF_8));
                                    //e2.setText(pan);

                                    e2.setEnabled(false);
                                    e2.setCursorVisible(false);
                                    img.setVisibility(GONE);
                                    linearLayout.setClickable(false);
                                    linearLayout.setBackgroundColor(Color.parseColor("#BDBDBD"));
                                    try {
                                        Glide.with(getContext().getApplicationContext())
                                                .load(imag)
                                                //  .apply(new RequestOptions().override(1200, 200))
                                                .apply(new RequestOptions().disallowHardwareConfig()
                                                        .override(Target.SIZE_ORIGINAL)
                                                        // .dontTransform()
                                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                        .skipMemoryCache(true)
                                                        .dontAnimate()
                                                        .placeholder(R.drawable.loading)
                                                        //.fitCenter()
                                                        .format(DecodeFormat.PREFER_ARGB_8888))
                                                .into(image);
                                    } catch (Exception e) {
                                        Log.e("Glide Error", e.getMessage());

                                    }
                                    PersonalInfo activity = (PersonalInfo) getActivity();
                                    String getData = activity.s1;
                                    if (!getData.equals("")) {
                                        img.setVisibility(View.VISIBLE);
                                        e2.setEnabled(true);
                                        e2.setCursorVisible(true);
                                        linearLayout.setClickable(true);
                                        linearLayout.setBackgroundColor(Color.parseColor("#0099cc"));
                                    } else {
                                        //img.setVisibility(GONE);
                                    }
                                } else {

                                }
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                             Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext().getApplicationContext(), "Server has encountered problem !!!", Toast.LENGTH_SHORT).show();
            }

        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", AppSession.logedInToken);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }


    private void requestStoragePermission() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            //  Toast.makeText(getContext().getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            selectImage2();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getContext().getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}
