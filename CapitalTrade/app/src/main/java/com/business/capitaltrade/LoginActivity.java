package com.business.capitaltrade;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * A login screen that offers login via email/password.
 */

@RequiresApi(api = Build.VERSION_CODES.O)
public class LoginActivity<headerAndPayloadHashed, secret, signature> extends BaseActivity {
    EditText e1, e2;
    ProgressDialog progressDialog;
    String mob = "", mobile = "";
    CheckBox c1;
    String serverurl = "https://onistech.in/capitaltrade/android/login.php";
    String serverurl1 = "https://onistech.in/capitaltrade/android/checkmobile.php";
    SessionManager session;
    String secret = null;
    final Context c = this;
    TextView t1, ctl_text;
    private byte[] encode =  null;
    private PrefManager prefManager;

    public LoginActivity() throws JSONException {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // prefManager = new PrefManager(this);
        // String s = prefManager.getLanguage(getApplicationContext());
        // Toast.makeText(LoginActivity.this, ""+s, Toast.LENGTH_SHORT).show();
        setContentView(R.layout.activity_login);
        session = new SessionManager(getApplicationContext());
        e1 = (EditText) findViewById(R.id.user);
        e2 = (EditText) findViewById(R.id.pass);
        c1 = (CheckBox) findViewById(R.id.check);
        t1 = (TextView) findViewById(R.id.sign);
        ctl_text = (TextView) findViewById(R.id.ctl_textView);
        t1.setPaintFlags(t1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        c1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (c1.isChecked()) {
                    e2.setTransformationMethod(null);
                } else {
                    e2.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });

        //preLogin();
//        prefManager = new PrefManager(this);
//         String s = prefManager.getLanguage(getApplicationContext());
//         Toast.makeText(LoginActivity.this, ""+s, Toast.LENGTH_SHORT).show();

        changeLanguage();

        //requestStoragePermission();
//        String lang = "eng";
//        if (PrefManager.getLanguage(getApplicationContext()).equals("hindi"))
//            lang = "hi";
//        Resources res = getApplicationContext().getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Locale locale = new Locale(lang);
//        android.content.res.Configuration conf = res.getConfiguration();
//        conf.locale = locale;
//        Locale.setDefault(locale);
//        res.updateConfiguration(conf, dm);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            getApplicationContext().createConfigurationContext(conf);
//        } else {
//            res.updateConfiguration(conf, dm);
//        }


        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            // requestStoragePermission();
        }


    }

    private void getdata() {
        mob = e1.getText().toString().trim();
        // pass=e2.getText().toString().trim();
        if (mob.equals("")) {
            new AlertDialog.Builder(LoginActivity.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Mobile Number")
//and some more method calls
                    .show();
        } else if (mob.length() != 10) {
            new AlertDialog.Builder(LoginActivity.this)
                    //.setTitle("Record New Track")
                    .setMessage("Mobile Number must be of 10 digits")
//and some more method calls
                    .show();
        } else if (e2.getText().toString().trim().equals("")) {
            new AlertDialog.Builder(LoginActivity.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Password")
//and some more method calls
                    .show();
        } else {

            getvolley();
        }
    }

    private void preLogin() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.preLogin(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject= jsonArray.getJSONObject(0);
                            String code = jsonObject.getString("ptoken");
                            AppSession.lToken = code;
                            String tokan = AppSession.lToken;
                            if (tokan != null){
                                getdata();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(LoginActivity.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this,"Server encountered some problem !!!", Toast.LENGTH_SHORT).show();
            }

        })  {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String mob = e1.getText().toString().trim();
                    params.put("prelogin","test");
                    params.put("mobile",mob);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(LoginActivity.this,R.style.AppCompatAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void getvolley() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.login(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                    String code = jsonObject.getString("code");
                    //String message = jsonObject.getString("code");
                    // String check = jsonObject.getString("check");//we should write key name

                    //Toast.makeText(SignIn.this, ""+check, Toast.LENGTH_SHORT).show();
                    if (code.equals("login_failed")) {
                        progressDialog.dismiss();
                        e1.setText("");
                        e2.setText("");
                        Toast.makeText(LoginActivity.this, "" + code, Toast.LENGTH_SHORT).show();
                    } else if (code.equals("login_success"))
                    //  else if(code.equals("login_success"))
                    {
                        progressDialog.dismiss();
                        String status = jsonObject.getString("status");
                        if (status.equals("Approved")) {
                            AppSession.logedInToken = jsonObject.getString("token");
                            String name = jsonObject.getString("name");
                            //String email= jsonObject.getString("email");
                            String id = jsonObject.getString("id");
                            String mobile = jsonObject.getString("mobile");
                            String bcode = jsonObject.getString("bcode");
                            session.createLoginSession(id, mobile, bcode);
                            Toast.makeText(LoginActivity.this, "Welcome " + name, Toast.LENGTH_LONG).show();
                            // startActivity(new Intent(LoginActivity.this,MainActivity.class));
                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            i.putExtra("fragmentNumber", 2);
                            startActivity(i);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, "Please wait for Admin Approval !!", Toast.LENGTH_SHORT).show();
                        }
                    }


                } catch (JSONException e) {
                    Log.d("error", "error log " + e.getMessage());
                    progressDialog.dismiss();
                    //Toast.makeText(LoginActivity.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(LoginActivity.this, "Something went wrong !", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                //   Toast.makeText(LoginActivity.this,"Sorry ,Something went Wrong Or No Connection !! ", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("u_mobile", mob);
                try {
                    String passHash = GFG.toHexString(GFG.getSHA(e2.getText().toString().trim()));
                    String preTokenHash = GFG.toHexString(GFG.getSHA(AppSession.preLoginToken.toString().trim()));
                    String token = AppSession.lToken;
                    String has = token+passHash;
                    String salt = GFG.toHexString(GFG.getSHA(has));

                    params.put("u_pass", salt);
                } catch (NoSuchAlgorithmException e) {
                    Toast.makeText(LoginActivity.this, "Something Went wrong !!!", Toast.LENGTH_SHORT).show();
                }
                //params.put("u_pass","");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Authenticating....");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

@RequiresApi(api = Build.VERSION_CODES.O)
String[] parts = AppSession.logedInToken.split("\\.");

//    JSONObject header = new JSONObject(decode(parts[0]));
//    JSONObject payload = new JSONObject(decode(parts[1]));
//     String signature = decode(parts[2]);



    @RequiresApi(api = Build.VERSION_CODES.O)
    private static String decode(String encodedString) {
        return new String(Base64.getUrlDecoder().decode(encodedString));
    }

    //payload.getLong("exp") > (System.currentTimeMillis() / 1000)

    private String hmacSha256(String data, String secret) {
        try {
            byte[] hash = secret.getBytes(StandardCharsets.UTF_8);
            Mac sha256Hmac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKey = new SecretKeySpec(hash, "HmacSHA256");
            sha256Hmac.init(secretKey);

            byte[] signedBytes = sha256Hmac.doFinal(data.getBytes(StandardCharsets.UTF_8));
            return Uri.encode( String.valueOf( signedBytes ) );
        } catch (NoSuchAlgorithmException | InvalidKeyException ex) {
            //Logger.getLogger(AppSession.logedInToken).log(Level.SEVERE, ex.getMessage(), ex);
            return null;
        }
    }
    String headerAndPayloadHashed;


    public void signup(View view) {
        startActivity(new Intent(LoginActivity.this, signup.class));
        finish();
    }

    public void signin(View view) {
        if (isNetworkAvailable(getApplicationContext())) {
            //getdata();
            preLogin();
        } else {
            Toast.makeText(LoginActivity.this, "Please Check Internet Connection !!!", Toast.LENGTH_SHORT).show();
        }

        //  getvolley();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.CAMERA)
//                        Manifest.permission.RECEIVE_SMS,
//                        Manifest.permission.SEND_SMS,
//                        Manifest.permission.EXPAND_STATUS_BAR)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        } else {
                            showSettingsDialog();
                            //   Toast.makeText(getApplicationContext(), "Something ", Toast.LENGTH_SHORT).show();
                        }

//                        // check for permanent denial of any permission
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            Toast.makeText(getApplicationContext(), "SomePermission Denied", Toast.LENGTH_SHORT).show();
//                            // show alert dialog navigating to Settings
//                            showSettingsDialog();
//                        }

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    public void otp1(View view) {
        mobile = e1.getText().toString().trim();
        if (mobile.equals("")) {
            new AlertDialog.Builder(LoginActivity.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Mobile Number")
//and some more method calls
                    .show();
        } else if (mobile.length() != 10) {
            new AlertDialog.Builder(LoginActivity.this)
                    //.setTitle("Record New Track")
                    .setMessage("Mobile Number must be of 10 digits")
//and some more method calls
                    .show();
        } else {
            if (isNetworkAvailable(getApplicationContext())) {
                getvolley1();
            } else {
                Toast.makeText(LoginActivity.this, "Please Check Internet Connection !!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getvolley1() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.checkMobile(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                    String code = jsonObject.getString("code");
                   // AppSession.logedInToken = jsonObject.getString("token");
                    //String message = jsonObject.getString("code");
                    // String check = jsonObject.getString("check");//we should write key name


                    //Toast.makeText(SignIn.this, ""+check, Toast.LENGTH_SHORT).show();
                    if (code.equals("login_failed")) {
                        progressDialog.dismiss();

                        Toast.makeText(LoginActivity.this, "Mobile Number not exists. Please Register with us.", Toast.LENGTH_SHORT).show();
                    } else if (code.equals("login_success"))
                    //  else if(code.equals("login_success"))
                    {


                        progressDialog.dismiss();
                        Intent intent = new Intent(LoginActivity.this, LoginOtp.class);
                        intent.putExtra("otp", mobile);
                        startActivity(intent);
                        finish();


                    } else {
                        Toast.makeText(LoginActivity.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("u_mobile", mobile);
                // params.put("u_pass",pass);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Verifying Number....");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestStoragePermission();
    }

    public void forgot(View view) {
        startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
        finish();
    }
}

