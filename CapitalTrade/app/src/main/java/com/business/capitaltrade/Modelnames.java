package com.business.capitaltrade;

public class Modelnames {
    int sr;
    String name;
    public Modelnames() {
    }
    public Modelnames(int sr,String name)
    {

        this.sr=sr;
        this.name=name;
    }

    public int getSr() {
        return sr;
    }

    public void setSr(int sr) {
        this.sr = sr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
