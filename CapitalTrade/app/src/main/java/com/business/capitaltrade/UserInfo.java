package com.business.capitaltrade;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;

public class UserInfo extends BaseActivity {
    ProgressDialog progressDialog;
    EditText e1;
    //    @BindView(R.id.gender)
//    TextView gender;
    RadioGroup rg;
    RelativeLayout relativeLayout;
    RadioButton rb1, rb;
    //    @BindView(R.id.religion)
//    TextView religion;
//    @BindView(R.id.qualification)
//    TextView qualification;
    @BindView(R.id.pinalert)
    TextView pinalert;
    @BindView(R.id.stateview)
    TextView stateview;
    @BindView(R.id.changestate)
    TextView changestate;
    @BindView(R.id.changecity)
    TextView changecity;
    @BindView(R.id.cityview)
    TextView cityview;
    @BindView(R.id.father)
    EditText father;
    @BindView(R.id.spouse)
    EditText spouse;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.dob)
    EditText dob;
    @BindView(R.id.pin)
    EditText pin;
    @BindView(R.id.landmark)
    EditText landmark;
    @BindView(R.id.linear)
    LinearLayout submit;
    BottomSheetDialog dialog;
    Calendar c;
    TextView male, female, hindu, muslim, sikh, christian, other, primary, senior, high, under, post, doctor, single, married, widow, divorce;
    LinearLayout l1, l2, l3, l4;
    String fname = "", sname = "", uaddress = "", ugender = "", ureligion = "", uqualification = "", umaritial = "", id = "", birthdate = "", ustate = "",
            ucity = "", pincode = "", ulandmark = "", uname = "", uemail = "", religionvalue = "", qualifyvalue = "", mstatusvalue = "",
            namevalue = "", emailvalue = "", gendervalue = "", dobvalue = "", fathernamevalue = "", spousenamename = "", addressnamevalue = "",
            pincodevalue = "", landmarkvalue = "";
    final String date[] = new String[1];
    String serverurl = "https://onistech.in/capitaltrade/android/personalinfo.php";
    SessionManager session;
    DatePickerDialog picker;
    int age;
    String state;
    int stateidint;
    String cityvalue = "", statevalue = "";
    String city;
    int cityidint;
    String url = "https://onistech.in/capitaltrade/android/state.php";
    String urlPincode = "https://onistech.in/capitaltrade/android/checkpincode.php";
    HashMap<String, String> states = new HashMap<>();
    HashMap<String, String> cities = new HashMap<>();
    Spinner s1, s2, religion, qualification, mstatus;
    String idz = "";
    EditText name, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        session = new SessionManager(UserInfo.this);
        ButterKnife.bind(this);
        e1 = (EditText) findViewById(R.id.address);
        s1 = (Spinner) findViewById(R.id.state);
        s2 = (Spinner) findViewById(R.id.city);
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        religion = (Spinner) findViewById(R.id.religion);
        qualification = (Spinner) findViewById(R.id.qualification);
        mstatus = (Spinner) findViewById(R.id.mstatus);

        View view = getLayoutInflater().inflate(R.layout.bottomsheetgen, null);
        dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);
        male = (TextView) dialog.findViewById(R.id.male);
        female = (TextView) dialog.findViewById(R.id.female);
        hindu = (TextView) dialog.findViewById(R.id.hindu);
        muslim = (TextView) dialog.findViewById(R.id.muslim);
        sikh = (TextView) dialog.findViewById(R.id.sikh);
        christian = (TextView) dialog.findViewById(R.id.chritian);
        other = (TextView) dialog.findViewById(R.id.other);
        high = (TextView) dialog.findViewById(R.id.high);
        senior = (TextView) dialog.findViewById(R.id.senior);
        primary = (TextView) dialog.findViewById(R.id.primary);
        under = (TextView) dialog.findViewById(R.id.undergradute);
        post = (TextView) dialog.findViewById(R.id.post);
        doctor = (TextView) dialog.findViewById(R.id.doctor);
        single = (TextView) dialog.findViewById(R.id.single);
        married = (TextView) dialog.findViewById(R.id.married);
        widow = (TextView) dialog.findViewById(R.id.widow);
        divorce = (TextView) dialog.findViewById(R.id.divorce);
        rg = (RadioGroup) findViewById(R.id.radio);

        rb = (RadioButton) findViewById(R.id.rb);
        rb1 = (RadioButton) findViewById(R.id.rb1);
        l1 = (LinearLayout) dialog.findViewById(R.id.l1);
        l2 = (LinearLayout) dialog.findViewById(R.id.l2);
        l3 = (LinearLayout) dialog.findViewById(R.id.l3);
        l4 = (LinearLayout) dialog.findViewById(R.id.l4);
        relativeLayout = (RelativeLayout) findViewById(R.id.container);
        initialize();


        dialogpicker();
        HashMap<String, String> user = session.getUserDetails();
        id = user.get(SessionManager.KEY_ID);

        HashMap<String, String> user1 = session.getPersonalDetails();

        // name = user.get(SessionManager.KEY_NAME);

        // email
        namevalue = user1.get(SessionManager.KEY_NAME);

        if (namevalue == null) {
            // Toast.makeText(this, ""+namevalue, Toast.LENGTH_SHORT).show();
            setstaticadapters();
            checkinfo();
            mc();

        } else {
            //  Toast.makeText(this, "" + namevalue, Toast.LENGTH_SHORT).show();

            namevalue = user1.get(SessionManager.KEY_NAME);
            emailvalue = user1.get(SessionManager.KEY_EMAIL);
            gendervalue = user1.get(SessionManager.KEY_GENDER);
            dobvalue = user1.get(SessionManager.KEY_DOB);
            religionvalue = user1.get(SessionManager.KEY_RELIGION);
            qualifyvalue = user1.get(SessionManager.KEY_QUALIFICATION);
            mstatusvalue = user1.get(SessionManager.KEY_MARTIAL);
            fathernamevalue = user1.get(SessionManager.KEY_FATHERNAME);
            spousenamename = user1.get(SessionManager.KEY_SPOUSENAME);
            addressnamevalue = user1.get(SessionManager.KEY_ADDRESS);
            statevalue = user1.get(SessionManager.KEY_STATE);
            cityvalue = user1.get(SessionManager.KEY_CITY);
            pincodevalue = user1.get(SessionManager.KEY_PINCODE);
            landmarkvalue = user1.get(SessionManager.KEY_LANDMARK);
            //  Toast.makeText(this, ""+dobvalue, Toast.LENGTH_SHORT).show();
            stateview.setVisibility(View.VISIBLE);
            cityview.setVisibility(View.VISIBLE);
            s1.setVisibility(GONE);
            s2.setVisibility(GONE);
            changestate.setVisibility(View.VISIBLE);
            changecity.setVisibility(View.VISIBLE);
            stateview.setText(statevalue);
            cityview.setText(cityvalue);


            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.religion));
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            religion.setAdapter(dataAdapter);

            if (religionvalue != "") {
                //  Toast.makeText(UserInfo.this, "got", Toast.LENGTH_SHORT).show();
                int spinnerPosition = dataAdapter.getPosition(religionvalue);
                religion.setSelection(spinnerPosition);
            } else if (religionvalue.equals("")) {
                //  Toast.makeText(UserInfo.this, ""+religionvalue, Toast.LENGTH_SHORT).show();
                // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
                // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                religion.setAdapter(dataAdapter);
            }


            ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.qualification));
            dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            qualification.setAdapter(dataAdapter1);


            if (qualifyvalue != "") {

                int spinnerPosition = dataAdapter1.getPosition(qualifyvalue);
                qualification.setSelection(spinnerPosition);
            } else if (qualifyvalue.equals("")) {
                // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
                // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                qualification.setAdapter(dataAdapter);
            }


            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.martial));
            dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mstatus.setAdapter(dataAdapter2);

            if (mstatusvalue != "") {

                int spinnerPosition = dataAdapter2.getPosition(mstatusvalue);
                mstatus.setSelection(spinnerPosition);
            } else if (mstatusvalue.equals("")) {
                // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
                // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mstatus.setAdapter(dataAdapter2);
            }


            if (gendervalue.equals("Male")) {
                rb.setChecked(true);
            } else {
                rb1.setChecked(true);
            }
            name.setText(namevalue);
            email.setText(emailvalue);
            dob.setText(dobvalue);
            //   religion.setText(rel);
            //  qualification.setText(qual);
            //  mstatus.setText(mstat);
            father.setText(fathernamevalue);
            spouse.setText(spousenamename);
            address.setText(addressnamevalue);
            pin.setText(pincodevalue);
            landmark.setText(landmarkvalue);
            //mc();

        }

        changestate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s1.setVisibility(View.VISIBLE);
                s2.setVisibility(View.VISIBLE);
                changestate.setVisibility(GONE);
                changecity.setVisibility(GONE);
                mc();
            }
        });
        changecity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s1.setVisibility(View.VISIBLE);
                s2.setVisibility(View.VISIBLE);
                changestate.setVisibility(GONE);
                changecity.setVisibility(GONE);
                mc();
            }
        });

        //   mc1();
        getgender();
        spinnervalue();
        ValueForReligion();
        pinCodeValue();

    }

    public void spinnervalue() {
        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (!isSpinnerTouched)
//                    return;
                statevalue = parent.getItemAtPosition(position).toString().trim();
                String selectedItem = parent.getItemAtPosition(position).toString();
                idz = states.get(selectedItem);//this is your selected item
                //   Toast.makeText(UserInfo.this, ""+ustate, Toast.LENGTH_SHORT).show();
                mc1();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        s2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (!isSpinnerTouched)
//                    return;
                cityvalue = parent.getItemAtPosition(position).toString().trim(); //this is your selected item
                //  Toast.makeText(UserInfo.this, ""+ucity, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

    }

    public void pinCodeValue() {
        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //  Toast.makeText(MainActivity.this, "on change", Toast.LENGTH_SHORT).show();
                //  int i=5;
                if (s.length() == 6) {
                    pincodeCheck();
                    // Toast.makeText(UserInfo.this, "done", Toast.LENGTH_SHORT).show();
                } else {

                }
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
    }

    public void ValueForReligion() {
        religion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (!isSpinnerTouched)
//                    return;
                ureligion = parent.getItemAtPosition(position).toString().trim();
                // Toast.makeText(UserInfo.this, ""+ureligion, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        mstatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (!isSpinnerTouched)
//                    return;
                umaritial = parent.getItemAtPosition(position).toString().trim(); //this is your selected item
                //   Toast.makeText(UserInfo.this, ""+umaritial, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


        qualification.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (!isSpinnerTouched)
//                    return;
                uqualification = parent.getItemAtPosition(position).toString().trim(); //this is your selected item
                //  Toast.makeText(UserInfo.this, ""+uqualification, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


    }


    private void pincodeCheck() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.checkPincode(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String code = jsonObject.getString("code");
                            //                          InputMethodManager inputManager = (InputMethodManager)UserInfo.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                            //Toast.makeText(LoanRequest.this, ""+code, Toast.LENGTH_LONG).show();
                            // String message=jsonObject.getString("message");
                            if (code.equals("Available")) {

                                progressDialog.dismiss();
                                //                              inputManager.hideSoftInputFromWindow(UserInfo.this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                                pinalert.setVisibility(View.VISIBLE);
                                pinalert.setTextColor(Color.parseColor("#33cc33"));
                                pinalert.setText(R.string.pinalert);
                            } else if (code.equals("Not Available")) {
                                progressDialog.dismiss();
//                                inputManager.hideSoftInputFromWindow(UserInfo.this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                                pinalert.setVisibility(View.VISIBLE);
                                pinalert.setTextColor(Color.parseColor("#ff1a1a"));
                                pinalert.setText(R.string.pinalertnotavail);
                                animateView(pinalert);
                            } else {
                                progressDialog.dismiss();
                                pin.setText("");
                                Toast.makeText(UserInfo.this, "Sorry, Something went wrong !!!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            Toast.makeText(UserInfo.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Toast.makeText(UserInfo.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("upincode", pin.getText().toString().trim());

                //   params.put("status","1");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(UserInfo.this);
        requestQueue.add(stringRequest);
        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(UserInfo.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Checking Availability...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

//    @OnClick(R.id.gender)
//    public void showBottomSheetDialog() {
//        l1.setVisibility(View.VISIBLE);
//        l2.setVisibility(GONE);
//        l3.setVisibility(GONE);
//        l4.setVisibility(GONE);
//        male.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ugender = male.getText().toString();
//                gender.setText(ugender);
//                dialog.dismiss();
//            }
//        });
//        female.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ugender = female.getText().toString();
//                gender.setText(ugender);
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//    }

    public void initialize() {
        if (date[0] == null) {
            date[0] = "";
            //Toast.makeText(getContext().getApplicationContext(), "date = "+date[0], Toast.LENGTH_SHORT).show();
        }
    }

    private void dialogpicker() {

        // final String date[] = new String[1];
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                final int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                // date picker dialog
                picker = new DatePickerDialog(UserInfo.this,
                        R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        c = Calendar.getInstance();
                        c.set(Calendar.YEAR, year);
                        c.set(Calendar.MONTH, monthOfYear);
                        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        StringBuilder sb = new StringBuilder();
                               /* date[0] = sb.append(dayOfMonth)
                                        .append("/").append(monthOfYear + 1).append("/").append(year).toString();*/
                        int mahina = monthOfYear + 1;
                        int din = dayOfMonth;
                        if (mahina < 10 && din < 10) {

                            date[0] = sb.append("0" + dayOfMonth)
                                    .append("-").append("0" + mahina).append("-").append(year).toString();


                        } else if (mahina < 10 && din >= 10) {
                            date[0] = sb.append(dayOfMonth)
                                    .append("-").append("0" + mahina).append("-").append(year).toString();
                        } else if (mahina >= 10 && din < 10) {
                            date[0] = sb.append("0" + dayOfMonth)
                                    .append("-").append(monthOfYear + 1).append("-").append(year).toString();
                        } else {
                            date[0] = sb.append(dayOfMonth)
                                    .append("-").append(monthOfYear + 1).append("-").append(year).toString();
                        }


                        //   dob.setText(date[0]);
                        CalcAge();
                    }
                }, year, month, day);
                //picker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                picker.show();
            }
        });


    }

//        @OnClick(R.id.qualification)
//
//        public void qualify() {
//        l1.setVisibility(GONE);
//        l2.setVisibility(GONE);
//       l3.setVisibility(View.VISIBLE);
//       l4.setVisibility(GONE);
//            primary.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    uqualification= post.getText().toString();
//                    qualification.setText(uqualification);
//                    dialog.dismiss();
//                }
//            });
//            high.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    uqualification= high.getText().toString();
//                    qualification.setText(uqualification);
//                    dialog.dismiss();
//                }
//            });
//            senior.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    uqualification= senior.getText().toString();
//                    qualification.setText(uqualification);
//                    dialog.dismiss();
//                }
//            });
//            under.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    uqualification= under.getText().toString();
//                    qualification.setText(uqualification);
//                    dialog.dismiss();
//                }
//            });
//            post.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    uqualification= post.getText().toString();
//                    qualification.setText(uqualification);
//                    dialog.dismiss();
//                }
//            });
//
//            doctor.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    uqualification= doctor.getText().toString();
//                    qualification.setText(uqualification);
//                    dialog.dismiss();
//                }
//            });
//            dialog.show();
//    }
//
//
//    @OnClick(R.id.mstatus)
//    public void mstat() {
//        l1.setVisibility(GONE);
//        l2.setVisibility(GONE);
//        l3.setVisibility(GONE);
//        l4.setVisibility(View.VISIBLE);
//        single.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               umaritial= single.getText().toString();
//                mstatus.setText(umaritial);
//                dialog.dismiss();
//            }
//        });
//        married.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                umaritial= married.getText().toString();
//                mstatus.setText(umaritial);
//                dialog.dismiss();
//            }
//        });
//        widow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                umaritial= widow.getText().toString();
//                mstatus.setText(umaritial);
//                dialog.dismiss();
//            }
//        });
//        divorce.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                umaritial= divorce.getText().toString();
//                mstatus.setText(umaritial);
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//    }


//    @OnClick(R.id.religion)
//    public void religion() {
//        l1.setVisibility(GONE);
//        l3.setVisibility(GONE);
//        l2.setVisibility(View.VISIBLE);
//        l4.setVisibility(GONE);
//        hindu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ureligion= hindu.getText().toString();
//                religion.setText(ureligion);
//                dialog.dismiss();
//            }
//        });
//        muslim.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ureligion= muslim.getText().toString();
//                religion.setText(ureligion);
//                dialog.dismiss();
//            }
//        });
//        muslim.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ureligion= muslim.getText().toString();
//                religion.setText(ureligion);
//                dialog.dismiss();
//            }
//        });
//        sikh.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ureligion= sikh.getText().toString();
//                religion.setText(ureligion);
//                dialog.dismiss();
//            }
//        });
//        christian.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ureligion= christian.getText().toString();
//                religion.setText(ureligion);
//                dialog.dismiss();
//            }
//        });
//
//        other.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ureligion= other.getText().toString();
//                religion.setText(ureligion);
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//    }

    public void get(String uname, String uemail, String gen, String birth, String qual, String mstat, String dad, String cname, String cadd, String pcode, String lanmark) {

        //    gender.setText(gen);
        if (gen.equals("Male")) {
            rb.setChecked(true);
        } else {
            rb1.setChecked(true);
        }
        name.setText(uname);
        email.setText(uemail);
        dob.setText(birth);
        //   religion.setText(rel);
        //  qualification.setText(qual);
        //  mstatus.setText(mstat);
        father.setText(dad);
        spouse.setText(cname);
        address.setText(cadd);
        pin.setText(pcode);
        landmark.setText(lanmark);
    }


    public void setstaticadapters() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.religion));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        religion.setAdapter(dataAdapter);

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.qualification));
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        qualification.setAdapter(dataAdapter1);

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.martial));
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mstatus.setAdapter(dataAdapter2);

    }


    public void getdata() {
        uname = name.getText().toString().trim();
        uemail = email.getText().toString().trim();
        fname = father.getText().toString().trim();
        sname = spouse.getText().toString().trim();
        uaddress = address.getText().toString().trim();
        birthdate = dob.getText().toString().trim();
        //  ugender=gender.getText().toString().trim();
        // ureligion= religion.getText().toString().trim();
        //  uqualification=qualification.getText().toString().trim();
        //umaritial=mstatus.getText().toString().trim();
        pincode = pin.getText().toString().trim();
        ulandmark = landmark.getText().toString().trim();
        if (uname.equals("")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please enter Name")
//and some more method calls
                    .show();
        } else if (gendervalue.equals("")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please Select Gender")
//and some more method calls
                    .show();
        } else if (birthdate.equals("")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please Select Date Of Birth")
//and some more method calls
                    .show();
        } else if (ureligion.equals("Select Religion")) {

            // new Button(new ContextThemeWrapper(this, R.style.ButtonText), null, 0);
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please Select Religion")
//and some more method calls
                    .show();
        } else if (uqualification.equals("Select")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please Select Qualification")
//and some more method calls
                    .show();
        } else if (umaritial.equals("Select")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please Select Martial Status")
//and some more method calls
                    .show();
        } else if (fname.equals("")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Father's Name")
//and some more method calls
                    .show();
        } else if (statevalue.equals("")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please select state")
//and some more method calls
                    .show();
        } else if (cityvalue.equals("")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please select city")
//and some more method calls
                    .show();
        } else if (uaddress.equals("")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Complete Address")
//and some more method calls
                    .show();
        } else if (pincode.equals("")) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please enter Pincode")
//and some more method calls
                    .show();
        } else if (pincode.length() < 6) {
            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Pincode can't be less than 6 digits")
//and some more method calls
                    .show();
        }

//        else if(age<21)
//        {
//            String s1= String.valueOf(age);
//            Toast.makeText(this, ""+s1, Toast.LENGTH_SHORT).show();
//            new AlertDialog.Builder(UserInfo.this)
//                    //.setTitle("Record New Track")
//                    .setMessage("Age must be 21 years")
////and some more method calls
//                    .show();
//        }
        else {
            getvolley();
            // doSomething();
        }

    }


    private void getvolley() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.personalInfo(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String success = jsonObject.getString("success");

                            if (success.equals("0")) {
                                Toast.makeText(UserInfo.this, "Sorry, Session time out!!!", Toast.LENGTH_LONG).show();
                                session.logoutUser();
                                finish();
                            } else {
                                String code = jsonObject.getString("code");
                                //Toast.makeText(LoanRequest.this, ""+code, Toast.LENGTH_LONG).show();
                                // String message=jsonObject.getString("message");

                                if (code.equals("request_submitted")) {

                                    session.createpersonalinfo(uname, uemail, gendervalue, birthdate, ureligion, uqualification, umaritial, fname, sname,
                                            uaddress, statevalue, cityvalue, pincode, ulandmark);
                                    //    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragemtDasboard()).commit();
                                    Intent intent = new Intent(UserInfo.this, MainActivity.class);
                                    //intent.putExtra("EXTRA", "openFragment");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("fragmentNumber", 1);
                                    //intent.putExtra("childname",s);
                                    startActivity(intent);
                                    finish();
                                    //  Toast.makeText(LoanRequest.this, ""+message, Toast.LENGTH_LONG).show();
                                } else if (code.equals("not_submitted")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(UserInfo.this, "Sorry,Try Again !!!", Toast.LENGTH_LONG).show();
//                                startActivity(new Intent(LoanRequest.this,LoginActivity.class));
//                                finish();
                                } else if (code.equals("Not Available")) {
                                    progressDialog.dismiss();
                                    pinalert.setVisibility(View.VISIBLE);
                                    pinalert.setTextColor(Color.parseColor("#ff1a1a"));
                                    pinalert.setText("Sorry, Currently we are not providing our service at your area. ");
                                    animateView(pinalert);
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(UserInfo.this, "Sorry,Something went wrong !!!", Toast.LENGTH_SHORT).show();
                                }
                            }


                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            Toast.makeText(UserInfo.this, "Sorry,Try Again !!!", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UserInfo.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Authorizations", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FuZHJvaWRcLyIsImF1ZCI6Imh0dHA6XC9cL2xvY2FsaG9zdFwvYW5kcm9pZFwvIiwiaWF0IjoxNjQwMTQyMDg4LCJleHAiOjE2NDAxNDU2ODgsImRhdGEiOnsiYm9ycm93ZXJfaWQiOiIxMzA0MSJ9fQ.cYCdfE28XxwWkmBJki0BZfb7jHP2GLgaz7ZcxPm87JQ");
                params.put("uname", uname);
                params.put("uemail", uemail);
                params.put("ugender", gendervalue);
                params.put("dob", birthdate);
                params.put("ureligion", ureligion.trim());
                params.put("uqualify", uqualification);
                params.put("umstatus", umaritial);
                params.put("fname", fname);
                params.put("sname", sname);
                params.put("uaddress", uaddress);
                params.put("state", statevalue);
                params.put("city", cityvalue);
                params.put("pin", pincode);
                params.put("id", id);
                params.put("landmark", ulandmark);
                //   params.put("status","1");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(UserInfo.this);
        requestQueue.add(stringRequest);
        //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(UserInfo.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Submitting Information.Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @OnClick(R.id.linear)
    public void submit() {
        if (isNetworkAvailable(UserInfo.this)) {
            getdata();
        } else {

            final Snackbar snackbar = Snackbar
                    .make(relativeLayout, "No Internet Connection",
                            Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // snackbar.dismiss();
                            submit();
                        }
                    });
            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();

            ViewGroup.LayoutParams params = sbView.getLayoutParams();
            params.height = 100;
            sbView.setLayoutParams(params);

            TextView textView = (TextView) sbView.findViewById
                    (android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);


//            FrameLayout.LayoutParams parentParams = (FrameLayout.LayoutParams)
            sbView.getLayoutParams();


            snackbar.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        progressDialog.setCancelable(true);
//        progressDialog.dismiss();
        finish();
    }

    public void checkinfo() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";

        String urll = "https://onistech.in/capitaltrade/android/showpersonalinfo.php?id=" + id + "";
        ;
        // urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.showPersonalInfo(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        // Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            //get value of first index
                           // String status = jsonObject.getString("status");
                            String success = jsonObject.getString("success");

                            if (success.equals("0")) {
                                Toast.makeText(UserInfo.this, "Sorry, Session time out!!!", Toast.LENGTH_LONG).show();
                                session.logoutUser();
                                finish();
                            } else {
                                if (!success.equals("")) {
                                    String name = jsonObject.getString("name");
                                    String email = jsonObject.getString("email");
                                    String gender = jsonObject.getString("gender");
                                    dobvalue = jsonObject.getString("dob");
                                    religionvalue = jsonObject.getString("religion");
                                    qualifyvalue = jsonObject.getString("qualify");
                                    mstatusvalue = jsonObject.getString("mstatus");
                                    String fname = jsonObject.getString("fname");
                                    String sname = jsonObject.getString("sname");
                                    String address = jsonObject.getString("address");
                                    statevalue = jsonObject.getString("state");
                                    cityvalue = jsonObject.getString("city");
                                    String pcode = jsonObject.getString("pin");
                                    String lanmark = jsonObject.getString("landmark");
                                    //  Toast.makeText(UserInfo.this, ""+dobvalue, Toast.LENGTH_SHORT).show();
                                    //  Toast.makeText(UserInfo.this, ""+cityvalue, Toast.LENGTH_SHORT).show();
                                    // get(gender,dob,religion,qualify,mstatus,fname,sname,address);

                                    session.createpersonalinfo(name, email, gender, dobvalue, religionvalue, qualifyvalue, mstatusvalue,
                                            fname, sname, address, statevalue, cityvalue, pcode, lanmark);

                                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.religion));
                                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    religion.setAdapter(dataAdapter);

                                    if (religionvalue != "") {
                                        //  Toast.makeText(UserInfo.this, "got", Toast.LENGTH_SHORT).show();
                                        int spinnerPosition = dataAdapter.getPosition(religionvalue);
                                        religion.setSelection(spinnerPosition);
                                    } else if (religionvalue.equals("")) {
                                        //  Toast.makeText(UserInfo.this, ""+religionvalue, Toast.LENGTH_SHORT).show();
                                        // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
                                        // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        religion.setAdapter(dataAdapter);
                                    }


                                    ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.qualification));
                                    dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    qualification.setAdapter(dataAdapter1);


                                    if (qualifyvalue != "") {

                                        int spinnerPosition = dataAdapter1.getPosition(qualifyvalue);
                                        qualification.setSelection(spinnerPosition);
                                    } else if (qualifyvalue.equals("")) {
                                        // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
                                        // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        qualification.setAdapter(dataAdapter);
                                    }


                                    ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.martial));
                                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    mstatus.setAdapter(dataAdapter2);

                                    if (mstatusvalue != "") {

                                        int spinnerPosition = dataAdapter2.getPosition(mstatusvalue);
                                        mstatus.setSelection(spinnerPosition);
                                    } else if (mstatusvalue.equals("")) {
                                        // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
                                        // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        mstatus.setAdapter(dataAdapter2);
                                    }
                                    get(name, email, gender, dobvalue, qualifyvalue, mstatusvalue, fname, sname, address, pcode, lanmark);

                                } else {

                                    setstaticadapters();

                                }
                            }
//
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            Toast.makeText(UserInfo.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UserInfo.this, "Server encountered problem !!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                // params.put("Authorizations", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FuZHJvaWRcLyIsImF1ZCI6Imh0dHA6XC9cL2xvY2FsaG9zdFwvYW5kcm9pZFwvIiwiaWF0IjoxNjQwMTQyMDg4LCJleHAiOjE2NDAxNDU2ODgsImRhdGEiOnsiYm9ycm93ZXJfaWQiOiIxMzA0MSJ9fQ.cYCdfE28XxwWkmBJki0BZfb7jHP2GLgaz7ZcxPm87JQ");
                params.put("uname", uname);
                return params;

            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(UserInfo.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(UserInfo.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    private void CalcAge() {

        // c = Calendar.getInstance();
        //c.set(1995,10,25);
        Calendar today = Calendar.getInstance();

        // today.set(2019, 01, 01);

        age = today.get(Calendar.YEAR) - c.get(Calendar.YEAR);
        int months = c.get(Calendar.YEAR);
        String aging = String.valueOf(months);
        //  Toast.makeText(this, ""+aging, Toast.LENGTH_SHORT).show();
        if (today.get(Calendar.DAY_OF_YEAR) < c.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        if (age < 21) {

            new AlertDialog.Builder(UserInfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Age must be 21 years to apply Loan")
//and some more method calls
                    .show();
            dob.setText("");
        } else {
            dob.setText(date[0]);
        }


    }

    private void mc() {

        JsonArrayRequest req = new JsonArrayRequest(UrlUtils.fetchState(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    ArrayList<String> al1 = new ArrayList<String>();
                    ArrayList<String> al2 = new ArrayList<String>();

                    for (int i = 0; i < response.length(); i++) {
                        JSONObject person = (JSONObject) response.get(i);
                        stateidint = person.getInt("catid");
                        al2.add(String.valueOf(stateidint));

                        state = person.getString("catname");
                        al1.add(state);

                        if (state.equals("All"))
                            al1.remove(state);
                       /* if(state.equals("Others"))
                            al1.remove(state);*/
                    }
                    chal(al1);
                    bkl(al1, al2);

                } catch (JSONException e) {


                    Toast.makeText(UserInfo.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UserInfo.this, "Failed to load States !!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };
        req.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(UserInfo.this);
        requestQueue.add(req);
    }


    public void chal(ArrayList<String> al) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinnertext, al);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s1.setAdapter(dataAdapter);
        if (statevalue != "") {
            int spinnerPosition = dataAdapter.getPosition(statevalue);
            s1.setSelection(spinnerPosition);
        } else if (statevalue.equals("")) {
            // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
            // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            s1.setAdapter(dataAdapter);
        }
    }


    public void bkl(ArrayList<String> al1, ArrayList<String> al2) {
        for (int i = 0; i < al1.size(); i++)
            states.put(al1.get(i), al2.get(i));
    }

    private void mc1() {
        String url1 = "https://onistech.in/capitaltrade/android/city.php?stateid=" + idz + "";
        JsonArrayRequest req = new JsonArrayRequest(UrlUtils.fetchCity(idz), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                try {
                    progressDialog.dismiss();
                    ArrayList<String> al1 = new ArrayList<String>();
                    ArrayList<String> al2 = new ArrayList<String>();

                    for (int i = 0; i < response.length(); i++) {
                        JSONObject person = (JSONObject) response.get(i);
                        cityidint = person.getInt("cityid");
                        al2.add(String.valueOf(cityidint));

                        city = person.getString("cityname");
                        al1.add(city);

                        if (city.equals("All"))
                            al1.remove(city);
                       /* if(state.equals("Others"))
                            al1.remove(state);*/
                    }
                    chal1(al1);
                    bkl1(al1, al2);

                } catch (JSONException e) {

                    progressDialog.dismiss();
                    Toast.makeText(UserInfo.this, "Sorry,Server encountered problem!!!", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UserInfo.this, "Failed to load Districts!!!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };

        req.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(UserInfo.this);
        requestQueue.add(req);
        progressDialog = new ProgressDialog(UserInfo.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Fetching city...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    public void chal1(ArrayList<String> al) {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinnertext, al);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s2.setAdapter(dataAdapter);
        if (cityvalue != "") {
            int spinnerPosition = dataAdapter.getPosition(cityvalue);
            s2.setSelection(spinnerPosition);
        } else if (cityvalue.equals("")) {
            // ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, al);
            // dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            s2.setAdapter(dataAdapter);
        }
    }

    public void bkl1(ArrayList<String> al1, ArrayList<String> al2) {
        for (int i = 0; i < al1.size(); i++)
            cities.put(al1.get(i), al2.get(i));
    }

    public String getgender() {
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) findViewById(checkedId);
                gendervalue = rb.getText().toString().trim();
                // Log.e("check",ugender);

            }
        });

        return gendervalue;
    }

    public void animateView(View view) {
        Animation shake = AnimationUtils.loadAnimation(UserInfo.this, R.anim.shake);
        view.startAnimation(shake);
    }

}
