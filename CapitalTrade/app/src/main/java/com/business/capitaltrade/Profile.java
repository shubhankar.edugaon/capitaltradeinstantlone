package com.business.capitaltrade;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment {
    TextView name, email, code, mobile, fname, address, dob;
    SessionManager session;
    RequestQueue mRequest;
    String id = "";
    Button backpress;
    ProgressDialog progressDialog;
    ImageView uimage, editprofile;

    public Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("My Profile");
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        name = (TextView) v.findViewById(R.id.name);
        email = (TextView) v.findViewById(R.id.email);
        code = (TextView) v.findViewById(R.id.code);
        mobile = (TextView) v.findViewById(R.id.mobile);
        fname = (TextView) v.findViewById(R.id.fname);
        address = (TextView) v.findViewById(R.id.address);
        backpress = (Button) v.findViewById(R.id.backpress);
        uimage = (ImageView) v.findViewById(R.id.uimage);
        editprofile = (ImageView) v.findViewById(R.id.editprofile);
        dob = (TextView) v.findViewById(R.id.dob);
        session = new SessionManager(getContext());
        mRequest = Volley.newRequestQueue(getContext().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        id = user.get(SessionManager.KEY_ID);

        getvolley();

        backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("fragmentNumber", 1);
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
            }
        });

        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //  Log.i(tag, "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {

                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("fragmentNumber", 1);
                    startActivity(i);
                    ((Activity) getActivity()).overridePendingTransition(0, 0);
//                    FragemtDasboard e1 = new FragemtDasboard();
//                    FragmentTransaction manager = getFragmentManager().beginTransaction();
//                    manager.replace(R.id.fragment_container, e1);
//                    // manager.addToBackStack(null);
//                    manager.commit();

                    return true;
                }
                return false;
            }
        });

        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), UserInfo.class);
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
            }
        });
        return v;
    }
    private void getvolley() {
        String url = "https://onistech.in/capitaltrade/android/userprofile.php";
        // String url = "http://bollywoodcity.in/event/index.php?start_date="+date[0]+"&end_date="+date1[0]+"&start_time="+time[0]+"&end_time="+time1[0]+"";
        //String url = "http://bollywoodcity.in/event/index.php?date="+date[0]+"&date2="+date1[0]+"&start_time="+time[0]+"&end_time="+time1[0]+"";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.userProfie(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    // ar.clear();
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String success = jsonObject.getString("success");
                        if (success.equals("0")){
                            session.logoutUser();
                            Toast.makeText(getContext().getApplicationContext(), "Session time out !!!", Toast.LENGTH_SHORT).show();
                        }
                            String ucode = jsonObject.getString("code");
                            String udob = jsonObject.getString("dob");
                            String uemail = jsonObject.getString("email");
                            String uname = jsonObject.getString("name");
                            String uaddress = jsonObject.getString("address");
                            String ufname = jsonObject.getString("fname");
                            String umobile = jsonObject.getString("mobile");
                            String image = jsonObject.getString("image");
                            code.setText(ucode);
                            name.setText(uname);
                            dob.setText(udob);
                            email.setText(uemail);
                            address.setText(uaddress);
                            fname.setText(ufname);
                            mobile.setText(umobile);
                            if (uaddress.equals(",,-")) {
                                address.setText("");
                            } else {
                                address.setText(uaddress);
                            }
                            if (image.equals("https://onistech.in/capitaltrade/android/uploads/")) {
                                Glide.with(getContext()).load(R.drawable.userproimg).into(uimage);
                            } else {
                                Glide.with(getContext().getApplicationContext())
                                        .load(image)
                                        //  .apply(new RequestOptions().override(1200, 200))
                                        .apply(new RequestOptions().disallowHardwareConfig()
                                                .override(Target.SIZE_ORIGINAL)
                                                // .dontTransform()
                                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                .skipMemoryCache(true)
                                                .dontAnimate()
                                                .placeholder(R.drawable.loading)
                                                //.fitCenter()
                                                .format(DecodeFormat.PREFER_ARGB_8888))
                                        .into(uimage);
                            }


//                        roi = jsonObject.getString("roi");
//                        loanno = jsonObject.getString("loanno");
//                        ddate = jsonObject.getString("ddate");
//
                        //   Toast.makeText(getContext(), ""+code, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+borrreqloanid, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+borrid, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+pname, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+lamnt, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+tenure, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getContext(), ""+roi, Toast.LENGTH_SHORT).show();


                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();

                    Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "Sorry ,Something went Wrong Or No Connection !! ", Toast.LENGTH_SHORT).show();
                // progressDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();

                params.put("id", id);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Fetching Details...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

}
