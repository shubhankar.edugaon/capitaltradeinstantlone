package com.business.capitaltrade;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class LoginOtp extends AppCompatActivity {
    ProgressDialog progressDialog;
    static EditText e1;
    Button b1, b2;
    TextView t1, t2, t3;
    LinearLayout l1, l2;
    SessionManager session;
    String serverurl = "https://onistech.in/capitaltrade/android/loginotp.php";
    String otpurl = "https://onistech.in/capitaltrade/android/otpApi.php";
    String take = "", change = "", s = "", otpValue = "";
    String getno = "", login = "";
    CountDownTimer cTimer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_otp);

        e1 = (EditText) findViewById(R.id.etlogin);

        t1 = (TextView) findViewById(R.id.show);
        t2 = (TextView) findViewById(R.id.resend);


        session = new SessionManager(getApplicationContext());
        getno = getIntent().getStringExtra("otp");
        again();
    }


    public void again() {
        s = "+91" + getno;
        //  otpurl = otpurl.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.otpApi(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                            String code = jsonObject.getString("code");
                            if (code.equals("sent")) {
                               // otpValue = jsonObject.getString("value");
                                Toast.makeText(LoginOtp.this, "OTP send Successfully", Toast.LENGTH_SHORT).show();
                                t1.setText("We've sent an OTP to the mobile number " + s + ". Please enter it below to complete verification.");
                                startTimer();
                            } else {
                                Toast.makeText(LoginOtp.this, "Sorry Server encountered problem!!!", Toast.LENGTH_SHORT).show();
                            }
                            //  Toast.makeText(signup.this, ""+status, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            Toast.makeText(LoginOtp.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
//
                Toast.makeText(getApplicationContext(), "Something went wrong !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("u_mobile", s);
                params.put("login", "1");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
//            //startActivity(new Intent(getContext().getApplicationContext(),MainActivity.class));
        progressDialog = new ProgressDialog(LoginOtp.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Sending OTP..");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void resend(View view) {
        again();
    }


    public void continueotp(View view) {
        login = e1.getText().toString().trim();
        if (login.equals("")) {
            Toast.makeText(this, "Please enter OTP to continue", Toast.LENGTH_SHORT).show();
//        }
//        else if (otpValue.equals(login)) {
//            getvolley();
//            //Toast.makeText(this, "Dashboard", Toast.LENGTH_SHORT).show();
//            // startActivity(new Intent(LoginOtp.this, MainActivity.class));
//            // finish();
        } else{
            getvolley();
        }

    }

    public void otp(View view) {
        startActivity(new Intent(LoginOtp.this, LoginActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(LoginOtp.this, LoginActivity.class));
        finish();
    }

    private void getvolley() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.loginOtp(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);  //get value of first index
                    String code = jsonObject.getString("token");
                    AppSession.logedInToken = code;
                    //String message = jsonObject.getString("code");
                    // String check = jsonObject.getString("check");//we should write key name

                    //Toast.makeText(SignIn.this, ""+check, Toast.LENGTH_SHORT).show();
                    if (code.equals("login_failed")) {
                        progressDialog.dismiss();

                        Toast.makeText(LoginOtp.this, "" + code, Toast.LENGTH_SHORT).show();
                    } else if (!code.equals("") )
                    //  else if(code.equals("login_success"))
                    {
                        progressDialog.dismiss();

                        AppSession.logedInToken = jsonObject.getString("token");

                        //   String name= jsonObject.getString("name");
                        // String email= jsonObject.getString("email");
                        String id = jsonObject.getString("id");
                        String mobile = jsonObject.getString("mobile");
                        String bcode = jsonObject.getString("bcode");
                        session.createLoginSession(id, mobile, bcode);
                        // Toast.makeText(LoginOtp.this, "Welcome "+name, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(LoginOtp.this, MainActivity.class));
                        finish();

                    }


                } catch (JSONException e) {
                    Log.d("error", "error log " + e.getMessage());
                    progressDialog.dismiss();
                    Toast.makeText(LoginOtp.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginOtp.this, "Sorry ,Something went Wrong Or No Connection !! ", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                    params.put("u_mobile", getno);
                    params.put("u_otp", login);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoginOtp.this);
        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(LoginOtp.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Authenticating....");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void recivedSms(String message) {
        try {
            //  String str="sdfvsdf68fsdfsf8999fsdf09";
            String numberOnly = message.replaceAll("[^0-9]", "");
            e1.setText(numberOnly);
        } catch (Exception e) {
        }
    }

    void startTimer() {
        cTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                // t2.setText("seconds remaining: " + millisUntilFinished / 1000);
                long millis = millisUntilFinished;
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                t2.setText(hms.substring(3, 8));//set text
                //    String abc = hms.replaceAll();
                t2.setClickable(false);
            }

            public void onFinish() {
                t2.setText("Resend OTP");
                t2.setClickable(true);
            }
        };
        cTimer.start();
    }

}
