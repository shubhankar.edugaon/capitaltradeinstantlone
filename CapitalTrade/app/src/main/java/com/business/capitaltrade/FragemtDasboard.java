package com.business.capitaltrade;


import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragemtDasboard extends BaseFragment {
    ProgressDialog progressDialog;
    Unbinder unbinder;
    public LinearLayout note;
    private TransparentProgressDialog pd;
    private Handler h;
    private Runnable r;
    @BindView(R.id.card_view)
    CardView lblTitle;
    @BindView(R.id.card_view1)
    CardView card2;
    @BindView(R.id.card_view6)
    CardView card3;
    @BindView(R.id.card_view2)
    CardView cominfo;
    @BindView(R.id.card_view5)
    CardView card4;
    @BindView(R.id.card_view7)
    CardView card7;
    @BindView(R.id.card_view3)
    CardView emicalc;
    @BindView(R.id.card_view8)
    CardView bank;
    ProgressBar progressBar, progressBar1, progressBar2, progressBar8;
    SessionManager session;
    String sesid = "";
    // @BindView(R.id.t1)
    TextView t1, t2, t3, circle, textnotice, t8;
    // @BindView(R.id.t2)
    ScrollView sc;
    // @BindView(R.id.t3)
    ImageView bell;
    String perinfo = "", compinfo = "", kyc = "", mob = "", code = "", premark = "", aremark = "", picremark = "", bankinfo = "";


    public FragemtDasboard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragemt_dasboard, container, false);
        // bind view using butter knife
        getActivity().setTitle("Dashboard");

        Log.d("token", "session token" + AppSession.logedInToken);

        unbinder = ButterKnife.bind(this, v);
        t1 = (TextView) v.findViewById(R.id.t1);
        t2 = (TextView) v.findViewById(R.id.t2);
        t3 = (TextView) v.findViewById(R.id.t3);
        t8 = (TextView) v.findViewById(R.id.t8);
        circle = (TextView) v.findViewById(R.id.circle);
        textnotice = (TextView) v.findViewById(R.id.textnotice);
        note = (LinearLayout) v.findViewById(R.id.notice);
        //  notice=(ImageView) v.findViewById(R.id.notic);
//        sc=(TextView)v.findViewById(R.id.sc);
        progressBar = (ProgressBar) v.findViewById(R.id.PROGRESS_BAR);
        progressBar1 = (ProgressBar) v.findViewById(R.id.PROGRESS_BAR1);
        progressBar2 = (ProgressBar) v.findViewById(R.id.PROGRESS_BAR2);
        progressBar8 = (ProgressBar) v.findViewById(R.id.PROGRESS_BAR8);
        bell = (ImageView) v.findViewById(R.id.notic);


//        notice.startAnimation(zoomout);
//        notice.startAnimation(zoomin);

//        final AnimatorSet mAnimatorSet = new AnimatorSet();
//        mAnimatorSet.playTogether(
//                ObjectAnimator.ofFloat(bell,"scaleX",1,0.9f,0.9f,1.1f,1.1f,1),
//                ObjectAnimator.ofFloat(bell,"scaleY",1,0.9f,0.9f,1.1f,1.1f,1),
//                ObjectAnimator.ofFloat(bell,"rotation",0 ,-3 , -3, 3, -3, 3, -3,3,-3,3,-3,0)
//        );
//
////define any animation you want,like above
//
//        mAnimatorSet.setDuration(2000); //set duration for animations
//        mAnimatorSet.start();

//        Glide.with(getContext())
//                .load(R.drawable.bellgif)
//                .into(bell);
        //headerView.findViewById(R.id.name);
        session = new SessionManager(getContext());
        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */
        session.checkLogin();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        //    name = user.get(SessionManager.KEY_NAME);

        // email
        sesid = user.get(SessionManager.KEY_ID);
        mob = user.get(SessionManager.KEY_MOBILE);
        code = user.get(SessionManager.KEY_CODE);

        if (isNetworkAvailable(getActivity())) {

        } else {
            alertdialog(getActivity());
        }

        bell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // MyActivity.setContentView(R.layout.newlayout);
                // Toast.makeText(getContext(), "bell", Toast.LENGTH_SHORT).show();
                final AlertDialog.Builder imageDialog = new AlertDialog.Builder(getContext(), R.style.DialogTheme2);
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                View customView = inflater.inflate(R.layout.chatlayout, null);
                imageDialog.setView(customView);
                TextView pan = (TextView) customView.findViewById(R.id.pan);
                TextView adhar = (TextView) customView.findViewById(R.id.adhar);
                TextView pic = (TextView) customView.findViewById(R.id.pic);
                TextView pancard = (TextView) customView.findViewById(R.id.pancard);
                TextView adharcard = (TextView) customView.findViewById(R.id.adharcard);
                TextView photo = (TextView) customView.findViewById(R.id.photo);

                if (premark.equals("")) {
                    pan.setVisibility(View.GONE);
                    pancard.setVisibility(View.GONE);
                    adharcard.setText(aremark);
                    photo.setText(picremark);
                }
                if (aremark.equals("")) {
                    adhar.setVisibility(View.GONE);
                    adharcard.setVisibility(View.GONE);
                    pancard.setText(premark);
                    photo.setText(picremark);
                }
                if (picremark.equals("")) {
                    pic.setVisibility(View.GONE);
                    photo.setVisibility(View.GONE);
                    adharcard.setText(aremark);
                    pancard.setText(premark);
                }
                if (!premark.equals("") && !aremark.equals("") && !picremark.equals("")) {
                    pancard.setText(premark);
                    adharcard.setText(aremark);
                    photo.setText(picremark);
                }
                alertDialogAndroid = imageDialog.create();
                alertDialogAndroid.show();
            }
        });

//        Toast.makeText(getContext(), ""+sesid, Toast.LENGTH_SHORT).show();
//        Toast.makeText(getContext(), ""+mob, Toast.LENGTH_SHORT).show();
//        Toast.makeText(getContext(), ""+code, Toast.LENGTH_SHORT).show();
        //  String arrcolor[]={"#FF4081","#ffff8800","#ffaa66cc","#757575"};

        // h = new Handler();

//        r =new Runnable() {
//            @Override
//            public void run() {
//                if (pd.isShowing()) {
//                    pd.dismiss();
//                }
//            }
//        };
        //Toast.makeText(getContext(), "hello", Toast.LENGTH_SHORT).show();
//        Snackbar snackbar = Snackbar
//                .make(getActivity().findViewById(android.R.id.content), "www.journaldev.com", Snackbar.LENGTH_LONG);
//        snackbar.show();
        return v;
    }

    @OnClick(R.id.card_view)
    public void onButtonClick(View view) {

        if (isNetworkAvailable(getActivity())) {
            requestStoragePermission();
        } else {
            alertdialog(getActivity());
        }

    }

    @OnClick(R.id.card_view1)
    public void card2(View view) {
        //   progressBar.setProgress(50);
        // Toast.makeText(getContext(), "Work is under Progress", Toast.LENGTH_SHORT).show();
        if (isNetworkAvailable(getActivity())) {
            Intent i = new Intent(getActivity(), UserInfo.class);
            startActivity(i);
            ((Activity) getActivity()).overridePendingTransition(0, 0);
        } else {

            alertdialog(getActivity());
        }
    }

    @OnClick(R.id.card_view6)
    public void card3(View view) {

        if (isNetworkAvailable(getActivity())) {
            if (kyc.equals("3") && perinfo.equals("1") && compinfo.equals("1") && bankinfo.equals("1")) {

                Intent i = new Intent(getActivity(), LoanRequest.class);
                startActivity(i);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
            } else {


                final Dialog dialog = new Dialog(getActivity());
                dialog.setCancelable(true);

                //  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                View view1 = getActivity().getLayoutInflater().inflate(R.layout.srydialog, null);
                dialog.setContentView(view1);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                ImageView sry = (ImageView) view1.findViewById(R.id.sry);
                ImageView i1 = (ImageView) view1.findViewById(R.id.text1);
                ImageView i2 = (ImageView) view1.findViewById(R.id.kdoc);
                ImageView i3 = (ImageView) view1.findViewById(R.id.cdoc);
                ImageView i4 = (ImageView) view1.findViewById(R.id.bankdoc);
                TextView ok = (TextView) view1.findViewById(R.id.ok);
                Glide.with(getContext())
                        .load(R.drawable.sry)
                        .into(sry);
// =========================== For KYC ===========================================================//
                if (kyc.equals("0") || kyc.equals("1") || kyc.equals("2")) {
                    Drawable res = getResources().getDrawable(R.drawable.ic_wrong);
                    i1.setImageDrawable(res);
                } else if (kyc.equals("3")) {
                    Drawable res = getResources().getDrawable(R.drawable.ic_tickright);
                    i1.setImageDrawable(res);
                } else {
                    Drawable res = getResources().getDrawable(R.drawable.ic_wrong);
                    i1.setImageDrawable(res);
                }

                //============================For Personal Info ==============================================//
                if (perinfo.equals("1")) {
                    Drawable res = getResources().getDrawable(R.drawable.ic_tickright);
                    i2.setImageDrawable(res);
                } else {
                    Drawable res = getResources().getDrawable(R.drawable.ic_wrong);
                    i2.setImageDrawable(res);
                }

                //=====================================For Company Info===========================================//
                if (compinfo.equals("1")) {
                    Drawable res = getResources().getDrawable(R.drawable.ic_tickright);
                    i3.setImageDrawable(res);
                } else {
                    Drawable res = getResources().getDrawable(R.drawable.ic_wrong);
                    i3.setImageDrawable(res);
                }

                //=====================================For Bank Info===========================================//
                if (bankinfo.equals("1")) {
                    Drawable res = getResources().getDrawable(R.drawable.ic_tickright);
                    i4.setImageDrawable(res);
                } else {
                    Drawable res = getResources().getDrawable(R.drawable.ic_wrong);
                    i4.setImageDrawable(res);
                }

                //---------------------------------------------------------------------------------------------------//
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        } else {
            alertdialog(getActivity());
        }

    }


    @OnClick(R.id.card_view3)
    public void emi(View view) {
        //  Toast.makeText(getContext(), "Work is under Progress", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(getActivity(), EmiCalc.class);
        startActivity(i);
        ((Activity) getActivity()).overridePendingTransition(0, 0);
    }

    @OnClick(R.id.card_view8)
    public void bank(View view) {
        if (isNetworkAvailable(getActivity())) {
            Intent i = new Intent(getActivity(), BankDetails.class);
            startActivity(i);
            ((Activity) getActivity()).overridePendingTransition(0, 0);
        } else {
            alertdialog(getActivity());
        }
        //  Toast.makeText(getContext(), "Work is under Progress", Toast.LENGTH_SHORT).show();

    }

    @OnClick(R.id.card_view5)
    public void card4(View view) {
        if (isNetworkAvailable(getActivity())) {
            //   progressBar.setProgress(50);
            Intent i = new Intent(getActivity(), Proposal.class);
            startActivity(i);
            ((Activity) getActivity()).overridePendingTransition(0, 0);
        } else {
            alertdialog(getActivity());
        }
    }

    @OnClick(R.id.card_view7)
    public void card7(View view) {
        //Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();

    }

    @OnClick(R.id.card_view2)
    public void cominfo(View view) {
        //   progressBar.setProgress(50);
        if (isNetworkAvailable(getActivity())) {
            Intent i = new Intent(getActivity(), Companyinfo.class);
            startActivity(i);
            ((Activity) getActivity()).overridePendingTransition(0, 0);
        } else {
            alertdialog(getActivity());
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // unbind the view to free some memory
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        //  if(isNetworkAvailable(getActivity())) {
        CheckProgressValues();
        //  }
//        else {
//            alertdialog(getActivity());
//        }
        //Toast.makeText(getContext(), "hi everyone!!", Toast.LENGTH_SHORT).show();

    }

    public void CheckProgressValues() {
        String serverurl = "https://onistech.in/capitaltrade/android/checkdoc.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.checkDoc(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //progressDialog.dismiss();
                        pd.dismiss();
                        try {
                            //progressDialog.dismiss();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String success = jsonObject.getString("success");
                            if (success.equals("0")){
                                Toast.makeText(getContext(),"Sorry Session time out try to login !!",Toast.LENGTH_SHORT).show();
                            }
                            kyc = jsonObject.getString("kyc").trim();
                            perinfo = jsonObject.getString("perinfo").trim();
                            compinfo = jsonObject.getString("compinfo").trim();
                            premark = jsonObject.getString("premark").trim();
                            aremark = jsonObject.getString("aremark").trim();
                            picremark = jsonObject.getString("picremark").trim();
                            bankinfo = jsonObject.getString("bankinfo").trim();
//                            Toast.makeText(getContext(), "Pan Remark"+premark, Toast.LENGTH_SHORT).show();
//                            Toast.makeText(getContext(), "Adhar Remark"+aremark, Toast.LENGTH_SHORT).show();
//                            Toast.makeText(getContext(), "photo Remark"+picremark, Toast.LENGTH_SHORT).show();
//===================================================================================================================//
                            card3.setClickable(true);
                            card3.setEnabled(true);
//===============================Value set for Progress for KYC Documents ===================================================//
                            if (kyc.equals("0")) {
                                progressBar.setProgress(0);
                                t1.setText("0 %");
                            } else if (kyc.equals("1")) {
                                progressBar.setProgress(35);
                                t1.setText("35 %");
                            } else if (kyc.equals("2")) {
                                progressBar.setProgress(60);
                                t1.setText("60 %");
                            } else if (kyc.equals("3")) {
                                progressBar.setProgress(100);
                                t1.setText("100 %");
                            } else {
                                Toast.makeText(getContext(), "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                            }
//===============================Value set for Progress for Personal Ino ===================================================//
                            if (perinfo.equals("1")) {
                                progressBar1.setProgress(100);
                                t2.setText("100 %");
                            } else {
                                progressBar1.setProgress(0);
                                t2.setText("0 %");
                            }
//===============================Value set for Progress for Company Info ===================================================//
                            if (compinfo.equals("1")) {
                                progressBar2.setProgress(100);
                                t3.setText("100 %");
                            } else {
                                progressBar2.setProgress(0);
                                t3.setText("0 %");
                            }
//===============================Value set for Progress for Bank Details Info ===================================================//
                            if (bankinfo.equals("1")) {
                                progressBar8.setProgress(100);
                                t8.setText("100 %");
                            } else {
                                progressBar8.setProgress(0);
                                t8.setText("0 %");
                            }

                            //================================Visibilty showing header in dashboard ==========================================//
                            if (kyc.equals("3") && perinfo.equals("1") && compinfo.equals("1") && bankinfo.equals("1")) {
                                textnotice.setVisibility(View.GONE);
                            } else {
                                textnotice.setVisibility(View.VISIBLE);
                            }

//====================================Value set for Bell Notification=================================================//
                            if (!premark.equals("") || !aremark.equals("") || !picremark.equals("")) {
                                textnotice.setVisibility(View.VISIBLE);
                                textnotice.setText("Dear Borrower, You have notification. Kindly please check by clicking on bell icon showing on right-top of the KYC Documents tab.");
                                textnotice.setBackgroundColor(Color.parseColor("#ff99cc00"));
                                circle.setVisibility(View.VISIBLE);
                                bell.setVisibility(View.VISIBLE);
                                Animation animation;
                                animation = AnimationUtils.loadAnimation(getContext(),
                                        R.anim.pendulam);
                                bell.startAnimation(animation);
                            } else {
                                circle.setVisibility(View.GONE);
                                bell.clearAnimation();
                                bell.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                                e.getMessage();
                            pd.dismiss();

                           // Toast.makeText(getContext(), "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();

                pd.dismiss();
                card3.setClickable(false);
                card3.setEnabled(false);

                Toast.makeText(getContext(), "Sorry, No Internet connection!!!", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Authorizations", "Bearer " + AppSession.logedInToken);

                params.put("u_id", sesid);
                //   params.put("u_pass",pass);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest);

        pd = new TransparentProgressDialog(getContext(), R.drawable.loadnew);
        pd.show();

    }

    private void requestStoragePermission() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            //  Toast.makeText(getContext().getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(getActivity(), PersonalInfo.class);
                            i.putExtra("premark", premark);
                            i.putExtra("aremark", aremark);
                            i.putExtra("picremark", picremark);
                            startActivity(i);
                            ((Activity) getActivity()).overridePendingTransition(0, 0);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings

                            Toast.makeText(getContext(), "Permission denied !!!", Toast.LENGTH_SHORT).show();
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getContext().getApplicationContext(), "Please give required permission from Phone Setting", Toast.LENGTH_LONG).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

}
