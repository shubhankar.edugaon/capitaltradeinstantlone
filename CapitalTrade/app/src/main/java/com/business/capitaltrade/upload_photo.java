package com.business.capitaltrade;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;


/**
 * A simple {@link Fragment} subclass.
 */
public class upload_photo extends BaseFragment {
    final private int IMG=2;
    final String date[] = new String[1];
    final private int IMG_REQUEST=1;
    Uri imageUri;
    Bitmap bitmap;
    SessionManager session;
    String mob="",id="";
    ProgressDialog progressDialog;
ImageView photoclick,profile;
LinearLayout l1;
    public upload_photo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_upload_photo, container, false);
        photoclick=(ImageView)v.findViewById(R.id.photoclick);
        profile=(ImageView)v.findViewById(R.id.profile);
        l1=(LinearLayout)v.findViewById(R.id.ll);
        session = new SessionManager(getContext().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        id = user.get(SessionManager.KEY_ID);
        mob = user.get(SessionManager.KEY_MOBILE);
        photoclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage2();
            }
        });

        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable(getActivity())) {
                    String backgroundImageName = String.valueOf(profile.getTag());
                    if(backgroundImageName.equals("userphoto"))
                    {
                        Toast.makeText(getContext(), "Please Select/Capture User Image", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        uploadphoto();
                    }

                }
                else
                {
                    Toast.makeText(getContext(), "Please Check Internet Connection !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        chechdatastatus1();


        return v;
    }

    private void selectImage2() {


        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};


        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {


                    selectimage();
                  /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

                    startActivityForResult(intent, 1);*/

                } else if (options[item].equals("Choose from Gallery"))

                {

                    Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);

                    //if everything is ok we will open image chooser
                   /* Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.putExtra("crop","true");
                    i.putExtra("aspectX",1);
                    i.putExtra("aspectY",1);
                    i.putExtra("outputX",200);
                    i.putExtra("outputY",200);
                    i.putExtra("return-data",true);
                 //   i.putExtra("crop","true");
                    startActivityForResult(i, 2);*/


                } /*else if (options[item].equals("Cancel")) {

                    dialog.dismiss();

                }*/

            }

        }).setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TOdo
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void selectimage(){
        //  Intent intent = new Intent();
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //  intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(i,IMG_REQUEST);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==IMG_REQUEST && resultCode ==RESULT_OK && data !=null){
            Uri path = data.getData();
            //  bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
            bitmap = (Bitmap) data.getExtras().get("data");
            profile.setImageBitmap(bitmap);
            profile.setTag("userimage");
        }

        else if(requestCode==IMG && resultCode ==RESULT_OK && data !=null)
        {
            /* Bundle extras  = data.getExtras();
            bitmap =extras.getParcelable("data");
            iv1.setImageBitmap(bitmap);*/
            //getting the image Uri
            imageUri = data.getData();

            // ImageCropFunction();
            try {
                //getting bitmap object from uri
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageUri);

                //displaying selected image to imageview
                //   image.setImageBitmap(bitmap);
                profile.setImageURI(imageUri);
                profile.setTag("userimage");
                //calling the method uploadBitmap to upload image
                // uploadBitmap(bitmap);
            } catch (IOException e) {

            }
        }

        else
        {

        }
    }

    private String imagetostring(Bitmap bitmap)
    {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BitmapFactory.Options options=new BitmapFactory.Options();
        options.inSampleSize=1;
        final float scale=getResources().getDisplayMetrics().density;

        // Display dis=getWindowManager().getDefaultDisplay();
        // int width=dis.getWidth();
        bitmap=Bitmap.createScaledBitmap(bitmap,600,550, true);
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[] imbytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imbytes, Base64.DEFAULT);

    }

    private void uploadphoto() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";
        String urll="https://onistech.in/capitaltrade/android/photoinfo.php";
        ;
      //  urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.uploadPhoto(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        // Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        try {
                            // ar.clear();
                            //for (int i = 0; i < response.length(); i++) {
                                JSONArray jsonArray = new JSONArray(response);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String success = jsonObject.getString("success");
                           // String status = jsonObject.getString("status");


                            if (success.equals(0)) {
                                session.logoutUser();
                                Toast.makeText(getActivity(), "Sorry, session time out!!!", Toast.LENGTH_LONG).show();

                            }
                            else {
                                String code = jsonObject.getString("code");
                                profile.setTag(null);
                                if (code.equals("updated")) {
                                    Toast.makeText(getContext(), "Data uploaded Sucessfully", Toast.LENGTH_SHORT).show();
                                    String imag = jsonObject.getString("image");
                                    //   Toast.makeText(getContext().getApplicationContext(), ""+imag, Toast.LENGTH_SHORT).show();
                                    l1.setClickable(false);
                                    l1.setBackgroundColor(Color.parseColor("#BDBDBD"));
                                    photoclick.setVisibility(GONE);
                                    Glide.with(getContext().getApplicationContext())
                                            .load(imag)
                                            //  .apply(new RequestOptions().override(1200, 200))
                                            .apply(new RequestOptions().disallowHardwareConfig()
                                                    .override(Target.SIZE_ORIGINAL)
                                                    .dontTransform()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true)
                                                    .dontAnimate()
                                                    .placeholder(R.drawable.loading)
                                                    .format(DecodeFormat.PREFER_ARGB_8888))
                                            .into(profile);
                                    // getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragemtDasboard()).commit();
                                    getActivity().onBackPressed();

                                } else {

                                }
                            }


                                //  Toast.makeText(getContext().getApplicationContext(), ""+id, Toast.LENGTH_SHORT).show();
                           // }
                        } catch (JSONException e) {
                            progressDialog.dismiss();

                            // Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext().getApplicationContext(), "Server has encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id",id);
                params.put("u_image",imagetostring(bitmap));
                params.put("status","1");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };



        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void chechdatastatus1() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";

        String urll="https://onistech.in/capitaltrade/android/showphoto.php?id="+id+"";
        ;
        // urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.showPhoto(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        // Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        try {
                            // ar.clear();
                          //  for (int i = 0; i < response.length(); i++) {
                                JSONArray jsonArray = new JSONArray(response);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                //get value of first index
                            String success = jsonObject.getString("success");
                            //String status1 = jsonObject.getString("status");


                            if (success.equals("0")) {
                                session.logoutUser();
                                Toast.makeText(getActivity(), "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                            }
                            else {
                               // String status = jsonObject.getString("image");
                                if (!success.equals("")) {
                                    profile.setTag(null);
                                    String imag = jsonObject.getString("image");
                                    photoclick.setVisibility(GONE);
                                    l1.setClickable(false);
                                    l1.setBackgroundColor(Color.parseColor("#BDBDBD"));
                                    Glide.with(getContext().getApplicationContext())
                                            .load(imag)
                                            //  .apply(new RequestOptions().override(1200, 200))
                                            .apply(new RequestOptions().disallowHardwareConfig()
                                                    .override(Target.SIZE_ORIGINAL)
                                                    //  .dontTransform()
                                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                    .skipMemoryCache(true)
                                                    .dontAnimate()
                                                    .placeholder(R.drawable.loading)
                                                    //.fitCenter()
                                                    .format(DecodeFormat.PREFER_ARGB_8888))
                                            .into(profile);

                                    PersonalInfo activity = (PersonalInfo) getActivity();
                                    String getData = activity.s3;
                                    if (!getData.equals("")) {
                                        photoclick.setVisibility(View.VISIBLE);
                                        l1.setClickable(true);
                                        l1.setBackgroundColor(Color.parseColor("#0099cc"));
                                    } else {
                                        // Toast.makeText(activity, "hi", Toast.LENGTH_SHORT).show();
                                        //  photoclick.setVisibility(GONE);
                                    }
                                } else {

                                }
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();

                            // Toast.makeText(getContext().getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT).show();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext().getApplicationContext(), "Server has encountered problem !!!", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }
}
