package com.business.capitaltrade;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OthersDocument extends BaseActivity implements OnItemClick {
    String[] nameArray;
    String abc = "";
    AlertDialog alertDialogAndroid;
    String[] serial;
    public static String docnaam = "";
    String code = "", date = "", doc = "", status = "", name = "", position = "";
    int sr = 1;
    int sr1 = 1;
    ArrayList<ModelDocs> ar = new ArrayList<ModelDocs>();
    RequestQueue mRequest;
    RecyclerDocuments adapter;
    SessionManager session;
    String sesid = "";
    TextView filename;
    String selectedFilePath = "", value = "", borrowerid = "", loanid = "", docname = "";
    String resp = "";
    ProgressDialog progressDialog;
    String urll = "";
    private String strImage = "";
    public static final String UPLOAD_URL = "https://onistech.in/capitaltrade/android/upload.php";
    //String tarrek[]={"1","","","",""};
    // String vald[]={"2","","","",""};
    RecyclerView lv;

    Dialog dialog;
    //Pdf request code
    private int PICK_PDF_REQUEST = 1;
    String s1 = "", empstatus = "";
    FloatingActionButton fab;
    long fileSizeInMB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_document);
        setResult(RESULT_OK);
        mRequest = Volley.newRequestQueue(OthersDocument.this);
        filename = (TextView) findViewById(R.id.filename);
        lv = (RecyclerView) findViewById(R.id.my_recycler_view);
        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();

        sesid = user.get(SessionManager.KEY_ID);
        borrowerid = sesid + "-";


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            s1 = bundle.getString("loanid");
            empstatus = bundle.getString("empstatus");
            if (empstatus.equals("Employee")) {
                nameArray = getResources().getStringArray(R.array.salary);
                serial = getResources().getStringArray(R.array.salaryserial);
                // Toast.makeText(this, "salary", Toast.LENGTH_SHORT).show();
                urll = "https://onistech.in/capitaltrade/android/uploadeddoc.php?id=" + s1 + "";
                getvolley();


            } else if (empstatus.equals("Self-employed")) {
                nameArray = getResources().getStringArray(R.array.business);
                serial = getResources().getStringArray(R.array.businessserial);
                //Toast.makeText(this, "business", Toast.LENGTH_SHORT).show();
                urll = "https://onistech.in/capitaltrade/android/uploadeddoc.php?id=" + s1 + "";
                getvolley();

                // Toast.makeText(this, ""+empstatus, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Sorry,Some problem occured!!!", Toast.LENGTH_SHORT).show();
            }
            // Toast.makeText(this, ""+s1, Toast.LENGTH_SHORT).show();

        } else {

        }

        loanid = "-" + s1;
        //  Toast.makeText(this, ""+loanid, Toast.LENGTH_SHORT).show();
        //   getvolley();

    }

    private void getvolley() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.showUploadedDoc(s1), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d(TAG,"working"+response);
                progressDialog.dismiss();
                try {
                    //ar.clear();

                    for (int i = 0; i < nameArray.length; i++) {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String success = jsonObject.getString("success");
                        String status = jsonObject.getString("status");

                        if (success.equals(0) && status.equals(401)) {
                            session.logoutUser();
                            Toast.makeText(OthersDocument.this, "Sorry, sessionn time out!!!", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else {
                            code = jsonObject.getString("code");
                            date = jsonObject.getString("date");
                            doc = jsonObject.getString("doc");
                            status = jsonObject.getString("status");
                            position = jsonObject.getString("position");
                            //  name=nameArray[i];
                            // sr=sr1++;
                            ar.add(new ModelDocs(code, date, doc, status, position));
                            adapter = new RecyclerDocuments(ar, OthersDocument.this, OthersDocument.this, nameArray, serial);
                            lv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                            lv.setAdapter(adapter);
                        }
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();

                    //  Toast.makeText(OthersDocument.this, "Something went wrong!!!", Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(OthersDocument.this, "Sorry ,Something went Wrong Or No Connection !! ", Toast.LENGTH_SHORT).show();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };;

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(OthersDocument.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Fetching Document Info...");
        progressDialog.setCancelable(false);
        progressDialog.show();


//                    for (int i = 0; i < nameArray.length; i++) {
//
//
//                        name=nameArray[i];
//                        sr=sr1++;
//                        Toast.makeText(this, ""+String.valueOf(sr), Toast.LENGTH_SHORT).show();
////                        //Toast.makeText(this, ""+String.valueOf(sr), Toast.LENGTH_SHORT).show();
////                        //date=tarrek[i];
////                      //  status=vald[i];
////
////                        //   String arr[] =doc.split();
////                        //  Toast.makeText(OtherDoc.this, ""+code, Toast.LENGTH_SHORT).show();
//////                        Toast.makeText(getContext(), ""+borrreqloanid, Toast.LENGTH_SHORT).show();
//////                        Toast.makeText(getContext(), ""+borrid, Toast.LENGTH_SHORT).show();
//////                        Toast.makeText(getContext(), ""+pname, Toast.LENGTH_SHORT).show();
//////                        Toast.makeText(getContext(), ""+lamnt, Toast.LENGTH_SHORT).show();
//////                        Toast.makeText(getContext(), ""+tenure, Toast.LENGTH_SHORT).show();
//////                        Toast.makeText(getContext(), ""+roi, Toast.LENGTH_SHORT).show();
////
//                        adapter = new RecyclerDocuments(Ali(),OthersDocument.this,this);
//                        lv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
//                        lv.setAdapter(adapter);


    }


//    public ArrayList<ModelDocs> Ali() {
//        ModelDocs m1 = new ModelDocs(code, date, doc,status,position);
//        ar.add(m1);
//        Toast.makeText(OthersDocument.this, ""+position, Toast.LENGTH_SHORT).show();
//        return ar;
//
//    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            //  filePath = data.getData();

            Uri selectedFileUri = data.getData();
            try {
                selectedFilePath = FilePath.getPath(this, selectedFileUri);
                //selectedFilePath = FilePath.getPath(this,selectedFileUri);
            } catch (URISyntaxException e) {

            }
            Log.i("File PAth", "Selected File Path:" + selectedFilePath);
            Log.i("File PAth", "Selected Uri:" + selectedFileUri.getPath());

            if (selectedFilePath != null && !selectedFilePath.equals("")) {
                String[] parts = selectedFilePath.split("/");
                final String fileName = parts[parts.length - 1];

                docnaam = fileName;
                File selectedFile = new File(selectedFilePath);
                // Get length of file in bytes
                long fileSizeInBytes = selectedFile.length();
                // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                long fileSizeInKB = fileSizeInBytes / 1024;
                //  Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                fileSizeInMB = fileSizeInKB / 1024;
                // Toast.makeText(this, ""+String.valueOf(fileSizeInMB), Toast.LENGTH_SHORT).show();

                // filename.setText(selectedFilePath);

                final AlertDialog.Builder imageDialog = new AlertDialog.Builder(OthersDocument.this);
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

                View customView = inflater.inflate(R.layout.uploadalertdialog, null);
                imageDialog.setView(customView);
                TextView filename = (TextView) customView.findViewById(R.id.filename);
                final TextView alert = (TextView) customView.findViewById(R.id.alert);
                Button yes = (Button) customView.findViewById(R.id.yes);
                Button no = (Button) customView.findViewById(R.id.no);
                alertDialogAndroid = imageDialog.create();
                alertDialogAndroid.setCanceledOnTouchOutside(false);
                alertDialogAndroid.setCancelable(false);
                imageDialog.setCancelable(false);
                filename.setText(fileName);
                alertDialogAndroid.show();
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (fileSizeInMB > 5) {
                            alert.setVisibility(View.VISIBLE);
                            alert.setText("File size can not be more than 5 MB");
                            // Toast.makeText(OthersDocument.this, "no", Toast.LENGTH_SHORT).show();

                            //Toast.makeText(OtherDoc.this, "File size should not be greater than 5 MB", Toast.LENGTH_SHORT).show();
                        } else {
                            //  Toast.makeText(OthersDocument.this, "yees", Toast.LENGTH_SHORT).show();
                            if (selectedFilePath != null) {
                                // progressDialog = ProgressDialog.show(OtherDoc.this,"","Uploading File...",true);
                                progressDialog = new ProgressDialog(OthersDocument.this, R.style.AppCompatAlertDialogStyle);
                                progressDialog.setMessage("Uploading File...");
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //creating new thread to handle Http Operations
                                        if (isNetworkAvailable(OthersDocument.this)) {
                                            uploadFile(selectedFilePath, docname, abc);
                                        } else {
                                            Toast.makeText(OthersDocument.this, "No Internet Available !!!", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }).start();
                            } else {
                                Toast.makeText(OthersDocument.this, "Please choose a File First", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogAndroid.dismiss();
//                        String str =borrowerid+docname+loanid+abc;
//                        Toast.makeText(OthersDocument.this, ""+str, Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Toast.makeText(this, "Cannot upload file to server", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(String value, String value2) {
        //str();
        int i = Integer.parseInt(value);

        if (empstatus.equals("Self-employed")) {

            if (i == 1) {
                docname = "itr";
            } else if (i == 2) {
                docname = "balacesheet";
            } else if (i == 3) {
                docname = "Businesslincense";
            } else if (i == 4) {
                docname = "Professionallicense";
            } else if (i == 5) {
                docname = "rc";
            } else if (i == 6) {
                docname = "address";
            } else {
                Toast.makeText(this, "Some error occured", Toast.LENGTH_SHORT).show();
            }

        } else if (empstatus.equals("Employee")) {

            if (i == 1) {
                docname = "itr";
            } else if (i == 2) {
                docname = "form";
            } else if (i == 3) {
                docname = "letter";
            } else if (i == 4) {
                docname = "payslip";
            } else if (i == 5) {
                docname = "incrementltr";
            } else if (i == 6) {
                docname = "bankstmt";
            } else {
                Toast.makeText(this, "Some error occured", Toast.LENGTH_SHORT).show();
            }

        }
        // docname=value2;
        abc = "-" + String.valueOf(i);
        showFileChooser();
        //Toast.makeText(this, ""+docname, Toast.LENGTH_SHORT).show();
        //  Toast.makeText(this, ""+abc, Toast.LENGTH_SHORT).show();
    }


    public int uploadFile(final String selectedFilePath, String doc, String position) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        int serverResponseCode = 0;

        HttpURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);


        String[] parts = selectedFilePath.split("/");
        final String fileName = parts[parts.length - 1];

        if (!selectedFile.isFile()) {
            progressDialog.dismiss();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(OthersDocument.this, "File doesn't exist", Toast.LENGTH_SHORT).show();
                    //  editText.setText("Source File Doesn't Exist: " + selectedFilePath);
                }
            });
            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL(UrlUtils.uploadDoc());
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                // connection.setRequestProperty("uploaded_file",selectedFilePath+"2aman.pdf");
                connection.setRequestProperty("uploaded_file", borrowerid + docname + loanid + position + ".pdf");
                // connection.addRequestProperty("getting",editText.getText().toString());
                //conn.addRequestProperty
                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
//                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
//                        + selectedFilePath+"2aman.pdf" + "\"" + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                        + borrowerid + doc + loanid + position + ".pdf" + "\"" + lineEnd);
                dataOutputStream.writeBytes(lineEnd);
                //     dataOutputStream.wr


                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0) {
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                //  Log.i("Response=", new String(buffer, 0, bytesRead, "utf-8"));
                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();

                InputStream in = null;
                try {
                    in = connection.getInputStream();
                    byte[] buffer1 = new byte[1024];
                    int read;
                    while ((read = in.read(buffer1)) > 0) {
                        // System.out.println(new String(buffer1, 0, read, "utf-8"));
                        // Log.i("Response=", new String(buffer1, 0, read, "utf-8"));
                        String st = new String(buffer1, 0, read, "utf-8");
                        resp = st.intern().trim();
                        Log.i("upload.php=", resp);
                        // Toast.makeText(this, ""+s2, Toast.LENGTH_SHORT).show();
                    }
                } finally {
                    in.close();
                }
                Log.i("Server Response", "Server Response is: " + serverResponseMessage + ": " + serverResponseCode);

                //response code of 200 indicates the server status OK
                if (serverResponseCode == 200) {
                    //progressDialog.dismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.i("resp", resp);
                            if (resp.equals("failed")) {
                                Toast.makeText(OthersDocument.this, "File name already exist!!", Toast.LENGTH_SHORT).show();
                            } else if (resp.equals("success")) {
                                Toast.makeText(OthersDocument.this, "Uploaded successfully", Toast.LENGTH_SHORT).show();
                            } else if (resp.equals("fail")) {
                                Toast.makeText(OthersDocument.this, "Sorry, Failed to upload !!!", Toast.LENGTH_SHORT).show();
                            } else if (resp.equals("onistech")) {
                                Toast.makeText(OthersDocument.this, "Sorry ,Server has encountered problem !!!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(OthersDocument.this, "Something went wrong !!!", Toast.LENGTH_SHORT).show();
                            }
                            alertDialogAndroid.dismiss();
                            getvolley();


                        }
                    });
                } else {
                    Toast.makeText(this, "Sorry,File Not Uploaded !!!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();

            } catch (FileNotFoundException e) {
                //  progressDialog.dismiss();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(OthersDocument.this, "File Not Found", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (MalformedURLException e) {

                //progressDialog.dismiss();
                Toast.makeText(OthersDocument.this, "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {

                //progressDialog.dismiss();
                Toast.makeText(OthersDocument.this, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
            }
            progressDialog.dismiss();

            return serverResponseCode;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
