package com.business.capitaltrade;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

;

import java.util.Arrays;
import java.util.List;

public class WebView extends AppCompatActivity {
    android.webkit.WebView webview;
    ProgressBar progressbar;
    String str="",loanid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        str= getIntent().getExtras().getString("name");
        List<String> doclist = Arrays.asList(str.split("-"));   // method to split string
        String arr[] =doclist.get(2).split("\\.");

        loanid=arr[0];
        getSupportActionBar().setTitle(doclist.get(1)+"."+arr[1]);
        webview = (android.webkit.WebView)findViewById(R.id.webview);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        webview.getSettings().setJavaScriptEnabled(true);
        // String filename ="https://onistech.in/gpschool/asset_Admin/img/Tikona_new.pdf";
        String filename =str;
       // Toast.makeText(this, ""+filename, Toast.LENGTH_SHORT).show();
        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=" + filename);

        webview.setWebViewClient(new WebViewClient() {

            public void onPageFinished(android.webkit.WebView view, String url) {
                // do your stuff here
                progressbar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(WebView.this, OtherDoc.class).putExtra("loanid",loanid));
       //finish();
    }
}
