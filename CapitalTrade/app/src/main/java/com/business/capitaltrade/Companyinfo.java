package com.business.capitaltrade;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Companyinfo extends BaseActivity {
    SessionManager session;
    ProgressDialog progressDialog;
    RadioButton rb1,rb;
    String id="";
    RelativeLayout relativeLayout;
    CheckBox cash,bank,cheque;
    LinearLayout l1,l2,l3,click;
    RadioGroup rg;
    EditText comname,comadd,comdesig,comsal,comwork,combadge,bname,bdesg,bwork,bsal,bgst,badd,bpan,pf;
    String cname="",cadd="",cdesig="",csal="",cwork="",cbadge="",bname1="",badd1="",bdesg1="",bwork1="",bsal1="",bgst1="",selectedtext="",bpan1="",
    pf1="",modesalary="",empstatus="";
    int count=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companyinfo);
        session = new SessionManager(Companyinfo.this);
        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */
        session.checkLogin();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        l1 = (LinearLayout) findViewById(R.id.compl1);
        l2 = (LinearLayout) findViewById(R.id.compl2);
        l3 = (LinearLayout) findViewById(R.id.compl3);
        click = (LinearLayout) findViewById(R.id.linear);
        cash = (CheckBox) findViewById(R.id.cash);
        cheque = (CheckBox) findViewById(R.id.cheque);
        bank = (CheckBox) findViewById(R.id.bank);
        // email
        id = user.get(SessionManager.KEY_ID);

         rg = (RadioGroup) findViewById(R.id.radio);

        rb = (RadioButton) findViewById(R.id.rb);
        rb1 = (RadioButton) findViewById(R.id.rb1);
        relativeLayout=(RelativeLayout)findViewById(R.id.container);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                if (rb.isChecked()) {
                    l1.setVisibility(View.VISIBLE);
                    l2.setVisibility(View.VISIBLE);
                    l3.setVisibility(View.GONE);


                } else if (rb1.isChecked()) {
                    l1.setVisibility(View.GONE);
                    l2.setVisibility(View.GONE);
                    l3.setVisibility(View.VISIBLE);
                    // mark ="1";
                }


                // Toast.makeText(CreateEvent.this, "" + mark, Toast.LENGTH_LONG).show();
            }
        });

        comname=(EditText)findViewById(R.id.comname);
        comadd=(EditText)findViewById(R.id.comadd);
        comdesig=(EditText)findViewById(R.id.comdesig);
        comwork=(EditText)findViewById(R.id.comwork);
        comsal=(EditText)findViewById(R.id.comsal);
        combadge=(EditText)findViewById(R.id.combadge);
        pf=(EditText)findViewById(R.id.pf);

        bname=(EditText)findViewById(R.id.bname);
        badd=(EditText)findViewById(R.id.badd);
        bdesg=(EditText)findViewById(R.id.bdesig);
        bwork=(EditText)findViewById(R.id.bwork);
        bsal=(EditText)findViewById(R.id.bsalary);
        bgst=(EditText)findViewById(R.id.gstin);
        bpan=(EditText)findViewById(R.id.bpan);

       // comname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
       // comdesig.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
       // bname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        bpan.setFilters(new InputFilter[]{new InputFilter.AllCaps(),new InputFilter.LengthFilter(10)});
        bgst.setFilters(new InputFilter[]{new InputFilter.AllCaps(),new InputFilter.LengthFilter(15)});
        //bdesg.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        combadge.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        //bname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        badd.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
       // bdesg.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    checkstatus();




            }
        });

        HashMap<String, String> user2 = session.getCompanyinformation();
        empstatus= user2.get(SessionManager.KEY_EMPSTATUS);
        if(empstatus==null || empstatus.equals("")) {
            checkinfo();
        }
        else if(empstatus.equals("Employee"))
        {
            cname = user2.get(SessionManager.KEY_CNAME);
            cadd = user2.get(SessionManager.KEY_CADDRESS);
            cdesig = user2.get(SessionManager.KEY_CDESIGNATION);
            cwork = user2.get(SessionManager.KEY_CWORKINGYEAR);
            modesalary = user2.get(SessionManager.KEY_SALARYMODE);
            csal = user2.get(SessionManager.KEY_MONTHLYSALARY);
            cbadge = user2.get(SessionManager.KEY_EMPBADGE);
            pf1 = user2.get(SessionManager.KEY_PF);
            rb.setChecked(true);
            rb1.setEnabled(false);
            comname.setText(cname);
            comadd.setText(cadd);
            comdesig.setText(cdesig);
            comwork.setText(cwork);
            comsal.setText(csal);
            combadge.setText(cbadge);
            pf.setText(pf1);
            if(modesalary.equals("Cash"))
                cash.setChecked(true);
            else if(modesalary.equals("Cheque"))
                cheque.setChecked(true);
            else if(modesalary.equals("Bank Transfer"))
                bank.setChecked(true);
        }
        else if(empstatus.equals("Self-employed")) {
            rb1.setChecked(true);
            rb.setEnabled(false);
            bname1 = user2.get(SessionManager.KEY_CNAME);
            badd1 = user2.get(SessionManager.KEY_CADDRESS);
            bdesg1 = user2.get(SessionManager.KEY_CDESIGNATION);
            bwork1 = user2.get(SessionManager.KEY_CWORKINGYEAR);
            bsal1 = user2.get(SessionManager.KEY_ANNUAL);
            bgst1 = user2.get(SessionManager.KEY_GSTIN);
            bpan1 = user2.get(SessionManager.KEY_PAN);

            bname.setText(bname1);
            badd.setText(badd1);
            bdesg.setText(bdesg1);
            bwork.setText(bwork1);
            bsal.setText(bsal1);
            // combadge.setText(badge);
            bgst.setText(bgst1);
            bpan.setText(bpan1);
        }


        //checkinfo();
        getcheckbox();

    }

    public void checkstatus(){
        if(isNetworkAvailable(Companyinfo.this)) {
            if (rg.getCheckedRadioButtonId() == -1) {

            } else {

                // get selected radio button from radioGroup
                int selectedId = rg.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton)
                        rg.findViewById(selectedId);
                selectedtext = (String) radioButton.getText();

                //Toast.makeText(getApplicationContext(), ""+selectedtext+ " is selected", Toast.LENGTH_SHORT).show();
                if (selectedtext.equals("Employee")) {
                    getcominfo();
                } else {
                    binfo();
                }

            }
        }
        else
        {
            final Snackbar snackbar = Snackbar
                    .make(relativeLayout, "No Internet Connection",
                            Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // snackbar.dismiss();
                            checkstatus();
                        }
                    });
            snackbar.setActionTextColor(Color.RED);
            View sbView = snackbar.getView();

            ViewGroup.LayoutParams params = sbView.getLayoutParams();
            params.height = 100;
            sbView.setLayoutParams(params);

            TextView textView = (TextView) sbView.findViewById
                    (android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);


//            FrameLayout.LayoutParams parentParams = (FrameLayout.LayoutParams)
            sbView.getLayoutParams();



            snackbar.show();
        }
    }

    private void uploadinfo() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";
        String urll="https://onistech.in/capitaltrade/android/userroll.php";
        ;
        urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.companyInfo(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                       //  Toast.makeText(Companyinfo.this, response, Toast.LENGTH_SHORT).show();
                        try {
                            // ar.clear();
                            //for (int i = 0; i < response.length(); i++) {
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            if (jsonArray.length()>0) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(1);
                                String success = jsonObject1.getString("success");
                                if (success.equals("0") ) {
                                    session.logoutUser();
                                    Toast.makeText(Companyinfo.this, "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                                    finish();
                                }
                            }

                            else {
                                String code = jsonObject.getString("code");
                                //  String status = jsonObject.getString("role");

                                if (code.equals("success")) {
                                    if (selectedtext.equals("Employee")) {
                                        session.createcompanyinfo(selectedtext, cname, cadd, cdesig, cwork, modesalary, csal, cbadge, pf1,
                                                "", "", "");
                                    } else if (selectedtext.equals("Self-employed")) {

                                        session.createcompanyinfo(selectedtext, bname1, badd1, bdesg1, bwork1, "", "", "", "",
                                                bsal1, bgst1, bpan1);
                                    }

                                    Toast.makeText(Companyinfo.this, "Data uploaded successfully", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(Companyinfo.this, MainActivity.class);
                                    //intent.putExtra("EXTRA", "openFragment");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("fragmentNumber", 1);
                                    //intent.putExtra("childname",s);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } catch (JSONException e) {
                            //Toast.makeText(Companyinfo.this, "Sorry, something went wrong!!!", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Companyinfo.this, "Server has encountered problem !!!", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("mob",mob);
                params.put("id",id);

                params.put("Authorizations" ,"Bearer "+ AppSession.logedInToken);

                if(selectedtext.equals("Employee"))
                {
                    params.put("roll",selectedtext);
                    params.put("cname",cname);
                    params.put("cadd",cadd);
                    params.put("cdesig",cdesig);
                    params.put("cwork",cwork);
                    params.put("csal",csal);
                    params.put("cbadge",cbadge);
                    params.put("pf",pf1);
                    params.put("mode",modesalary);


                }
                else if(selectedtext.equals("Self-employed"))
                {
                    params.put("roll",selectedtext);
                    params.put("bname",bname1);
                    params.put("badd",badd1);
                    params.put("bdesig",bdesg1);
                    params.put("bwork",bwork1);
                    params.put("bsal",bsal1);
                    params.put("bgst",bgst1);
                    params.put("bpan",bpan1);
                 //   params.put("back",imagetostring1(bitmap1));
                }
//                else if(secret.equals("3"))
//                {
//
//                    params.put("adharno",adhaarno);
//                    params.put("status","1");
//                }
//                //
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations" ,"Bearer "+ AppSession.logedInToken);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                35000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(Companyinfo.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(Companyinfo.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void checkinfo() {
        //  String urll="http://bollywoodcity.in/event/updateprofile.php?signup_id="+idd+"&u_name="+na+"&u_mail="+em+"&u_mobile="+mo+"&u_pass="+pa+"";

        String urll="https://onistech.in/capitaltrade/android/showroll.php?id="+id+"";
        ;
        // urll = urll.replaceAll(" ", "%20");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlUtils.showCompanyInfo(id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        // Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            //get value of first index
                            String success = jsonObject.getString("success");

                            if (success.equals("0") ) {
                                Toast.makeText(Companyinfo.this, "Sorry, session time out!!!", Toast.LENGTH_LONG).show();
                                session.logoutUser();
                                finish();
                            }
                            else {
                                String status = jsonObject.getString("role");
                                if (status.equals("Employee")) {
                                    rb.setChecked(true);
                                    rb1.setEnabled(false);
                                    String name = jsonObject.getString("cname");
                                    String add = jsonObject.getString("cadd");
                                    String desig = jsonObject.getString("cdesig");
                                    String work = jsonObject.getString("cwork");
                                    String sal = jsonObject.getString("csal");
                                    String badge = jsonObject.getString("cbadge");
                                    String gst = jsonObject.getString("cgst");
                                    String getpf = jsonObject.getString("pf");
                                    String mode = jsonObject.getString("mode");
                                    comname.setText(name);
                                    comadd.setText(add);
                                    comdesig.setText(desig);
                                    comwork.setText(work);
                                    comsal.setText(sal);
                                    combadge.setText(badge);
                                    pf.setText(getpf);
                                    if (mode.equals("Cash"))
                                        cash.setChecked(true);
                                    else if (mode.equals("Cheque"))
                                        cheque.setChecked(true);
                                    else if (mode.equals("Bank Transfer"))
                                        bank.setChecked(true);

                                    session.createcompanyinfo(status, name, add, desig, work, mode, sal, badge, getpf,
                                            "", "", "");

                                } else if (status.equals("Self-employed")) {
                                    rb1.setChecked(true);
                                    rb.setEnabled(false);
                                    String name = jsonObject.getString("cname");
                                    String add = jsonObject.getString("cadd");
                                    String desig = jsonObject.getString("cdesig");
                                    String work = jsonObject.getString("cwork");
                                    String sal = jsonObject.getString("csal");
                                    String badge = jsonObject.getString("cbadge");
                                    String gst = jsonObject.getString("cgst");
                                    String pan = jsonObject.getString("bpan");

                                    bname.setText(name);
                                    badd.setText(add);
                                    bdesg.setText(desig);
                                    bwork.setText(work);
                                    bsal.setText(sal);
                                    // combadge.setText(badge);
                                    bgst.setText(gst);
                                    bpan.setText(pan);


                                    session.createcompanyinfo(status, name, add, desig, work, "", "", "", "",
                                            sal, gst, pan);
                                }
                            }
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Companyinfo.this, "Server has encountered problem !!!", Toast.LENGTH_SHORT).show();

            }

        })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                return params;
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(Companyinfo.this);
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

        //initialize the progress dialog and show it
        progressDialog = new ProgressDialog(Companyinfo.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void getcominfo(){
       cname= comname.getText().toString().trim();
      cadd=  comadd.getText().toString().trim();
       cdesig= comdesig.getText().toString().trim();
       cwork= comwork.getText().toString().trim();
       csal= comsal.getText().toString().trim();
       cbadge= combadge.getText().toString().trim();
        pf1= pf.getText().toString().trim();
     //   Toast.makeText(this, ""+pf1, Toast.LENGTH_SHORT).show();
        if(cname.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Company Name")
//and some more method calls
                    .show();
        }

        else if(cadd.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Company address")
//and some more method calls
                    .show();
        }

        else if(cdesig.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill your designation")
//and some more method calls
                    .show();
        }

        else if(cwork.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill working years ")
//and some more method calls
                    .show();
        }
        else if(modesalary.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please select Mode of Salary ")
//and some more method calls
                    .show();
        }

        else if(csal.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill your monthly salary")
//and some more method calls
                    .show();
        }
        else {
            uploadinfo();
        }

    }


    public void binfo(){
        bname1= bname.getText().toString().trim();
        badd1=  badd.getText().toString().trim();
        bdesg1= bdesg.getText().toString().trim();
        bwork1= bwork.getText().toString().trim();
        bsal1= bsal.getText().toString().trim();
        bgst1= bgst.getText().toString().trim();
        bpan1= bpan.getText().toString().trim();
        if(bname1.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Company Name")
//and some more method calls
                    .show();
        }

        else if(badd1.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Company address")
//and some more method calls
                    .show();
        }

        else if(bdesg1.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill your designation")
//and some more method calls
                    .show();
        }

        else if(bwork1.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill working years ")
//and some more method calls
                    .show();
        }

        else if(bsal1.equals(""))
        {
            new AlertDialog.Builder(Companyinfo.this)
                    //.setTitle("Record New Track")
                    .setMessage("Please fill Annual Turnover")
//and some more method calls
                    .show();
        }
        else if(!bgst1.equals("") && bgst1.length()<15)
        {

                new AlertDialog.Builder(Companyinfo.this)
                        //.setTitle("Record New Track")
                        .setMessage("GST number can't be less than 15 alphanumeric")
//and some more method calls
                        .show();


        }

        else if(!bpan1.equals("") && bpan1.length()<10)
        {

                new AlertDialog.Builder(Companyinfo.this)
                        //.setTitle("Record New Track")
                        .setMessage("Pan number can't be less than 10 alphanumeric")
//and some more method calls
                        .show();


        }
        else {
            uploadinfo();
        }

    }

    public  void getcheckbox() {
        cash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cheque.setChecked(false);
                    bank.setChecked(false);
                    modesalary=compoundButton.getText().toString().trim();

                }
            }
        });
        cheque.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cash.setChecked(false);
                    bank.setChecked(false);
                    modesalary=compoundButton.getText().toString().trim();

                }
            }
        });
        bank.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cash.setChecked(false);
                    cheque.setChecked(false);
                    modesalary=compoundButton.getText().toString().trim();

                }
            }
        });

    }
}
