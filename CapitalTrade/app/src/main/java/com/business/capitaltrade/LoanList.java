package com.business.capitaltrade;


import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class LoanList extends Fragment implements RecyclerView.OnScrollChangeListener {
    ArrayList<Model2> ar = new ArrayList<Model2>();
    ProgressBar p1;
    String emiid = "", borrreqloanid = "", borrid = "", emiamnt = "", principal = "", interest = "", balance = "", duedate = "", panalty = "", sr = "",
            fixpanalty = "", emistatus = "", depositedate = "", id = "", code = "", value = "";
    // SessionManager session;
    RecyclerView r1;
    Recyclertwo rec;
    ProgressDialog progressDialog;
    SessionManager session;
    //LinearLayoutManager manager;
    private RecyclerView.LayoutManager layoutManager;
    //The request counter to send ?page=1, ?page=2  requests
    private int requestCount = 1;
    private RecyclerView.Adapter adapter;
    private RequestQueue requestQueue;
    //String data="1";
    int data = 1;
    int serial = 0;

    public LoanList() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("EMI's");
        View v = inflater.inflate(R.layout.fragment_loan_list, container, false);
        //   mRequest = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        value = getArguments().getString("ID");
        //Toast.makeText(getContext(), ""+value, Toast.LENGTH_SHORT).show();
        r1 = (RecyclerView) v.findViewById(R.id.recy);
        r1.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        r1.setLayoutManager(layoutManager);
        //  manager= new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        p1 = (ProgressBar) v.findViewById(R.id.progressBar1);

        //Adding an scroll change listener to recyclerview

//        r1.setHasFixedSize(true);
//         r1.setItemViewCacheSize(20);
//        r1.setDrawingCacheEnabled(true);
//        r1.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        session = new SessionManager(getContext());
        HashMap<String, String> user = session.getUserDetails();
        id = user.get(SessionManager.KEY_ID);
        //mob = user.get(SessionManager.KEY_MOBILE);
        getData();
        r1.setOnScrollChangeListener(this);


        adapter = new Recyclertwo(ar, getContext());

        r1.setAdapter(adapter);


        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //  Log.i(tag, "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    NoOFLoans e1 = new NoOFLoans();
                    FragmentTransaction manager = getFragmentManager().beginTransaction();
                    manager.replace(R.id.fragment_container, e1);
                    // manager.addToBackStack(null);
                    manager.commit();

                    return true;
                }
                return false;
            }
        });
//        r1.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
//                {
//                    isscrolling = true;
//                }
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                currentitem= r1.getChildCount();
//                totalitem=manager.getItemCount();
//                scrollitem=manager.findFirstVisibleItemPosition();
//                if(isscrolling && currentitem + scrollitem == totalitem)
//                {
//                    isscrolling = false;
//                   // fetchdata();
//                }
//            }
//        });
        return v;
    }


    private JsonArrayRequest getDataFromServer(int requestCount, int data) {
        //Initializing ProgressBar

        String urll = "https://onistech.in/capitaltrade/android/showemi.php?id=" + value + "&page=" + String.valueOf(requestCount) + "&serial=" + String.valueOf(data);
        //Displaying Progressbar
        p1.setVisibility(View.VISIBLE);
        //setProgressBarIndeterminateVisibility(true);

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(UrlUtils.showEmiChart(value, String.valueOf(requestCount), String.valueOf(data)),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //Calling method parseData to parse the json response
                        parseData(response);
                        //Hiding the progressbar
                        p1.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        p1.setVisibility(View.GONE);
                        //If an error occurs that means end of the list has reached
                        Toast.makeText(getContext(), "No EMI  is available", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorizations", "Bearer " + AppSession.logedInToken);
                // params.put("Authorizations", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2FuZHJvaWRcLyIsImF1ZCI6Imh0dHA6XC9cL2xvY2FsaG9zdFwvYW5kcm9pZFwvIiwiaWF0IjoxNjQwMTQyMDg4LCJleHAiOjE2NDAxNDU2ODgsImRhdGEiOnsiYm9ycm93ZXJfaWQiOiIxMzA0MSJ9fQ.cYCdfE28XxwWkmBJki0BZfb7jHP2GLgaz7ZcxPm87JQ");
                return params;
            }
        };
        ;

        //Returning the request
        return jsonArrayRequest;
    }

    private void parseData(final JSONArray array) {


        for (int i = 0; i < array.length(); i++) {
            //  Model2 superHero = new Model2();
            JSONObject jsonObject = null;

            try {
                jsonObject = array.getJSONObject(i);
                String success = jsonObject.getString("success");

                if (success.equals("0") ) {
                    session.logoutUser();
                    Toast.makeText(getActivity(), "Sorry, session time out!!!", Toast.LENGTH_LONG).show();

                }
                else {
                    emiid = jsonObject.getString("emiid");
                    borrreqloanid = jsonObject.getString("borrreqloanid");
                    borrid = jsonObject.getString("borrid");
                    emiamnt = jsonObject.getString("emiamnt");
                    principal = jsonObject.getString("pricipal");
                    interest = jsonObject.getString("interest");
                    balance = jsonObject.getString("balance");
                    duedate = jsonObject.getString("duedate");
                    panalty = jsonObject.getString("panalty");
                    fixpanalty = jsonObject.getString("fixpanalty");
                    emistatus = jsonObject.getString("emistatus");
                    depositedate = jsonObject.getString("depositedate");
                    sr = jsonObject.getString("sr");
                    code = jsonObject.getString("code");
                }
            } catch (JSONException e) {

            }
            Model2 m1 = new Model2(emiid, borrreqloanid, borrid, emiamnt, principal, interest, balance, duedate, panalty, fixpanalty, emistatus, depositedate, sr, code);
            ar.add(m1);

        }
        adapter.notifyDataSetChanged();


    }

    //This method will get data from the web api
    private void getData() {
        //Adding the method to the queue by calling the method getDataFromServer
        //  getvolley(requestCount);
        requestQueue.add(getDataFromServer(requestCount, data));
        //Incrementing the request counter
        requestCount++;
        data = data + 10;
    }

    //This method would check that the recyclerview scroll has reached the bottom or not
    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }

    //Overriden method to detect scrolling
    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        //Ifscrolled at last then
        if (isLastItemDisplaying(r1)) {
            //Calling the method getdata again
            getData();
            //   data++;
            // data+3;
        }
    }
}
