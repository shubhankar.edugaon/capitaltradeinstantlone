package com.business.capitaltrade;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class ShowAgreement extends AppCompatActivity {
    WebView webview;
    ProgressBar progressbar;
    String str="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_agreement);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        str= getIntent().getExtras().getString("name");
        webview = (WebView)findViewById(R.id.webview);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        webview.getSettings().setJavaScriptEnabled(true);
        // String filename ="https://onistech.in/gpschool/asset_Admin/img/Tikona_new.pdf";
        String filename =str;
        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=" + filename);

        webview.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                progressbar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ShowAgreement.this,Proposal.class));
        finish();
    }
    @Override
    public boolean onSupportNavigateUp(){
        startActivity(new Intent(ShowAgreement.this,Proposal.class));
        finish();
        return true;
    }
}
