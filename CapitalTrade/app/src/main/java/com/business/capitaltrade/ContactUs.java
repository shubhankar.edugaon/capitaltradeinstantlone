package com.business.capitaltrade;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Properties;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUs extends Fragment {
    //Declaring EditText
    public MainActivity activity;
    private EditText editTextEmail;
    private EditText editTextSubject;
    private EditText editTextMessage,editTextMobile;
    String code="";
    SessionManager session;
    ContactUs context = this;
    //Send button
    private Button buttonSend;
    public ContactUs()
    {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      View v= inflater.inflate(R.layout.fragment_contact_us, container, false);
        session = new SessionManager(getContext().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        code = user.get(SessionManager.KEY_CODE);
        getActivity().setTitle("Contact Us");
       // Initializing the views
      //  editTextEmail = (EditText) v.findViewById(R.id.editTextEmail);
        editTextSubject = (EditText)v. findViewById(R.id.editTextSubject);
        editTextMessage = (EditText) v.findViewById(R.id.editTextMessage);
        editTextMobile = (EditText) v.findViewById(R.id.editTextMobile);
//
        buttonSend = (Button) v.findViewById(R.id.buttonSend);

//
//        //Adding click listener
       // buttonSend.setOnClickListener(this);
        // contact=(TextView)v.findViewById(R.id.contact);
        //copy=(TextView)v.findViewById(R.id.copy);
       // email=(TextView)v.findViewById(R.id.email);
      //  contact.setPaintFlags(contact.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //  Log.i(tag, "keyCode: " + keyCode);
                if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {

                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("fragmentNumber",1);
                    startActivity(i);
                    ((Activity) getActivity()).overridePendingTransition(0, 0);

                    return true;
                }
                return false;
            }
        });


//        copy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ClipboardManager cm = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
//                cm.setText(email.getText().toString());
//                Toast.makeText(getContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
//            }
//        });


        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();

            }
        });



      return v;
    }


//    public void getMessage(Context context,String msg)
//    {
//        Log.i("MESSAGESENT",msg);
//
//       // get(msg);
//        Intent i = new Intent(getActivity(), UserInfo.class);
//        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        i.putExtra("fragmentNumber",1);
//        startActivity(i);
//        ((Activity) getActivity()).overridePendingTransition(0, 0);
//
//    }

    private void sendEmail() {
        if(editTextSubject.getText().toString().trim().equals(""))
        {
            new AlertDialog.Builder(getContext())
                    //.setTitle("Record New Track")
                    .setMessage("Please enter your name")
//and some more method calls
                    .show();
        }
        else if(editTextMobile.getText().toString().trim().length()<10)
        {
            new AlertDialog.Builder(getContext())
                    //.setTitle("Record New Track")
                    .setMessage("Please enter 10 digit Mobile Number")
//and some more method calls
                    .show();
        }
        else if(editTextMessage.getText().toString().trim().equals(""))
        {
            new AlertDialog.Builder(getContext())
                    //.setTitle("Record New Track")
                    .setMessage("Please enter Message")
//and some more method calls
                    .show();
        }
        else {
            //Getting content for email
            // String email = editTextEmail.getText().toString().trim();
            String subject = editTextSubject.getText().toString().trim() + " - " + code;
            String message = editTextMessage.getText().toString().trim() + "\n\n" + "Borrower Mobile - " + editTextMobile.getText().toString().trim();

            //Creating SendMail object
            SendMail sm = new SendMail(getContext(), "capitaltrade84@gmail.com", subject, message);

            //Executing sendmail to send email
            sm.execute();

        }
    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//                Intent i = new Intent(getActivity(), MainActivity.class);
//    //    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        i.putExtra("fragmentNumber",1);
//        startActivity(i);
//        ((Activity) getActivity()).overridePendingTransition(0, 0);
//    }
}
