package com.business.capitaltrade;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Lincoln on 05/05/16.
 */
public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences constants
    private static final String PREF_NAME = "MyPreference";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_FIRST_TIME_LAUNCH_PERMISSION = "IsFirstTimeLaunchPermission";
    private static final String LANGUAGE = "lang";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunchPermission(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH_PERMISSION, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunchPermission() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH_PERMISSION, true);
    }



    public static void setLanguage(Context context, String lang) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE).edit();
        editor.putString(LANGUAGE, lang);
        editor.commit();
    }


    public static String getLanguage(Context context) {
        SharedPreferences savedSession = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        return savedSession.getString(LANGUAGE, "english");
    }


    public void setLanguage(String lang) {
//        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME,
//                Context.MODE_PRIVATE).edit();
        editor.putString(LANGUAGE, lang);
        editor.commit();
    }

    public String getLanguage() {
//        SharedPreferences savedSession = context.getSharedPreferences(PREF_NAME,
//                Context.MODE_PRIVATE);
        return pref.getString(LANGUAGE, "english");
    }

}
