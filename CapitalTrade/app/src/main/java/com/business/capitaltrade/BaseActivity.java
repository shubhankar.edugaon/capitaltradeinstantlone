package com.business.capitaltrade;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.Toast;

import java.util.Locale;

public class BaseActivity extends AppCompatActivity {
   // final Context c = this;
  //  PrefManager prefManager = new PrefManager(c);
    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void changeLanguage() {

        String lang = "en";
        if (PrefManager.getLanguage(getApplicationContext()).equals("hindi"))
            lang = "hi";
        Resources res = getApplicationContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Locale locale = new Locale(lang);
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = locale;
        Locale.setDefault(locale);
        res.updateConfiguration(conf, dm);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getApplicationContext().createConfigurationContext(conf);
        } else {
            res.updateConfiguration(conf, dm);
        }
    }


//    public void changeLanguage() {
//        //Toast.makeText(getApplicationContext(), "hi", Toast.LENGTH_SHORT).show();
//        String lang = "en";
//        if (PrefManager.getLanguage(getApplicationContext()).equals("hindi"))
//            lang = "hi";
//        Resources res = getApplicationContext().getResources();
//        DisplayMetrics dm = res.getDisplayMetrics();
//        Locale locale = new Locale(lang);
//        android.content.res.Configuration conf = res.getConfiguration();
//        conf.locale = locale;
//        Locale.setDefault(locale);
//        res.updateConfiguration(conf, dm);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            getApplicationContext().createConfigurationContext(conf);
//        } else {
//            res.updateConfiguration(conf, dm);
//        }
//    }
}
