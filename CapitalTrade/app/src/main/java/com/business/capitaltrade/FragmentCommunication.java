package com.business.capitaltrade;


public interface FragmentCommunication {
    void respond(int position, String id);
}