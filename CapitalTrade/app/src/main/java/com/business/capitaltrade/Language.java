package com.business.capitaltrade;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class Language extends AppCompatActivity {
CheckBox ch1,ch2;
    AlertDialog alertDialogAndroid;
    private PrefManager prefManager;
    TextView eng,hindi1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);

 //=====================================Working ========================================================

        if (prefManager.isFirstTimeLaunch() ==true) {
          //  Toast.makeText(this, "first time", Toast.LENGTH_SHORT).show();
        }
        else {
            startActivity(new Intent(Language.this, PermissionRequest.class));
            // Toast.makeText(this, "second time", Toast.LENGTH_SHORT).show();
            finish();
        }
// =======================================================================================================//
        setContentView(R.layout.activity_language);
        ch1=(CheckBox)findViewById(R.id.ch1);
        ch2=(CheckBox)findViewById(R.id.ch2);
        eng=(TextView)findViewById(R.id.eng);
        hindi1=(TextView)findViewById(R.id.hind1);


        ch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    ch2.setChecked(false);
                }
            }
        });
        ch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    ch1.setChecked(false);
                }
            }
        });
    }

    public void go(View view) {
        if(!ch1.isChecked() && !ch2.isChecked()){
            Toast.makeText(this, "Please choose any one of the Language", Toast.LENGTH_SHORT).show();
        }
      else
        {
//            Intent myIntent = new Intent(Language.this,
//                    LoginActivity.class);
//            startActivity(myIntent);
//            finish();

            if(ch1.isChecked())
            {
               // String lang= "en";
                PrefManager.setLanguage(getApplicationContext(), "english");
                String lang = "en";
                if (PrefManager.getLanguage(getApplicationContext()).equals("hindi"))
                    lang = "hi";
                Resources res =getApplicationContext().getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Locale locale = new Locale(lang);
                android.content.res.Configuration conf = res.getConfiguration();
                conf.locale = locale;
                Locale.setDefault(locale);
                res.updateConfiguration(conf, dm);
                //prefManager.setLanguage("english");
                Intent myIntent = new Intent(Language.this,
                        PermissionRequest.class);
                startActivity(myIntent);
                finish();
            }
            else
            {
                PrefManager.setLanguage(getApplicationContext(), "hindi");
                String lang = "en";
                if (PrefManager.getLanguage(getApplicationContext()).equals("hindi"))
                    lang = "hi";
                Resources res =getApplicationContext().getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Locale locale = new Locale(lang);
                android.content.res.Configuration conf = res.getConfiguration();
                conf.locale = locale;
                Locale.setDefault(locale);
                res.updateConfiguration(conf, dm);

                final AlertDialog.Builder imageDialog = new AlertDialog.Builder(Language.this);
                LayoutInflater inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);

                View customView = inflater.inflate(R.layout.prompts,null);
                imageDialog.setView(customView);
                TextView text1 = (TextView) customView.findViewById(R.id.userInputDialog);
                TextView text = (TextView) customView.findViewById(R.id.text);
                TextView ok = (TextView) customView.findViewById(R.id.ok);
                text1.setText("Work is under Progress !");

                text.setText("Note - Please use English version ");
                ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialogAndroid.cancel();
                }
            });
                alertDialogAndroid = imageDialog.create();
//                alertDialogAndroid.setCanceledOnTouchOutside(false);
//                alertDialogAndroid.setCancelable(false);
//                imageDialog.setCancelable(false);
                alertDialogAndroid.show();
//                Resources activityRes = getResources();
//                Configuration activityConf = activityRes.getConfiguration();
//                Locale newLocale = new Locale(lang);
//                activityConf.setLocale(newLocale);
//                activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());
//
//                Resources applicationRes = getApplicationContext().getResources();
//                Configuration applicationConf = applicationRes.getConfiguration();
//                applicationConf.setLocale(newLocale);
//                applicationRes.updateConfiguration(applicationConf,
//                        applicationRes.getDisplayMetrics());


            }
//===============================================Working 2 ===============================================//
            prefManager.setFirstTimeLaunch(false);

//=========================================================================================================

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
